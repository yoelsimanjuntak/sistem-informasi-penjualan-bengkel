-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 06 Bulan Mei 2021 pada 13.16
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_swa_v2`
--
CREATE DATABASE IF NOT EXISTS `web_swa_v2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `web_swa_v2`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mbarang`
--

DROP TABLE IF EXISTS `mbarang`;
CREATE TABLE `mbarang` (
  `Id` bigint(10) UNSIGNED NOT NULL,
  `NmStock` varchar(100) NOT NULL DEFAULT '',
  `NmKode` varchar(100) DEFAULT NULL,
  `NmSatuan` varchar(10) DEFAULT NULL,
  `NmKeterangan` text,
  `Harga` double DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mbarang`
--

INSERT INTO `mbarang` (`Id`, `NmStock`, `NmKode`, `NmSatuan`, `NmKeterangan`, `Harga`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 'STOCK 1', 'ST01', 'UNIT', NULL, 97000, 'admin', '2021-01-30 18:51:29', 'admin', '2021-04-05 18:49:38'),
(2, 'STOCK 2', 'ST02', 'PCS', NULL, 120000, 'admin', '2021-01-31 14:13:12', 'admin', '2021-04-05 18:49:32'),
(3, 'STOCK 3', 'ST003', 'UNIT', NULL, 100000, 'admin', '2021-04-05 18:49:21', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mgudang`
--

DROP TABLE IF EXISTS `mgudang`;
CREATE TABLE `mgudang` (
  `Id` bigint(10) UNSIGNED NOT NULL,
  `NmWarehouse` varchar(100) NOT NULL DEFAULT '',
  `NmAlamat` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mgudang`
--

INSERT INTO `mgudang` (`Id`, `NmWarehouse`, `NmAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 'GUDANG UTAMA', 'Jl. Sisingamangaraja No. 122, Medan, Sumatera Utara', 'admin', '2021-01-30 18:44:22', 'admin', '2021-01-30 18:48:06'),
(3, 'GUDANG SEKTOR 1', 'Medan', 'admin', '2021-01-30 23:55:48', NULL, NULL),
(4, 'GUDANG SEKTOR 2', 'Medan', 'admin', '2021-01-30 23:55:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mkategori`
--

DROP TABLE IF EXISTS `mkategori`;
CREATE TABLE `mkategori` (
  `Id` bigint(10) UNSIGNED NOT NULL,
  `NmKategori` varchar(100) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mpelanggan`
--

DROP TABLE IF EXISTS `mpelanggan`;
CREATE TABLE `mpelanggan` (
  `Id` bigint(10) UNSIGNED NOT NULL,
  `NmCustomer` varchar(100) NOT NULL DEFAULT '',
  `NmNama` varchar(100) DEFAULT NULL,
  `NmNoHP` varchar(100) DEFAULT NULL,
  `NmAlamat` varchar(100) DEFAULT NULL,
  `CreatedBy` varchar(50) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) NOT NULL DEFAULT '',
  `UpdatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mpelanggan`
--

INSERT INTO `mpelanggan` (`Id`, `NmCustomer`, `NmNama`, `NmNoHP`, `NmAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 'CV. TIMBUL TENGGELAM', 'Jorbut', '-', 'Jl. Teuku Amir Hamzah No. 55, Medan, Sumatera Utara', 'admin', '2021-01-30 18:24:15', 'admin', '2021-01-30 18:31:08'),
(2, 'CV. PARTOPI TAO', 'Rolas Sim', '-', 'Jl. Teuku Amir Hamzah No. 55, Medan, Sumatera Utara', 'admin', '2021-01-31 18:50:53', 'admin', '2021-05-03 10:49:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mpemasok`
--

DROP TABLE IF EXISTS `mpemasok`;
CREATE TABLE `mpemasok` (
  `Id` bigint(10) UNSIGNED NOT NULL,
  `NmSupplier` varchar(100) NOT NULL DEFAULT '',
  `NmNama` varchar(100) DEFAULT NULL,
  `NmNoHP` varchar(100) DEFAULT NULL,
  `NmAlamat` varchar(100) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mpemasok`
--

INSERT INTO `mpemasok` (`Id`, `NmSupplier`, `NmNama`, `NmNoHP`, `NmAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 'CV. MAJU MUNDUR', 'Hasudungan', '-', 'Medan', 'admin', '2021-01-30 18:37:06', 'admin', '2021-01-30 18:41:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbarang`
--

DROP TABLE IF EXISTS `tbarang`;
CREATE TABLE `tbarang` (
  `MovId` bigint(10) UNSIGNED NOT NULL,
  `IdWarehouse` bigint(10) UNSIGNED NOT NULL,
  `IdStock` bigint(10) UNSIGNED NOT NULL,
  `DOId` bigint(10) UNSIGNED DEFAULT NULL,
  `ReceiptId` bigint(10) UNSIGNED DEFAULT NULL,
  `MoveType` varchar(10) NOT NULL DEFAULT '',
  `MoveDate` date NOT NULL,
  `MoveQty` double NOT NULL,
  `MoveRemarks` text,
  `CreatedBy` varchar(10) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbarang`
--

INSERT INTO `tbarang` (`MovId`, `IdWarehouse`, `IdStock`, `DOId`, `ReceiptId`, `MoveType`, `MoveDate`, `MoveQty`, `MoveRemarks`, `CreatedBy`, `CreatedOn`) VALUES
(1, 3, 1, NULL, 1, 'PUR', '2021-04-05', 100, 'DEV', 'admin', '2021-04-05 18:57:23'),
(2, 3, 1, 1, NULL, 'SAL', '2021-04-05', -12, 'DEV', 'admin', '2021-04-05 19:38:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tfaktur`
--

DROP TABLE IF EXISTS `tfaktur`;
CREATE TABLE `tfaktur` (
  `InvId` bigint(10) UNSIGNED NOT NULL,
  `DOId` bigint(10) UNSIGNED DEFAULT NULL,
  `ReceiptId` bigint(10) UNSIGNED DEFAULT NULL,
  `InvNo` varchar(50) DEFAULT NULL,
  `InvDate` date NOT NULL,
  `InvDue` date NOT NULL,
  `InvRemarks` text,
  `InvRemarksPayment` text,
  `InvFreight` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tfaktur`
--

INSERT INTO `tfaktur` (`InvId`, `DOId`, `ReceiptId`, `InvNo`, `InvDate`, `InvDue`, `InvRemarks`, `InvRemarksPayment`, `InvFreight`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 1, NULL, 'INVP-DEV001', '2021-05-03', '2021-05-03', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', '22000', 'admin', '2021-05-03 10:16:28', NULL, NULL),
(2, NULL, 1, 'INVP-DEV002', '2021-05-03', '2021-05-03', 'DEV', 'DEV', '22000', 'admin', '2021-05-03 13:30:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tfaktur_det`
--

DROP TABLE IF EXISTS `tfaktur_det`;
CREATE TABLE `tfaktur_det` (
  `InvDetId` bigint(10) UNSIGNED NOT NULL,
  `InvId` bigint(10) UNSIGNED NOT NULL,
  `InvQty` double NOT NULL,
  `InvPrice` double NOT NULL,
  `InvDisc` double NOT NULL,
  `InvTax` double NOT NULL,
  `InvTotal` double NOT NULL,
  `IdStock` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tfaktur_det`
--

INSERT INTO `tfaktur_det` (`InvDetId`, `InvId`, `InvQty`, `InvPrice`, `InvDisc`, `InvTax`, `InvTotal`, `IdStock`) VALUES
(1, 1, 12, 97000, 0, 10, 1280400, 1),
(2, 2, 100, 75000, 0, 10, 8250000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpembelian`
--

DROP TABLE IF EXISTS `tpembelian`;
CREATE TABLE `tpembelian` (
  `PurchId` bigint(10) UNSIGNED NOT NULL,
  `IdSupplier` bigint(10) UNSIGNED NOT NULL,
  `PurchNo` varchar(50) DEFAULT NULL,
  `PurchDate` date NOT NULL,
  `PurchAddr` text,
  `PurchRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpembelian`
--

INSERT INTO `tpembelian` (`PurchId`, `IdSupplier`, `PurchNo`, `PurchDate`, `PurchAddr`, `PurchRemarks`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 1, 'DEV001', '2021-04-05', NULL, 'DEV', 'admin', '2021-04-05 18:53:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpembelian_det`
--

DROP TABLE IF EXISTS `tpembelian_det`;
CREATE TABLE `tpembelian_det` (
  `PurchDetId` bigint(10) UNSIGNED NOT NULL,
  `PurchId` bigint(10) UNSIGNED NOT NULL,
  `IdStock` bigint(10) UNSIGNED NOT NULL,
  `PurchQty` double NOT NULL,
  `PurchPrice` double NOT NULL,
  `PurchDisc` double NOT NULL,
  `PurchTax` double NOT NULL,
  `PurchTotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpembelian_det`
--

INSERT INTO `tpembelian_det` (`PurchDetId`, `PurchId`, `IdStock`, `PurchQty`, `PurchPrice`, `PurchDisc`, `PurchTax`, `PurchTotal`) VALUES
(1, 1, 1, 100, 75000, 0, 10, 8250000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpenerimaan`
--

DROP TABLE IF EXISTS `tpenerimaan`;
CREATE TABLE `tpenerimaan` (
  `ReceiptId` bigint(10) UNSIGNED NOT NULL,
  `IdWarehouse` bigint(10) UNSIGNED NOT NULL,
  `PurchId` bigint(10) UNSIGNED NOT NULL,
  `ReceiptDate` date NOT NULL,
  `ReceiptRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpenerimaan`
--

INSERT INTO `tpenerimaan` (`ReceiptId`, `IdWarehouse`, `PurchId`, `ReceiptDate`, `ReceiptRemarks`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 3, 1, '2021-04-05', 'DEV', 'admin', '2021-04-05 18:57:23', '', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpenerimaan_det`
--

DROP TABLE IF EXISTS `tpenerimaan_det`;
CREATE TABLE `tpenerimaan_det` (
  `ReceiptDetId` bigint(10) UNSIGNED NOT NULL,
  `ReceiptId` bigint(10) UNSIGNED NOT NULL,
  `ReceiptQty` double NOT NULL,
  `IdStock` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpenerimaan_det`
--

INSERT INTO `tpenerimaan_det` (`ReceiptDetId`, `ReceiptId`, `ReceiptQty`, `IdStock`) VALUES
(1, 1, 100, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpengiriman`
--

DROP TABLE IF EXISTS `tpengiriman`;
CREATE TABLE `tpengiriman` (
  `DOId` bigint(10) UNSIGNED NOT NULL,
  `IdWarehouse` bigint(10) UNSIGNED NOT NULL,
  `SalesId` bigint(10) UNSIGNED NOT NULL,
  `DONo` varchar(100) DEFAULT NULL,
  `DODate` date NOT NULL,
  `DORemarks` text,
  `DOAddr` text NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpengiriman`
--

INSERT INTO `tpengiriman` (`DOId`, `IdWarehouse`, `SalesId`, `DONo`, `DODate`, `DORemarks`, `DOAddr`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 3, 1, 'DO-DEV001', '2021-04-05', 'DEV', '', 'admin', '2021-04-05 19:38:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpengiriman_det`
--

DROP TABLE IF EXISTS `tpengiriman_det`;
CREATE TABLE `tpengiriman_det` (
  `DODetId` bigint(10) UNSIGNED NOT NULL,
  `DOId` bigint(10) UNSIGNED NOT NULL,
  `DOQty` double NOT NULL,
  `IdStock` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpengiriman_det`
--

INSERT INTO `tpengiriman_det` (`DODetId`, `DOId`, `DOQty`, `IdStock`) VALUES
(1, 1, 12, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpenjualan`
--

DROP TABLE IF EXISTS `tpenjualan`;
CREATE TABLE `tpenjualan` (
  `SalesId` bigint(10) UNSIGNED NOT NULL,
  `IdCustomer` bigint(10) UNSIGNED NOT NULL,
  `SalesNo` varchar(50) DEFAULT NULL,
  `SalesDate` date NOT NULL,
  `SalesAddr` text,
  `SalesRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpenjualan`
--

INSERT INTO `tpenjualan` (`SalesId`, `IdCustomer`, `SalesNo`, `SalesDate`, `SalesAddr`, `SalesRemarks`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`) VALUES
(1, 2, 'SAL-DEV001', '2021-04-05', NULL, 'DEV', 'admin', '2021-04-05 19:36:17', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpenjualan_det`
--

DROP TABLE IF EXISTS `tpenjualan_det`;
CREATE TABLE `tpenjualan_det` (
  `SalesDetId` bigint(10) UNSIGNED NOT NULL,
  `SalesId` bigint(10) UNSIGNED NOT NULL,
  `SalesQty` double NOT NULL,
  `SalesPrice` double NOT NULL,
  `SalesDisc` double NOT NULL,
  `SalesTax` double NOT NULL,
  `SalesTotal` double NOT NULL,
  `IdStock` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tpenjualan_det`
--

INSERT INTO `tpenjualan_det` (`SalesDetId`, `SalesId`, `SalesQty`, `SalesPrice`, `SalesDisc`, `SalesTax`, `SalesTotal`, `IdStock`) VALUES
(1, 1, 12, 97000, 0, 10, 1280400, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `_roles`
--

DROP TABLE IF EXISTS `_roles`;
CREATE TABLE `_roles` (
  `RoleID` int(10) UNSIGNED NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `_roles`
--

INSERT INTO `_roles` (`RoleID`, `RoleName`) VALUES
(1, 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `_settings`
--

DROP TABLE IF EXISTS `_settings`;
CREATE TABLE `_settings` (
  `SettingID` int(10) UNSIGNED NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `_settings`
--

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`) VALUES
(1, 'SETTING_WEB_NAME', 'SETTING_WEB_NAME', 'SWA'),
(2, 'SETTING_WEB_DESC', 'SETTING_WEB_DESC', 'CV. SINAR WARNA AURORA'),
(3, 'SETTING_WEB_DISQUS_URL', 'SETTING_WEB_DISQUS_URL', 'https://general-9.disqus.com/embed.js'),
(4, 'SETTING_ORG_NAME', 'SETTING_ORG_NAME', 'CV. SINAR WARNA AURORA'),
(5, 'SETTING_ORG_ADDRESS', 'SETTING_ORG_ADDRESS', 'PERGUDANGAN CEMARA TRADE CENTRE, NO. 8-YZ, KEL. SAENTIS'),
(6, 'SETTING_ORG_LAT', 'SETTING_ORG_LAT', ''),
(7, 'SETTING_ORG_LONG', 'SETTING_ORG_LONG', ''),
(8, 'SETTING_ORG_PHONE', 'SETTING_ORG_PHONE', '081220212122'),
(9, 'SETTING_ORG_FAX', 'SETTING_ORG_FAX', '-'),
(10, 'SETTING_ORG_MAIL', 'SETTING_ORG_MAIL', 'Partopi Tao'),
(11, 'SETTING_WEB_API_FOOTERLINK', 'SETTING_WEB_API_FOOTERLINK', '-'),
(12, 'SETTING_WEB_LOGO', 'SETTING_WEB_LOGO', 'logo.png'),
(13, 'SETTING_WEB_SKIN_CLASS', 'SETTING_WEB_SKIN_CLASS', 'skin-green-light'),
(14, 'SETTING_WEB_PRELOADER', 'SETTING_WEB_PRELOADER', 'loader.gif'),
(15, 'SETTING_WEB_VERSION', 'SETTING_WEB_VERSION', '1.0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `_userinformation`
--

DROP TABLE IF EXISTS `_userinformation`;
CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `IdUnit` bigint(10) DEFAULT NULL,
  `Email` varchar(50) NOT NULL DEFAULT '',
  `NM_FullName` varchar(50) DEFAULT NULL,
  `NM_ProfileImage` varchar(250) DEFAULT NULL,
  `DATE_Registered` datetime DEFAULT NULL,
  `IS_EmailVerified` tinyint(1) DEFAULT NULL,
  `IS_LoginViaGoogle` tinyint(1) DEFAULT NULL,
  `IS_LoginViaFacebook` tinyint(1) DEFAULT NULL,
  `NM_GoogleOAuthID` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `_userinformation`
--

INSERT INTO `_userinformation` (`UserName`, `IdUnit`, `Email`, `NM_FullName`, `NM_ProfileImage`, `DATE_Registered`, `IS_EmailVerified`, `IS_LoginViaGoogle`, `IS_LoginViaFacebook`, `NM_GoogleOAuthID`) VALUES
('admin', NULL, 'yoelrolas@gmail.com', 'Partopi Tao', NULL, '2018-08-17 00:00:00', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `_users`
--

DROP TABLE IF EXISTS `_users`;
CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) UNSIGNED NOT NULL,
  `IsSuspend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `_users`
--

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`) VALUES
('admin', '3798e989b41b858040b8b69aa6f2ce90', 1, 0, '2021-05-06 18:13:41', '::1');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `mbarang`
--
ALTER TABLE `mbarang`
  ADD PRIMARY KEY (`Id`);

--
-- Indeks untuk tabel `mgudang`
--
ALTER TABLE `mgudang`
  ADD PRIMARY KEY (`Id`);

--
-- Indeks untuk tabel `mkategori`
--
ALTER TABLE `mkategori`
  ADD PRIMARY KEY (`Id`);

--
-- Indeks untuk tabel `mpelanggan`
--
ALTER TABLE `mpelanggan`
  ADD PRIMARY KEY (`Id`);

--
-- Indeks untuk tabel `mpemasok`
--
ALTER TABLE `mpemasok`
  ADD PRIMARY KEY (`Id`);

--
-- Indeks untuk tabel `tbarang`
--
ALTER TABLE `tbarang`
  ADD PRIMARY KEY (`MovId`),
  ADD KEY `FK_MOV_STOCK` (`IdStock`),
  ADD KEY `FK_MOV_WAREHOUSE` (`IdWarehouse`),
  ADD KEY `FK_MOV_DO` (`DOId`),
  ADD KEY `FK_MOV_RECEIPT` (`ReceiptId`);

--
-- Indeks untuk tabel `tfaktur`
--
ALTER TABLE `tfaktur`
  ADD PRIMARY KEY (`InvId`),
  ADD KEY `FK_INVOICE_RECEIPT` (`ReceiptId`),
  ADD KEY `FK_INVOICE_DELIVERY` (`DOId`);

--
-- Indeks untuk tabel `tfaktur_det`
--
ALTER TABLE `tfaktur_det`
  ADD PRIMARY KEY (`InvDetId`),
  ADD KEY `FK_DET_INVOICE` (`InvId`),
  ADD KEY `FK_INVOICE_STOCK` (`IdStock`);

--
-- Indeks untuk tabel `tpembelian`
--
ALTER TABLE `tpembelian`
  ADD PRIMARY KEY (`PurchId`),
  ADD KEY `FK_PURCHASE_SUPP` (`IdSupplier`);

--
-- Indeks untuk tabel `tpembelian_det`
--
ALTER TABLE `tpembelian_det`
  ADD PRIMARY KEY (`PurchDetId`),
  ADD KEY `FK_DET_PURCHASE` (`PurchId`),
  ADD KEY `FK_PURCHASE_STOCK` (`IdStock`);

--
-- Indeks untuk tabel `tpenerimaan`
--
ALTER TABLE `tpenerimaan`
  ADD PRIMARY KEY (`ReceiptId`),
  ADD KEY `FK_RECEIPT_PURCHASE` (`PurchId`),
  ADD KEY `FK_RECEIPT_WAREHOUSE` (`IdWarehouse`);

--
-- Indeks untuk tabel `tpenerimaan_det`
--
ALTER TABLE `tpenerimaan_det`
  ADD PRIMARY KEY (`ReceiptDetId`),
  ADD KEY `FK_DET_RECEIPT` (`ReceiptId`),
  ADD KEY `FK_RECEIPT_STOCK` (`IdStock`);

--
-- Indeks untuk tabel `tpengiriman`
--
ALTER TABLE `tpengiriman`
  ADD PRIMARY KEY (`DOId`),
  ADD KEY `FK_DO_WAREHOUSE` (`IdWarehouse`),
  ADD KEY `FK_DO_SALES` (`SalesId`);

--
-- Indeks untuk tabel `tpengiriman_det`
--
ALTER TABLE `tpengiriman_det`
  ADD PRIMARY KEY (`DODetId`),
  ADD KEY `FK_DET_DO` (`DOId`),
  ADD KEY `FK_DO_STOCK` (`IdStock`);

--
-- Indeks untuk tabel `tpenjualan`
--
ALTER TABLE `tpenjualan`
  ADD PRIMARY KEY (`SalesId`);

--
-- Indeks untuk tabel `tpenjualan_det`
--
ALTER TABLE `tpenjualan_det`
  ADD PRIMARY KEY (`SalesDetId`),
  ADD KEY `FK_DET_SALES` (`SalesId`),
  ADD KEY `FK_SALES_STOCK` (`IdStock`);

--
-- Indeks untuk tabel `_roles`
--
ALTER TABLE `_roles`
  ADD PRIMARY KEY (`RoleID`);

--
-- Indeks untuk tabel `_settings`
--
ALTER TABLE `_settings`
  ADD PRIMARY KEY (`SettingID`);

--
-- Indeks untuk tabel `_userinformation`
--
ALTER TABLE `_userinformation`
  ADD PRIMARY KEY (`UserName`);

--
-- Indeks untuk tabel `_users`
--
ALTER TABLE `_users`
  ADD PRIMARY KEY (`UserName`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `mbarang`
--
ALTER TABLE `mbarang`
  MODIFY `Id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `mgudang`
--
ALTER TABLE `mgudang`
  MODIFY `Id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `mkategori`
--
ALTER TABLE `mkategori`
  MODIFY `Id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `mpelanggan`
--
ALTER TABLE `mpelanggan`
  MODIFY `Id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `mpemasok`
--
ALTER TABLE `mpemasok`
  MODIFY `Id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbarang`
--
ALTER TABLE `tbarang`
  MODIFY `MovId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tfaktur`
--
ALTER TABLE `tfaktur`
  MODIFY `InvId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tfaktur_det`
--
ALTER TABLE `tfaktur_det`
  MODIFY `InvDetId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tpembelian`
--
ALTER TABLE `tpembelian`
  MODIFY `PurchId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tpembelian_det`
--
ALTER TABLE `tpembelian_det`
  MODIFY `PurchDetId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tpenerimaan`
--
ALTER TABLE `tpenerimaan`
  MODIFY `ReceiptId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tpenerimaan_det`
--
ALTER TABLE `tpenerimaan_det`
  MODIFY `ReceiptDetId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tpengiriman`
--
ALTER TABLE `tpengiriman`
  MODIFY `DOId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tpengiriman_det`
--
ALTER TABLE `tpengiriman_det`
  MODIFY `DODetId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tpenjualan`
--
ALTER TABLE `tpenjualan`
  MODIFY `SalesId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tpenjualan_det`
--
ALTER TABLE `tpenjualan_det`
  MODIFY `SalesDetId` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `_roles`
--
ALTER TABLE `_roles`
  MODIFY `RoleID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `_settings`
--
ALTER TABLE `_settings`
  MODIFY `SettingID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbarang`
--
ALTER TABLE `tbarang`
  ADD CONSTRAINT `FK_MOV_DO` FOREIGN KEY (`DOId`) REFERENCES `tpengiriman` (`DOId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_MOV_RECEIPT` FOREIGN KEY (`ReceiptId`) REFERENCES `tpenerimaan` (`ReceiptId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_MOV_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_MOV_WAREHOUSE` FOREIGN KEY (`IdWarehouse`) REFERENCES `mgudang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `tfaktur`
--
ALTER TABLE `tfaktur`
  ADD CONSTRAINT `FK_INVOICE_DELIVERY` FOREIGN KEY (`DOId`) REFERENCES `tpengiriman` (`DOId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_INVOICE_RECEIPT` FOREIGN KEY (`ReceiptId`) REFERENCES `tpenerimaan` (`ReceiptId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `tfaktur_det`
--
ALTER TABLE `tfaktur_det`
  ADD CONSTRAINT `FK_DET_INVOICE` FOREIGN KEY (`InvId`) REFERENCES `tfaktur` (`InvId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_INVOICE_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `tpembelian`
--
ALTER TABLE `tpembelian`
  ADD CONSTRAINT `FK_PURCHASE_SUPP` FOREIGN KEY (`IdSupplier`) REFERENCES `mpemasok` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `tpembelian_det`
--
ALTER TABLE `tpembelian_det`
  ADD CONSTRAINT `FK_DET_PURCHASE` FOREIGN KEY (`PurchId`) REFERENCES `tpembelian` (`PurchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PURCHASE_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `tpenerimaan`
--
ALTER TABLE `tpenerimaan`
  ADD CONSTRAINT `FK_RECEIPT_PURCHASE` FOREIGN KEY (`PurchId`) REFERENCES `tpembelian` (`PurchId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_RECEIPT_WAREHOUSE` FOREIGN KEY (`IdWarehouse`) REFERENCES `mgudang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `tpenerimaan_det`
--
ALTER TABLE `tpenerimaan_det`
  ADD CONSTRAINT `FK_DET_RECEIPT` FOREIGN KEY (`ReceiptId`) REFERENCES `tpenerimaan` (`ReceiptId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_RECEIPT_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `tpengiriman`
--
ALTER TABLE `tpengiriman`
  ADD CONSTRAINT `FK_DO_SALES` FOREIGN KEY (`SalesId`) REFERENCES `tpenjualan` (`SalesId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_DO_WAREHOUSE` FOREIGN KEY (`IdWarehouse`) REFERENCES `mgudang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `tpengiriman_det`
--
ALTER TABLE `tpengiriman_det`
  ADD CONSTRAINT `FK_DET_DO` FOREIGN KEY (`DOId`) REFERENCES `tpengiriman` (`DOId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_DO_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`);

--
-- Ketidakleluasaan untuk tabel `tpenjualan_det`
--
ALTER TABLE `tpenjualan_det`
  ADD CONSTRAINT `FK_DET_SALES` FOREIGN KEY (`SalesId`) REFERENCES `tpenjualan` (`SalesId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_SALES_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `_userinformation`
--
ALTER TABLE `_userinformation`
  ADD CONSTRAINT `FK_Userinformation_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
