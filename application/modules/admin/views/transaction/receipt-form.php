<style>
#tbl-items td, #tbl-items th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container {
  margin-right: 0 !important;
}
</style>

<div class="modal-body">
  <form id="form-editor" method="post" action="#">
  <div class="row">
    <div class="col-sm-2">
      <div class="form-group">
        <label>TANGGAL</label>
        <input type="text" class="form-control datepicker text-right" name="<?=COL_PURCHDATE?>" value="<?=!empty($data)?$data[COL_PURCHDATE]:""?>" required />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label>NO. PEMBELIAN</label>
        <input type="text" class="form-control" name="<?=COL_PURCHNO?>" value="<?=!empty($data)?$data[COL_PURCHNO]:""?>" required />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>NAMA TOKO / PENJUAL</label>
        <input type="text" class="form-control" name="<?=COL_PURCHSUPPLIER?>" value="<?=!empty($data)?$data[COL_PURCHSUPPLIER]:""?>" required />
        <select class="no-select2 d-none" name="<?=COL_IDSUPPLIER?>" style="width: 100%" required>
          <?=GetCombobox("SELECT * FROM mpemasok ORDER BY NmSupplier", COL_ID, COL_NMSUPPLIER, (!empty($data)?$data[COL_IDSUPPLIER]:null))?>
        </select>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>LOKASI / GUDANG</label>
        <select class="form-control" name="<?=COL_IDWAREHOUSE?>" style="width: 100%" required>
          <?=GetCombobox("SELECT * FROM mgudang ORDER BY NmWarehouse", COL_ID, COL_NMWAREHOUSE, (!empty($data)?$data[COL_IDWAREHOUSE]:null))?>
        </select>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>CATATAN</label>
        <input type="text" class="form-control" name="<?=COL_PURCHREMARKS?>" value="<?=!empty($data)?$data[COL_PURCHREMARKS]:""?>" />
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group row mb-0">
        <label class="col-sm-2">ITEM :</label>
        <div class="col-sm-10 text-right">
          <!--<button type="button" id="btn-load-request" class="btn btn-xs btn-outline-secondary"><i class="fa fa-search"></i> ISI BERDASARKAN PERMINTAAN</button>-->
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group mb-0">
        <input type="hidden" name="DistItems" />
        <div class="row">
          <div class="col-sm-12">
            <table id="tbl-items" class="table table-bordered mb-0">
              <thead class="bg-secondary">
                <tr>
                  <th>Barang</th>
                  <th>Qty</th>
                  <th>Harga</th>
                  <th>PPN (%)</th>
                  <th>Subtotal</th>
                  <th class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <?php
              if(!isset($disabled) || empty($disabled)) {
                ?>
                <tfoot>
                  <tr>
                    <th style="white-space: nowrap; min-width: 250px">
                      <select name="<?=COL_IDSTOCK?>" class="form-control form-control-sm" style="width: 100%;">
                        <?=GetCombobox("SELECT * from mbarang order by NmStock", COL_ID, COL_NMSTOCK, null, true, false, '-- Pilih Barang --')?>
                      </select>
                    </th>
                    <th style="white-space: nowrap; max-width: 100px">
                      <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_PURCHQTY?>" />
                      <p class="m-0 text-sm d-none" id="label-satuan"></p>
                    </th>
                    <th style="white-space: nowrap; max-width: 100px">
                      <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_PURCHPRICE?>" />
                    </th>
                    <th style="white-space: nowrap; max-width: 100px">
                      <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_PURCHTAX?>" />
                    </th>
                    <th style="white-space: nowrap; max-width: 100px">
                      <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_PURCHTOTAL?>" readonly />
                    </th>
                    <th style="white-space: nowrap;" class="text-center">
                      <button type="button" id="btn-add-item" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                    </th>
                  </tr>
                </tfoot>
                <?php
              }
              ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
  if(!empty($data)) {
    ?>
    <p class="text-muted text-sm font-italic label-info mb-0 mt-3 pt-2">
      Diinput oleh: <b><?=$data['Nm_CreatedBy']?></b><br />
      Diinput pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_CREATEDON]))?></b>
    </p>
    <?php
    if(!empty($data[COL_UPDATEDBY])) {
      ?>
      <p class="text-muted text-sm font-italic label-info mb-0">
        Diubah oleh: <b><?=$data['Nm_UpdatedBy']?></b><br />
        Diubah pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_UPDATEDON]))?></b>
      </p>
      <?php
    }
  }
  ?>
  <button type="submit" class="d-none">SUBMIT</button>
  </form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i> BATAL</button>
  <button type="button" class="btn btn-sm btn-primary btn-ok"><i class="far fa-check-circle"></i> SIMPAN</button>
</div>
<script>
$(document).ready(function() {
  var form = $('#form-editor');
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('[name=DistItems]', form).change(function() {
    writeItem('tbl-items', 'DistItems');
  }).val(encodeURIComponent('<?=$DistItems?>')).trigger('change');

  $('[name=IdStock]', form).change(function() {
    var val = $(this).val();
    $.ajax({
      url: "<?=site_url('admin/ajax/get-stock-uom')?>",
      method: "POST",
      data: {IdStock: val}
    }).done(function(data) {
      $('#label-satuan', form).text(data);
    });
  });

  $('[name=PurchQty],[name=PurchPrice],[name=PurchTax]', form).change(function() {
    var PurchQty = $('[name=PurchQty]', form).val();
    var PurchPrice = $('[name=PurchPrice]', form).val();
    var PurchTax = $('[name=PurchTax]', form).val();

    var total = 0;
    if(PurchQty && PurchPrice) {
      total += parseFloat(PurchQty)*parseFloat(PurchPrice);
      if(PurchTax) {
        total += (parseFloat(PurchQty)*parseFloat(PurchPrice)*parseFloat(PurchTax)/100);
      }
    }

    $('[name=PurchTotal]', form).val(total);
  }).trigger('change');

  $('#btn-add-item', $('#tbl-items')).click(function() {
    var dis = $(this);
    var arr = $('[name=DistItems]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var IdStock = $('[name=IdStock]', row).val();
    var NmStock = $('[name=IdStock] option:selected').text();
    var PurchQty = $('[name=PurchQty]', row).val();
    var PurchPrice = $('[name=PurchPrice]', row).val();
    var PurchTax = $('[name=PurchTax]', row).val();
    var PurchTotal = (parseFloat(PurchQty)*parseFloat(PurchPrice)) + (parseFloat(PurchQty)*parseFloat(PurchPrice)*parseFloat(PurchTax)/100);
    var NmSatuan = $('#label-satuan', row).text();
    if(IdStock && PurchQty) {
      var exist = jQuery.grep(arr, function(a) {
        return a.IdReceipt == IdStock;
      });
      if(exist.length == 0) {
        arr.push({'IdStock': IdStock, 'NmStock':NmStock, 'PurchQty':PurchQty, 'PurchPrice':PurchPrice, 'PurchDisc':0, 'PurchTax':PurchTax, 'PurchTotal':PurchTotal, 'NmSatuan':NmSatuan});
        $('[name=DistItems]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      } else {
        alert('Item yang sama tidak bisa diinput berulang.');
        return false;
      }
    } else {
      alert('Harap isi kolom isian dengan benar.');
      return false;
    }
  });
});

function writeItem(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].NmStock+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].PurchQty, 0)+' '+arr[i].NmSatuan+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].PurchPrice, 0)+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].PurchTax, 0)+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].PurchTotal, 0)+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].IdStock+'" /></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-center">-</td>';
          <?php
        }
        ?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.IdStock != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="6"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="6"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
