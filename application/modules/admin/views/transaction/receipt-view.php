<style>
#tbl-items td, #tbl-items th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container {
  margin-right: 0 !important;
}
.table.table-sm td, .table.table-sm th {
  padding: .5rem !important
}
</style>
<div class="modal-header">
  <h5 class="modal-title">Penerimaan No. <strong><?=$data[COL_PURCHNO]?></strong></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="TUTUP">
    <span aria-hidden="true"><i class="far fa-times"></i></span>
  </button>
</div>
<div class="modal-body p-0">
  <table class="table table-striped table-sm mb-0">
    <tbody>
      <tr>
        <td style="width: 10px; white-space: nowrap">TANGGAL</td><td style="width: 10px">:</td>
        <td><strong><?=date('d-m-Y', strtotime($data[COL_PURCHDATE]))?></strong></td>
      </tr>
      <tr>
        <td style="width: 10px; white-space: nowrap">PENJUAL / TOKO</td><td style="width: 10px">:</td>
        <td><strong><?=$data[COL_PURCHSUPPLIER]?></strong></td>
      </tr>
      <tr>
        <td style="width: 10px; white-space: nowrap">LOKASI</td><td style="width: 10px">:</td>
        <td><strong><?=$data[COL_NMWAREHOUSE]?></strong></td>
      </tr>
      <tr>
        <td style="width: 10px; white-space: nowrap">CATATAN</td><td style="width: 10px">:</td>
        <td><strong><?=$data[COL_PURCHREMARKS]?></strong></td>
      </tr>
    </tbody>
  </table>
  <div class="row">
    <div class="col-sm-12 p-3" style="background: #f4f6f9">
      <table id="tbl-items" class="table table-bordered bg-white mb-0">
        <thead>
          <tr>
            <th>Barang</th>
            <th>Qty</th>
            <th>Satuan</th>
            <th>Harga</th>
            <th>PPN</th>
            <th>Sub. Total</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $sum = 0;
          foreach ($det as $d) {
            ?>
            <tr>
              <td><?=$d[COL_NMSTOCK]?></td>
              <td class="text-right"><?=number_format($d[COL_PURCHQTY])?></td>
              <td><?=$d[COL_NMSATUAN]?></td>
              <td>Rp. <span class="pull-right"><?=number_format($d[COL_PURCHPRICE])?></span></td>
              <td class="text-right"><?=number_format($d[COL_PURCHTAX])?></td>
              <th>Rp. <span class="pull-right"><?=number_format($d[COL_PURCHTOTAL])?></span></th>
            </tr>
            <?php
            $sum+=$d[COL_PURCHTOTAL];
          }
          ?>
        </tbody>
        <tfoot>
          <tr>
            <th colspan="5" class="text-center">TOTAL</th>
            <th>Rp. <span class="pull-right"><?=number_format($sum)?></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
