<style>
#tbl-items td, #tbl-items th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container {
  margin-right: 0 !important;
}
.table.table-sm td, .table.table-sm th {
  padding: .5rem !important
}
</style>
<div class="modal-header">
  <h5 class="modal-title">Pembayaran No. <strong><?=$data[COL_PURCHNO]?></strong></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="TUTUP">
    <span aria-hidden="true"><i class="far fa-times"></i></span>
  </button>
</div>
<div class="modal-body p-0">
  <div class="row">
    <div class="col-sm-12 p-3" style="background: #f4f6f9">
      <table id="tbl-items" class="table table-bordered bg-white mb-0">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th style="width: 20%">Tanggal</th>
            <th>No. Faktur</th>
            <th style="width: 20%">Oleh</th>
            <th>Jumlah</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $sum = 0;
          if(!empty($inv)) {
            foreach ($inv as $d) {
              ?>
              <tr>
                <td class="text-center">
                  <a href="<?=site_url('admin/transaction/receipt-inv-delete/'.$d[COL_INVID])?>" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"</a>
                </td>
                <td class="text-right"><?=$d[COL_INVDATE]?></td>
                <td><?=$d[COL_INVNO]?></td>
                <td><?=$d[COL_CREATEDBY]?></td>
                <td>Rp. <span class="pull-right"><?=number_format($d[COL_INVTOTAL])?></span></td>
              </tr>
              <?php
              $sum+=$d[COL_INVTOTAL];
            }
          } else {
            ?>
            <tr>
              <th colspan="5" class="text-center">(KOSONG)</th>
            </tr>
            <?php
          }
          ?>
        </tbody>
        <tfoot>
          <tr>
            <th colspan="4" class="text-left">TOTAL PEMBELIAN</th>
            <th>Rp. <span class="pull-right"><?=number_format($data['TotalPurch'])?></span></th>
          </tr>
          <tr>
            <th colspan="4" class="text-left">TOTAL PEMBAYARAN</th>
            <th>Rp. <span class="pull-right"><?=number_format($sum)?></span></th>
          </tr>
          <tr>
            <th colspan="4" class="text-left">SISA</th>
            <th>Rp. <span class="pull-right"><?=number_format($data['TotalPurch']-$sum)?></span></th>
          </tr>
        </tfoot>
      </table>
    </div>
    <div class="col-sm-12 pl-3 pr-3 pb-3" style="background: #f4f6f9">
      <div class="card card-primary">
        <div class="card-header">
          <h5 class="card-title text-sm">BUAT PEMBAYARAN</h5>
        </div>
        <form id="form-invoice" action="<?=current_url()?>" method="post">
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <input type="text" class="form-control form-control-sm" placeholder="NO. FAKTUR" name="<?=COL_INVNO?>" required />
                </div>
                <div class="col-sm-3">
                  <input type="text" class="form-control form-control-sm datepicker" placeholder="TANGGAL" name="<?=COL_INVDATE?>" required />
                </div>
                <div class="col-sm-3">
                  <input type="text" class="form-control form-control-sm uang text-right" placeholder="JUMLAH" name="<?=COL_INVTOTAL?>" required />
                </div>
              </div>
            </div>
            <div class="form-group mb-0">
              <textarea class="form-control form-control-sm" name="<?=COL_INVREMARKS?>" placeholder="CATATAN"></textarea>
            </div>
          </div>
          <div class="modal-footer" style="padding: .5rem 1rem !important">
            <button type="submit" class="btn btn-xs btn-primary btn-ok"><i class="far fa-check-circle"></i> SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var form = $('#form-invoice');
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });
});
</script>
