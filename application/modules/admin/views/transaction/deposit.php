<?php
/**
 * Created by PhpStorm.
 * Date: 30/01/2020
 * Time: 22:01
 */
$user = GetLoggedUser();
?>
<style>
div.dataTables_info {
    padding-top: 0 !important;
}
div.dataTables_length label {
    margin-bottom: 0 !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
    </div>
  </div>
</div>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-body">
              <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 10px">#</th>
                      <th>TANGGAL</th>
                      <th>NO. & NAMA</th>
                      <th>KETERANGAN</th>
                      <th>JLH. DEPOSIT</th>
                      <th>TGL. INPUT</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <label>
    <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateFrom" value="<?=date('Y-m-1')?>" />
    <span class="ml-2">s.d</span>
    <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateTo" value="<?=date('Y-m-t')?>" />
  </label>
</div>
<div id="dom-buttons" class="d-none">
  <button type="button" class="btn btn-success btn-sm btn-refresh"><i class="far fa-refresh"></i> REFRESH</button>
  <?=anchor('admin/transaction/deposit-add','<i class="fa fa-plus"></i> TAMBAH',array('class'=>'btn-popup-form btn btn-primary btn-sm'))?>
</div>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var mform = $('#modal-form');

  mform.on('hidden.bs.modal', function (e) {
    $('.modal-content', mform).empty();
  });

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/transaction/deposit-load')?>",
      "type": 'POST',
      "data": function(data){
          var dateFrom = $('[name=filterDateFrom]', $('.filtering')).val();
          var dateTo = $('[name=filterDateTo]', $('.filtering')).val();

          data.dateFrom = dateFrom;
          data.dateTo = dateTo;
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": ""
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-8 d-flex'f<'filtering'>><'col-sm-4'<'buttons'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 1, "desc" ]],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[1,4,5], "className":'nowrap dt-body-right'},
      {"targets":[2], "className":'nowrap'},
    ],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true,"width": "50px"},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false,"width": "50px"},
      {"orderable": true,"width": "100px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-popup-form', $(row)).click(function() {
        var href = $(this).attr('href');
        $('.modal-content', mform).load(href, function() {
          mform.modal('show');

          $('form', mform).validate({
            submitHandler: function(form) {
              var btnSubmit = $('button[type=submit]', $(form));
              var txtSubmit = btnSubmit[0].innerHTML;
              btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
              $(form).ajaxSubmit({
                url: href,
                dataType: 'json',
                type : 'post',
                success: function(res) {
                  if(res.error != 0) {
                    toastr.error(res.error);
                  } else {
                    toastr.success(res.success);
                    dt.DataTable().ajax.reload();
                    mform.modal('hide');
                  }
                },
                error: function() {
                  toastr.error('SERVER ERROR');
                },
                complete: function() {
                  btnSubmit.html(txtSubmit);
                }
              });
              return false;
            }
          });

          $('.btn-action', mform).click(function() {
            var url = $(this).attr('href');
            if(confirm('Apakah anda yakin?')) {
              $.get(url, function(res) {
                if(res.error != 0) {
                  toastr.error(res.error);
                } else {
                  toastr.success(res.success);
                  mform.modal("hide");
                }
              }, "json").done(function() {
                dt.DataTable().ajax.reload();
              }).fail(function() {
                toastr.error('SERVER ERROR');
              });
            }
            return false;
          });
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).addClass('m-0').attr('placeholder', 'Kata Kunci');
    }
  });
  $("div.filtering").html($('#dom-filter').html()).addClass('dataTables_filter ml-2');
  $("div.buttons").html($('#dom-buttons').html()).addClass('d-block text-right');

  $('.btn-refresh').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-popup-form').click(function() {
    var href = $(this).attr('href');
    $('.modal-content', mform).load(href, function() {
      mform.modal('show');

      $('form', mform).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', $(form));
          var txtSubmit = btnSubmit[0].innerHTML;
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          $(form).ajaxSubmit({
            url: href,
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                dt.DataTable().ajax.reload();
                mform.modal('hide');
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
            }
          });
          return false;
        }
      });
    });
    return false;
  });
});
</script>
