<html>
<head>
  <title>Faktur <?=$this->setting_web_name?> - <?=$data[COL_SALESNO]?></title>
  <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>">
  <style>
  table#tbl-item {
    border: 1px solid #000;
    border-left: none;
  }
  table#tbl-header {
    border: 1px solid #000;
  }
  table#tbl-item td, table#tbl-item th {
    border: 1px solid #000;
    padding: .75rem;
  }
  table#tbl-item td, table#tbl-item th {
    border-right-width: 0;
    border-bottom-width: 0;
    padding: .5rem;
  }
  table#tbl-header td, table#tbl-header th {
    padding: .25rem .5rem;
  }
  table#tbl-item tr:first-child td, table#tbl-item tr:first-child th, table#tbl-header tr:first-child td, table#tbl-header tr:first-child th {
    border-top: none;
  }
  .text-right {
    text-align: right !important;
  }
  .text-left {
    text-align: left !important;
  }
  .text-center {
    text-align: center !important;
  }
  .pull-right {
    float: right !important;
  }
  </style>
</head>
<body>
  <table width="100%">
    <tr>
      <td width="20%">
        <img src="<?=MY_IMAGEURL.'kop.png'?>" style="height: 100px" />
      </td>
      <td style="padding-top: 10px 0; text-align: center">
        <p style="text-align: center; font-weight: bold">FAKTUR PENJUALAN<br />No. <?=$data[COL_SALESNO]?></p>
      </td>
    </tr>
  </table>
  <br />
  <table id="tbl-header" style="width: 100%" cellspacing="0">
    <tr>
      <td style="width: 10px; white-space: nowrap">Tanggal</td><td style="width: 10px">:</td>
      <td><strong><?=date('d-m-Y', strtotime($data[COL_SALESDATE]))?></strong></td>

      <td style="width: 10px; white-space: nowrap; padding-left: 20px">Jenis / Merk Kendaraan</td><td style="width: 10px">:</td>
      <td><strong><?=$data[COL_SALESCUSTOMERNMVEHICLE]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">Nama</td><td style="width: 10px">:</td>
      <td><strong><?=$data[COL_SALESCUSTOMER]?></strong></td>

      <td style="width: 10px; white-space: nowrap; padding-left: 20px">No. Plat Kendaraan</td><td style="width: 10px">:</td>
      <td><strong><?=$data[COL_SALESCUSTOMERNOPLAT]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">No. Telp / HP</td><td style="width: 10px">:</td>
      <td><strong><?=$data[COL_SALESCUSTOMERNO]?></strong></td>
    </tr>
  </table>
  <br />
  <table id="tbl-item" width="100%" cellspacing="0">
    <thead>
      <tr>
        <th style="border-top: none !important">Jasa / Sparepart</th>
        <th style="border-top: none !important">Qty</th>
        <th style="border-top: none !important">Satuan</th>
        <th style="border-top: none !important">Harga</th>
        <th style="border-top: none !important">PPN</th>
        <th style="border-top: none !important">Sub. Total</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $sum = 0;
      foreach ($det as $d) {
        ?>
        <tr>
          <td <?=$d[COL_ISSTOCK]!=1?'colspan="5"':''?>><?=$d[COL_ISSTOCK]==1?$d[COL_NMSTOCK]:$d[COL_SALESDESC]?></td>
          <?php
          if($d[COL_ISSTOCK]==1) {
            ?>
            <td class="text-right"><?=number_format($d[COL_SALESQTY])?></td>
            <td><?=$d[COL_NMSATUAN]?></td>
            <td class="text-right"><?=number_format($d[COL_SALESPRICE])?></td>
            <td class="text-right"><?=number_format($d[COL_SALESTAX])?></td>
            <?php
          }
          ?>
          <th class="text-right"><?=number_format($d[COL_SALESTOTAL])?></th>
        </tr>
        <?php
        $sum+=$d[COL_SALESTOTAL];
      }
      ?>
    </tbody>
    <tfoot>
      <tr>
        <th colspan="5" class="text-right">TOTAL</th>
        <th class="text-right"><?=number_format($sum)?></th>
      </tr>
      <tr>
        <th colspan="5" class="text-right">UANG MUKA / PEMBAYARAN SEBELUMNYA</th>
        <th class="text-right"><?=number_format($inv[COL_INVTOTAL])?></th>
      </tr>
      <tr>
        <th colspan="5" class="text-right">SISA PEMBAYARAN</th>
        <th class="text-right"><?=number_format($sum-$inv[COL_INVTOTAL])?></th>
      </tr>
    </tfoot>
  </table>
  <br />
  <br />
  <table>
    <tr>
      <td style="width: 200px">
        Tanda Terima:
        <br />
        <br />
        <br />
        <br />
        <br />
        <hr />
      </td>
    </tr>
  </table>
</body>
</html>
