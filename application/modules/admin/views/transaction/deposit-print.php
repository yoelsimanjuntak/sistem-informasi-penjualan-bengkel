<html>
<head>
  <title>Tanda Terima <?=$this->setting_web_name?> - <?=$data[COL_DEPOSITNO]?></title>
  <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>">
  <style>
  table#tbl-item {
    border: 1px solid #000;
    border-left: none;
  }
  table#tbl-header {
    border: 1px solid #000;
  }
  table#tbl-item td, table#tbl-item th {
    border: 1px solid #000;
    padding: .75rem;
  }
  table#tbl-item td, table#tbl-item th {
    border-right-width: 0;
    border-bottom-width: 0;
    padding: .5rem;
  }
  table#tbl-header td, table#tbl-header th {
    padding: .25rem .5rem;
  }
  table#tbl-item tr:first-child td, table#tbl-item tr:first-child th, table#tbl-header tr:first-child td, table#tbl-header tr:first-child th {
    border-top: none;
  }
  .text-right {
    text-align: right !important;
  }
  .text-left {
    text-align: left !important;
  }
  .text-center {
    text-align: center !important;
  }
  .pull-right {
    float: right !important;
  }
  </style>
</head>
<body>
  <table width="100%">
    <tr>
      <td width="20%">
        <img src="<?=MY_IMAGEURL.'kop.png'?>" style="height: 100px" />
      </td>
      <td style="padding-top: 10px 0; text-align: center">
        <p style="text-align: center; font-weight: bold">TANDA TERIMA DEPOSIT / UANG MUKA<br />No. <?=$data[COL_DEPOSITNO]?></p>
      </td>
    </tr>
  </table>
  <br />
  <table id="tbl-header" style="width: 100%" cellspacing="0">
    <tr>
      <td style="width: 10px; white-space: nowrap">Tanggal</td><td style="width: 10px">:</td>
      <td><strong><?=date('d-m-Y', strtotime($data[COL_DEPOSITDATE]))?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">Nama</td><td style="width: 10px">:</td>
      <td><strong><?=$data[COL_DEPOSITNAME]?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">Jumlah</td><td style="width: 10px">:</td>
      <td><strong>Rp. <?=number_format($data[COL_DEPOSITAMOUNT])?></strong></td>
    </tr>
    <tr>
      <td style="width: 10px; white-space: nowrap">Keterangan</td><td style="width: 10px">:</td>
      <td><strong><?=$data[COL_DEPOSITREMARKS]?></strong></td>
    </tr>
  </table>
  <br />
  <br />
  <table>
    <tr>
      <td style="width: 200px">
        Tanda Terima:
        <br />
        <br />
        <br />
        <br />
        <br />
        <hr />
      </td>
    </tr>
  </table>
</body>
</html>
