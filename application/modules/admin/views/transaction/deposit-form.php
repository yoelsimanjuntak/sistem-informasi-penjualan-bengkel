<style>
#tbl-items td, #tbl-items th, #tbl-services td, #tbl-services th {
  padding: .5rem !important;
  font-size: 10pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container, #tbl-services .select2-container {
  margin-right: 0 !important;
}
#tab-detail.nav-tabs .nav-link.active {
  font-weight: bold;
  color: #fff;
  background: #007bff;
}
#tab-detail.nav-tabs .nav-link {
  background: #fff;
}
</style>
<form id="form-editor" method="post" action="#">
<div class="modal-body pb-0">
  <div class="row">
    <div class="col-sm-4">
      <div class="form-group">
        <label>TANGGAL</label>
        <input type="text" class="form-control datepicker text-right" name="<?=COL_DEPOSITDATE?>" value="<?=!empty($data)?$data[COL_DEPOSITDATE]:""?>" required />
      </div>
    </div>
    <div class="col-sm-8">
      <div class="form-group">
        <label>NAMA PELANGGAN</label>
        <input type="text" class="form-control" name="<?=COL_DEPOSITNAME?>" value="<?=!empty($data)?$data[COL_DEPOSITNAME]:""?>" required />
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label>JUMLAH</label>
        <input type="text" class="form-control text-right uang" name="<?=COL_DEPOSITAMOUNT?>" value="<?=!empty($data)?$data[COL_DEPOSITAMOUNT]:""?>" required />
      </div>
    </div>
    <div class="col-sm-8">
      <div class="form-group">
        <label>NO. DEPOSIT</label>
        <input type="text" class="form-control" name="<?=COL_DEPOSITNO?>" value="<?=!empty($data)?$data[COL_DEPOSITNO]:""?>" required />
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
        <label>CATATAN</label>
        <textarea class="form-control" name="<?=COL_DEPOSITREMARKS?>"><?=!empty($data)?$data[COL_DEPOSITREMARKS]:""?></textarea>
      </div>
    </div>
  </div>
  <?php
  if(!empty($data)) {
    ?>
    <p class="text-muted text-sm font-italic label-info mb-0 mt-3 pt-2">
      Diinput oleh: <b><?=$data['Nm_CreatedBy']?></b><br />
      Diinput pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_CREATEDON]))?></b>
    </p>
    <?php
  }
  ?>
</div>
<?php
if(!isset($disabled) || empty($disabled)) {
  ?>
  <div class="modal-footer">
    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i> BATAL</button>
    <button type="submit" class="btn btn-sm btn-primary btn-ok"><i class="far fa-check-circle"></i> SIMPAN</button>
  </div>
  <?php
}
?>
</form>
<script>
$(document).ready(function() {
  var form = $('#form-editor');
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });
  <?php
  if(!empty($disabled)) {
    ?>
    $('input,select,textarea,button', form).not('[data-dismiss=modal]').attr('disabled', true);
    <?php
  }
  ?>
});
</script>
