<style>
#tbl-items td, #tbl-items th, #tbl-services td, #tbl-services th {
  padding: .5rem !important;
  font-size: 10pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container, #tbl-services .select2-container {
  margin-right: 0 !important;
}
#tab-detail.nav-tabs .nav-link.active {
  font-weight: bold;
  color: #fff;
  background: #007bff;
}
#tab-detail.nav-tabs .nav-link {
  background: #fff;
}
</style>
<div class="modal-body pb-0">
  <form id="form-editor" method="post" action="#">
  <div class="row">
    <div class="col-sm-4">
      <div class="form-group">
        <label>NO. TRANSAKSI</label>
        <input type="text" class="form-control" name="<?=COL_SALESNO?>" value="<?=!empty($data)?$data[COL_SALESNO]:""?>" required />
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>TANGGAL</label>
        <input type="text" class="form-control datepicker text-right" name="<?=COL_SALESDATE?>" value="<?=!empty($data)?$data[COL_SALESDATE]:""?>" required />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>NAMA PELANGGAN</label>
        <input type="text" class="form-control" name="<?=COL_SALESCUSTOMER?>" value="<?=!empty($data)?$data[COL_SALESCUSTOMER]:""?>" required />
        <select class="no-select2 d-none" name="<?=COL_IDCUSTOMER?>" style="width: 100%" required>
          <?=GetCombobox("SELECT * FROM mpelanggan ORDER BY NmCustomer", COL_ID, COL_NMCUSTOMER, (!empty($data)?$data[COL_IDCUSTOMER]:null))?>
        </select>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
        <label>NO. HP / TELP</label>
        <input type="text" class="form-control" name="<?=COL_SALESCUSTOMERNO?>" value="<?=!empty($data)?$data[COL_SALESCUSTOMERNO]:""?>" required />
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label>NO. PLAT</label>
        <input type="text" class="form-control" name="<?=COL_SALESCUSTOMERNOPLAT?>" value="<?=!empty($data)?$data[COL_SALESCUSTOMERNOPLAT]:""?>" required />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>JENIS / MERK KENDARAAN</label>
        <input type="text" class="form-control" name="<?=COL_SALESCUSTOMERNMVEHICLE?>" value="<?=!empty($data)?$data[COL_SALESCUSTOMERNMVEHICLE]:""?>" required />
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>LOKASI SPAREPART</label>
        <select class="form-control" name="<?=COL_IDWAREHOUSE?>" style="width: 100%" required>
          <?=GetCombobox("SELECT * FROM mgudang ORDER BY NmWarehouse", COL_ID, COL_NMWAREHOUSE, (!empty($data)?$data[COL_IDWAREHOUSE]:null))?>
        </select>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>CATATAN</label>
        <input type="text" class="form-control" name="<?=COL_SALESREMARKS?>" value="<?=!empty($data)?$data[COL_SALESREMARKS]:""?>" />
      </div>
    </div>
  </div>
  <div class="row pt-3 pb-3" style="border-top: 1px solid #e9ecef; background: #f4f6f9; margin: 0 -15px">
    <div class="col-sm-12">
      <ul class="nav nav-tabs" id="tab-detail" role="tablist">
        <li class="nav-item">
          <a class="nav-link text-sm active" id="tab-detail-service" data-toggle="pill" href="#detail-service" role="tab"><i class="far fa-tools"></i>&nbsp;&nbsp;JASA SERVICE / PERBAIKAN</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-sm" id="tab-detail-item" data-toggle="pill" href="#detail-item" role="tab"><i class="far fa-toolbox"></i>&nbsp;&nbsp;SPAREPART</a>
        </li>
      </ul>
      <div class="tab-content bg-white">
        <div class="tab-pane fade show active" id="detail-service" role="tabpanel">
          <div class="form-group mb-0">
            <input type="hidden" name="DistServiceItems" />
            <div class="row">
              <div class="col-sm-12">
                <table id="tbl-services" class="table table-bordered mb-0" style="border-top: none !important">
                  <thead class="bg-secondary disabled">
                    <tr>
                      <th style="border-top: none !important; border-bottom: none !important">Keterangan</th>
                      <th style="border-top: none !important; border-bottom: none !important; width: 200px">Subtotal</th>
                      <th style="border-top: none !important; border-bottom: none !important; width: 10px !important; white-space: nowrap" class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <?php
                  if(!isset($disabled) || empty($disabled)) {
                    ?>
                    <tfoot>
                      <tr>
                        <th style="white-space: nowrap; min-width: 250px">
                          <input type="text" class="form-control form-control-sm" name="<?=COL_SALESDESC?>" />
                        </th>
                        <th style="white-space: nowrap; max-width: 100px">
                          <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_SALESTOTAL?>" />
                        </th>
                        <th style="width: 10px !important; white-space: nowrap" class="text-center">
                          <button type="button" id="btn-add-service" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                        </th>
                      </tr>
                    </tfoot>
                    <?php
                  }
                  ?>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="detail-item" role="tabpanel">
          <div class="form-group mb-0">
            <input type="hidden" name="DistItems" />
            <div class="row">
              <div class="col-sm-12">
                <table id="tbl-items" class="table table-bordered mb-0" style="border-top: none !important">
                  <thead class="bg-secondary disabled">
                    <tr>
                      <th style="border-top: none !important; border-bottom: none !important;">Barang</th>
                      <th style="border-top: none !important; border-bottom: none !important;">Qty</th>
                      <th style="border-top: none !important; border-bottom: none !important;">Harga</th>
                      <th style="border-top: none !important; border-bottom: none !important;">PPN (%)</th>
                      <th style="border-top: none !important; border-bottom: none !important;">Subtotal</th>
                      <th style="border-top: none !important; border-bottom: none !important;" class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <?php
                  if(!isset($disabled) || empty($disabled)) {
                    ?>
                    <tfoot>
                      <tr>
                        <th style="white-space: nowrap; min-width: 250px">
                          <select name="<?=COL_IDSTOCK?>" class="form-control form-control-sm" style="width: 100%;">
                            <?=GetCombobox("SELECT mbarang.* from mbarang inner join mkategori kat on kat.Id = mbarang.IdKategori where kat.IsStock = 1 and kat.IsSaleItem = 1 order by NmStock", COL_ID, COL_NMSTOCK, null, true, false, '-- Pilih Barang --')?>
                          </select>
                        </th>
                        <th style="white-space: nowrap; max-width: 100px">
                          <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_SALESQTY?>" />
                          <p class="m-0 text-sm d-none" id="label-satuan"></p>
                        </th>
                        <th style="white-space: nowrap; max-width: 100px">
                          <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_SALESPRICE?>" />
                        </th>
                        <th style="white-space: nowrap; max-width: 100px">
                          <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_SALESTAX?>" />
                        </th>
                        <th style="white-space: nowrap; max-width: 100px">
                          <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_SALESTOTAL?>" readonly />
                        </th>
                        <th style="white-space: nowrap;" class="text-center">
                          <button type="button" id="btn-add-item" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                        </th>
                      </tr>
                    </tfoot>
                    <?php
                  }
                  ?>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
  if(!empty($data)) {
    ?>
    <p class="text-muted text-sm font-italic label-info mb-0 mt-3 pt-2">
      Diinput oleh: <b><?=$data['Nm_CreatedBy']?></b><br />
      Diinput pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_CREATEDON]))?></b>
    </p>
    <?php
    if(!empty($data[COL_UPDATEDBY])) {
      ?>
      <p class="text-muted text-sm font-italic label-info mb-0">
        Diubah oleh: <b><?=$data['Nm_UpdatedBy']?></b><br />
        Diubah pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_UPDATEDON]))?></b>
      </p>
      <?php
    }
  }
  ?>
  <button type="submit" class="d-none">SUBMIT</button>
  </form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i> BATAL</button>
  <button type="button" class="btn btn-sm btn-primary btn-ok"><i class="far fa-check-circle"></i> SIMPAN</button>
</div>
<script>
$(document).ready(function() {
  var form = $('#form-editor');
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('[name=DistItems]', form).change(function() {
    writeItem('tbl-items', 'DistItems');
  }).val(encodeURIComponent('<?=$DistItems?>')).trigger('change');
  $('[name=DistServiceItems]', form).change(function() {
    writeItemService('tbl-services', 'DistServiceItems');
  }).val(encodeURIComponent('<?=$DistItemServices?>')).trigger('change');

  $('[name=IdStock]', form).change(function() {
    var val = $(this).val();
    $.ajax({
      url: "<?=site_url('admin/ajax/get-stock-uom')?>",
      method: "POST",
      data: {IdStock: val}
    }).done(function(data) {
      $('#label-satuan', form).text(data);
    });

    /*$.ajax({
      url: "<?=site_url('admin/ajax/get-stock-harga')?>",
      method: "POST",
      data: {IdStock: val}
    }).done(function(data) {
      $('[name=SalesPrice]', form).val(data);
    });*/
  });

  $('[name=SalesQty],[name=SalesPrice],[name=SalesTax]', form).change(function() {
    var SalesQty = $('[name=SalesQty]', form).val();
    var SalesPrice = $('[name=SalesPrice]', form).val();
    var SalesTax = $('[name=SalesTax]', form).val();
    var row = $(this).closest('tr');

    var total = 0;
    if(SalesQty && SalesPrice) {
      total += parseFloat(SalesQty)*parseFloat(SalesPrice);
      if(SalesTax) {
        total += (parseFloat(SalesQty)*parseFloat(SalesPrice)*parseFloat(SalesTax)/100);
      }
    }

    $('[name=SalesTotal]', row).val(total);
  }).trigger('change');

  $('#btn-add-item', $('#tbl-items')).click(function() {
    var dis = $(this);
    var arr = $('[name=DistItems]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var IdStock = $('[name=IdStock]', row).val();
    var NmStock = $('[name=IdStock] option:selected').text();
    var SalesQty = $('[name=SalesQty]', row).val();
    var SalesPrice = $('[name=SalesPrice]', row).val();
    var SalesTax = $('[name=SalesTax]', row).val();
    var SalesTotal = (parseFloat(SalesQty)*parseFloat(SalesPrice)) + (parseFloat(SalesQty)*parseFloat(SalesPrice)*parseFloat(SalesTax)/100);
    var NmSatuan = $('#label-satuan', row).text();
    if(IdStock && SalesQty) {
      var exist = jQuery.grep(arr, function(a) {
        return a.IdStock == IdStock;
      });
      if(exist.length == 0) {
        arr.push({'IdStock': IdStock, 'NmStock':NmStock, 'SalesQty':SalesQty, 'SalesPrice':SalesPrice, 'SalesDisc':0, 'SalesTax':SalesTax, 'SalesTotal':SalesTotal, 'NmSatuan':NmSatuan});
        $('[name=DistItems]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      } else {
        alert('Item yang sama tidak bisa diinput berulang.');
        return false;
      }
    } else {
      alert('Harap isi kolom isian dengan benar.');
      return false;
    }
  });
  $('#btn-add-service', $('#tbl-services')).click(function() {
    var dis = $(this);
    var arr = $('[name=DistServiceItems]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var SalesDesc = $('[name=SalesDesc]', row).val();
    var SalesTotal = $('[name=SalesTotal]', row).val();
    if(SalesDesc && SalesTotal) {
      var exist = jQuery.grep(arr, function(a) {
        return a.SalesDesc == SalesDesc;
      });
      if(exist.length == 0) {
        arr.push({'SalesDesc': SalesDesc, 'SalesTotal':SalesTotal});
        $('[name=DistServiceItems]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
      } else {
        alert('Item yang sama tidak bisa diinput berulang.');
        return false;
      }
    } else {
      alert('Harap isi kolom isian dengan benar.');
      return false;
    }
  });
});

function writeItem(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].NmStock+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].SalesQty, 0)+' '+arr[i].NmSatuan+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].SalesPrice, 0)+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].SalesTax, 0)+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].SalesTotal, 0)+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].IdStock+'" /></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-center">-</td>';
          <?php
        }
        ?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.IdStock != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="6"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="6"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
function writeItemService(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].SalesDesc+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].SalesTotal, 0)+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].SalesDesc+'" /></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-center">-</td>';
          <?php
        }
        ?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.SalesDesc != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
