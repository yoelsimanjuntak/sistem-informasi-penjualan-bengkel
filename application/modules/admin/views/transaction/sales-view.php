<style>
#tbl-items td, #tbl-items th, #tbl-services td, #tbl-services th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container, #tbl-services .select2-container {
  margin-right: 0 !important;
}
.table.table-sm td, .table.table-sm th {
  padding: .5rem !important
}
</style>
<div class="modal-header">
  <h5 class="modal-title">Penjualan No. <strong><?=$data[COL_SALESNO]?></strong></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="TUTUP">
    <span aria-hidden="true"><i class="far fa-times"></i></span>
  </button>
</div>
<div class="modal-body p-0">
  <table class="table table-striped table-sm mb-0">
    <tbody>
      <tr>
        <td style="width: 10px; white-space: nowrap">TANGGAL</td><td style="width: 10px">:</td>
        <td><strong><?=date('d-m-Y', strtotime($data[COL_SALESDATE]))?></strong></td>
      </tr>
      <tr>
        <td style="width: 10px; white-space: nowrap">PELANGGAN</td><td style="width: 10px">:</td>
        <td><strong><?=$data[COL_SALESCUSTOMER]?></strong></td>
      </tr>
      <tr>
        <td style="width: 10px; white-space: nowrap">CATATAN</td><td style="width: 10px">:</td>
        <td><strong><?=$data[COL_SALESREMARKS]?></strong></td>
      </tr>
    </tbody>
  </table>
  <div class="row">
    <div class="col-sm-12 pl-3 pr-3 pb-3" style="background: #f4f6f9">
      <?php
      if(!empty($det)) {
        ?>
        <table id="tbl-items" class="table table-bordered bg-white mt-3">
          <thead>
            <tr>
              <th>Jasa / Sparepart</th>
              <th>Qty</th>
              <th>Satuan</th>
              <th>Harga</th>
              <th>PPN</th>
              <th>Sub. Total</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $sum = 0;
            foreach ($det as $d) {
              ?>
              <tr>
                <td <?=$d[COL_ISSTOCK]!=1?'colspan="5"':''?>><?=$d[COL_ISSTOCK]==1?$d[COL_NMSTOCK]:$d[COL_SALESDESC]?></td>
                <?php
                if($d[COL_ISSTOCK]==1) {
                  ?>
                  <td class="text-right"><?=number_format($d[COL_SALESQTY])?></td>
                  <td><?=$d[COL_NMSATUAN]?></td>
                  <td>Rp. <span class="pull-right"><?=number_format($d[COL_SALESPRICE])?></span></td>
                  <td class="text-right"><?=number_format($d[COL_SALESTAX])?></td>
                  <?php
                }
                ?>
                <th>Rp. <span class="pull-right"><?=number_format($d[COL_SALESTOTAL])?></span></th>
              </tr>
              <?php
              $sum+=$d[COL_SALESTOTAL];
            }
            ?>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="text-center">TOTAL</th>
              <th>Rp. <span class="pull-right"><?=number_format($sum)?></th>
            </tr>
          </tfoot>
        </table>
        <?php
      }
      ?>

    </div>
  </div>
</div>
