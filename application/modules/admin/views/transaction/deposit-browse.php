<table class="table table-sm table-bordered table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th style="width: 100px; white-space: nowrap">TANGGAL</th>
      <th>NO. / NAMA</th>
      <th style="width: 100px; white-space: nowrap">JUMLAH</th>
      <th>KETERANGAN</th>
    </tr>
  </thead>
  <tbody>
    <?php
    if(!empty($deposit)) {
      foreach($deposit as $d) {
        ?>
        <tr>
          <td class="text-center">
            <button type="button" class="btn btn-primary btn-xs btn-select"
                    data-depositid="<?=$d[COL_DEPOSITID]?>"
                    data-depositno="<?=$d[COL_DEPOSITNO]?>"
                    data-depositdesc="<?=$d[COL_DEPOSITNO]?> - an. <?=$d[COL_DEPOSITNAME]?>"
                    data-depositamt="<?=$d[COL_DEPOSITAMOUNT]?>">
              <i class="far fa-check-circle"></i>&nbsp;&nbsp;PILIH
            </button>
          </td>
          <td class="text-right"><?=date('d-m-Y', strtotime($d[COL_DEPOSITDATE]))?></td>
          <td>No. <strong><?=$d[COL_DEPOSITNO]?></strong> - an. <strong><?=$d[COL_DEPOSITNAME]?></strong></td>
          <td class="text-right"><?=number_format($d[COL_DEPOSITAMOUNT])?></td>
          <td ><?=$d[COL_DEPOSITREMARKS]?></td>
        </tr>
        <?php
      }
    } else {
      echo '<tr><td colspan="5" class="text-center">(KOSONG)</td></tr>';
    }
    ?>
  </tbody>
</table>
