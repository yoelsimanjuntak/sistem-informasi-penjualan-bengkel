<?php
/**
 * Created by PhpStorm.
 * Date: 30/01/2020
 * Time: 22:01
 */
 $data = array();
 $i = 0;
 foreach ($res as $d) {
   $htmlBtn = '';
   $htmlBtn .= '<a href="'.site_url('admin/master/warehouse-delete/'.$d[COL_ID]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
   $htmlBtn .= '<a href="'.site_url('admin/master/warehouse-edit/'.$d[COL_ID]).'" class="btn btn-xs btn-outline-primary modal-popup-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui" data-name="'.$d[COL_NMWAREHOUSE].'" data-address="'.$d[COL_NMALAMAT].'"><i class="fas fa-pencil-alt"></i></a>';

   $res[$i] = array(
       $htmlBtn,
       $d[COL_NMWAREHOUSE],
       $d[COL_NMALAMAT],
       date("Y-m-d H:i:s", strtotime($d[COL_CREATEDON]))
   );
   $i++;
 }
 $data = json_encode($res);
 $user = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>NAMA</th>
                    <th>ALAMAT</th>
                    <th>DIBUAT PADA</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
        <div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <form id="form-editor" method="post" action="#">
                <div class="modal-body">
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" name="<?=COL_NMWAREHOUSE?>" required />
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" name="<?=COL_NMALAMAT?>"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i> BATAL</button>
                  <button type="submit" class="btn btn-sm btn-primary btn-ok"><i class="far fa-check-circle"></i> SIMPAN</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-buttons" class="d-none">
  <button type="button" class="btn btn-success btn-sm btn-refresh"><i class="far fa-refresh"></i> REFRESH</button>
  <?=anchor('admin/master/warehouse-add','<i class="fa fa-plus"></i> TAMBAH',array('class'=>'modal-popup btn btn-primary btn-sm'))?>
</div>
<script type="text/javascript">
var dt = null;
$(document).ready(function() {
  $('#modal-editor').on('hidden.bs.modal', function (e) {
    $('input, textarea', $('#modal-editor')).val('').change();
  });

  var dt = $('#datalist').dataTable({
    "autoWidth": false,
    //"sDom": "Rlfrtip",
    "aaData": <?=$data?>,
    "scrollY" : '44vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "PENCARIAN "
    },
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-6 d-flex'f><'col-sm-6'<'buttons'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[3], "className":'nowrap dt-body-right'},
      {"targets":[1,2], "className":'nowrap'}
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true,"width": "10px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('[data-toggle="tooltip"]', $(row)).tooltip();
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            location.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.modal-popup, .modal-popup-edit', $(row)).click(function(){
        var a = $(this);
        var name = $(this).data('name');
        var address = $(this).data('address');

        var info_createdby = $(this).data('createdby');
        var info_createdon = $(this).data('createdon');
        var info_updatedby = $(this).data('updatedby');
        var info_updatedon = $(this).data('updatedon');
        var htmlinfo = '';
        var editor = $("#modal-editor");

        $('[name=<?=COL_NMWAREHOUSE?>]', editor).val(name);
        $('[name=<?=COL_NMALAMAT?>]', editor).val(address);
        $('.label-info', editor).html(htmlinfo);

        editor.modal("show");
        $('#form-editor').validate({
          submitHandler: function(form) {
            $('button[type=submit]', form).attr("disabled", true);
            $(form).ajaxSubmit({
              dataType: 'json',
              url : a.attr('href'),
              success : function(data){
                if(data.error==0){
                  toastr.success(data.success);
                }else{
                  toastr.error(data.error);
                }
              },
              complete: function(data) {
                location.reload();
                editor.modal("hide");
                $('button[type=submit]', form).attr("disabled", false);
              }
            });
            return false;
          }
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).attr('placeholder', 'Kata Kunci');
    }
  });
  $("div.buttons").html($('#dom-buttons').html()).addClass('d-block text-right');
  $('input,select', $("div.filtering")).change(function() {
    location.reload();
  });
  $('button.btn-refresh', $("div.buttons")).click(function() {
    location.reload();
  });

  $('.modal-popup').click(function(){
    var a = $(this);
    var editor = $("#modal-editor");
    editor.modal("show");

    $('#form-editor').validate({
      submitHandler: function(form) {
        $('button[type=submit]', form).attr("disabled", true);
        $(form).ajaxSubmit({
          dataType: 'json',
          url : a.attr('href'),
          success : function(data){
            if(data.error==0){
              toastr.success(data.success);
            }else{
              toastr.error(data.error);
            }
          },
          complete: function(data) {
            location.reload();
            editor.modal("hide");
            $('button[type=submit]', form).attr("disabled", false);
          }
        });
        return false;
      }
    });
    return false;
  });
});
</script>
