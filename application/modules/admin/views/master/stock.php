<?php
/**
 * Created by PhpStorm.
 * Date: 30/01/2020
 * Time: 22:01
 */
?>
<style>
.table.table-sm td, .table.table-sm th {
  padding: .5rem !important
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>KATEGORI</th>
                    <th>BARANG</th>
                    <th>SATUAN</th>
                    <th>HARGA BELI</th>
                    <th>DIBUAT PADA</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
        <div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <form id="form-editor" method="post" action="#">
                <div class="modal-body">
                    <div class="form-group">
                      <label>Nama</label>
                      <input type="text" class="form-control" name="<?=COL_NMSTOCK?>" required />
                    </div>
                    <div class="form-group">
                      <label>Kategori</label>
                      <select class="form-control form-control-sm" name="<?=COL_IDKATEGORI?>" style="width: 100%" required>
                        <?=GetCombobox("SELECT * from mkategori where IsStock=1", COL_ID, COL_NMKATEGORI)?>
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-4">
                          <label>Satuan</label>
                          <input type="text" class="form-control" name="<?=COL_NMSATUAN?>" required />
                        </div>
                        <div class="col-sm-8">
                          <label>Harga Beli / Modal</label>
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text font-weight-bold">Rp.</span>
                            </div>
                            <input type="text" class="form-control text-right uang" name="<?=COL_HARGA?>" required />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group d-none" id="form-init-stock">
                      <div class="row">
                        <div class="col-sm-12">
                          <table class="table table-bordered table-sm mt-3">
                            <tbody>
                              <tr>
                                <td colspan="3">
                                  <label class="mb-0"><input type="checkbox" name="InitStock" value="1" checked disabled />&nbsp;&nbsp;INPUT STOK AWAL</label>
                                </td>
                              </tr>
                              <tr>
                                <td style="width: 120px">
                                  <input type="text" class="form-control form-control-sm datepicker text-right" placeholder="TANGGAL" name="InitStockDate" disabled />
                                </td>
                                <td style="width: 100px">
                                  <input type="text" class="form-control form-control-sm uang text-right" placeholder="JLH" value="0" name="InitStockNum" disabled />
                                </td>
                                <td>
                                  <select class="form-control form-control-sm no-select2" name="InitStockWH" style="width: 100%" disabled required>
                                    <?=GetCombobox("SELECT * FROM mgudang ORDER BY NmWarehouse", COL_ID, COL_NMWAREHOUSE, (!empty($data)?$data[COL_IDWAREHOUSE]:null))?>
                                  </select>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i> BATAL</button>
                  <button type="submit" class="btn btn-sm btn-primary btn-ok"><i class="far fa-check-circle"></i> SIMPAN</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <select class="form-control form-control-sm no-select2" name="filterKategori" style="width: 200px">
    <?=GetCombobox("SELECT * from mkategori", COL_ID, COL_NMKATEGORI, null, true, false, '-- SEMUA KATEGORI --')?>
  </select>
</div>
<div id="dom-buttons" class="d-none">
  <button type="button" class="btn btn-success btn-sm btn-refresh"><i class="far fa-refresh"></i> REFRESH</button>
  <?=anchor('admin/master/stock-add','<i class="fa fa-plus"></i> TAMBAH',array('class'=>'modal-popup btn btn-primary btn-sm'))?>
</div>
<script type="text/javascript">
var dt = null;
$(document).ready(function() {
  $('#modal-editor').on('hidden.bs.modal', function (e) {
    $('input, textarea', $('#modal-editor')).val('').change();
    $('#form-init-stock').addClass('d-none');
    $('input, select', $('#form-init-stock')).attr('disabled', true);
  });

  var dt = $('#datalist').dataTable({
    "autoWidth": false,
    //"sDom": "Rlfrtip",
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/master/stock-load')?>",
      "type": 'POST',
      "data": function(data){
        data.filterKategori = $('[name=filterKategori]', $('.filtering')).val();
       }
    },
    "scrollY" : '44vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "PENCARIAN "
    },
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-6 d-flex'f<'filtering'>><'col-sm-6'<'buttons'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[4,5], "className":'nowrap dt-body-right'},
      {"targets":[1,2], "className":'nowrap'}
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      {"orderable": true,"width": "10px"},
      {"orderable": true},
      {"orderable": false,"width": "10px"},
      {"orderable": false,"width": "10px"},
      {"orderable": true,"width": "10px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('[data-toggle="tooltip"]', $(row)).tooltip();
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.modal-popup, .modal-popup-edit', $(row)).click(function(){
        var a = $(this);
        var name = $(this).data('name');
        var kategori = $(this).data('kategori');
        var satuan = $(this).data('satuan');
        var harga = $(this).data('harga');

        var info_createdby = $(this).data('createdby');
        var info_createdon = $(this).data('createdon');
        var info_updatedby = $(this).data('updatedby');
        var info_updatedon = $(this).data('updatedon');
        var htmlinfo = '';
        var editor = $("#modal-editor");

        $('[name=<?=COL_NMSTOCK?>]', editor).val(name);
        $('[name=<?=COL_IDKATEGORI?>]', editor).val(kategori).trigger('change');
        $('[name=<?=COL_NMSATUAN?>]', editor).val(satuan);
        $('[name=<?=COL_HARGA?>]', editor).val(harga);
        $('.label-info', editor).html(htmlinfo);

        editor.modal("show");
        $('#form-editor').validate({
          submitHandler: function(form) {
            $('button[type=submit]', form).attr("disabled", true);
            $(form).ajaxSubmit({
              dataType: 'json',
              url : a.attr('href'),
              success : function(data){
                if(data.error==0){
                  toastr.success(data.success);
                }else{
                  toastr.error(data.error);
                }
              },
              complete: function(data) {
                dt.DataTable().ajax.reload();
                editor.modal("hide");
                $('button[type=submit]', form).attr("disabled", false);
              }
            });
            return false;
          }
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).attr('placeholder', 'Kata Kunci');
    }
  });
  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');
  $("div.buttons").html($('#dom-buttons').html()).addClass('d-block text-right');
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });
  $('button.btn-refresh', $("div.buttons")).click(function() {
    dt.DataTable().ajax.reload();
  });

  $('.modal-popup').click(function(){
    var a = $(this);
    var editor = $("#modal-editor");

    editor.modal("show");
    $('#form-init-stock', editor).removeClass('d-none');
    $('input, select', $('#form-init-stock')).attr('disabled', false);

    $('#form-editor').validate({
      submitHandler: function(form) {
        $('button[type=submit]', form).attr("disabled", true);
        $(form).ajaxSubmit({
          dataType: 'json',
          url : a.attr('href'),
          success : function(data){
            if(data.error==0){
              toastr.success(data.success);
            }else{
              toastr.error(data.error);
            }
          },
          complete: function(data) {
            dt.DataTable().ajax.reload();
            editor.modal("hide");
            $('button[type=submit]', form).attr("disabled", false);
          }
        });
        return false;
      }
    });
    return false;
  });
});
</script>
