<html>
<head>
  <title>Faktur Penjualan</title>
  <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
  <style>
  table#tbl-item {
    border: 1px solid #000;
    border-left: none;
  }
  table#tbl-item td, table#tbl-item th {
    border: 1px solid #000;
    padding: .75rem;
  }
  table#tbl-item td, table#tbl-item th {
    border-right-width: 0;
    border-bottom-width: 0;
    padding: .5rem;
  }
  table#tbl-item tr:first-child td, table#tbl-item tr:first-child th {
    border-top: none;
  }
  </style>
</head>
<body>
  <table width="100%" style="border-bottom: 1px solid #000">
    <tr>
      <td width="80%">
        <strong><?=$this->setting_org_name?></strong><br />
        <?=$this->setting_org_address?><br /><br />
        <table width="100%">
          <tr>
            <td style="width: 60px" valign="top">Kepada</td>
            <td style="width: 10px" valign="top">:</td>
            <td>
              <span style="font-weight: bold"><?=$data[COL_NMCUSTOMER]?></span><br />
              <span><?=$data[COL_NMALAMAT]?></span>
            </td>
          </tr>
        </table>
      </td>
      <td width="20%" valign="top">
        <strong style="text-decoration: underline">FAKTUR PENJUALAN</strong><br />
        <table width="100%">
          <tr>
            <td style="width: 50px"><span>NO. FAKTUR</span></td>
            <td style="width: 10px"><span>:</span></td>
            <td><span><?=$data[COL_INVNO]?></span></td>
          </tr>
          <tr>
            <td style="width: 50px"><span>TANGGAL</span></td>
            <td style="width: 10px"><span>:</span></td>
            <td><span><?=date('d-m-Y', strtotime($data[COL_INVDATE]))?></span></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br />
  <table id="tbl-item" width="100%" cellspacing="0">
    <tr>
      <th>KODE</th>
      <th>NAMA BARANG</th>
      <th>SATUAN</th>
      <th>QTY</th>
      <th>HARGA</th>
      <th>JUMLAH</th>
    </tr>
    <?php
    $sumInv = 0;
    foreach($DistItems as $d) {
      $sumInv += $d[COL_INVTOTAL];
      ?>
      <tr>
        <td><?=$d[COL_NMKODE]?></td>
        <td><?=$d[COL_NMSTOCK]?></td>
        <td><?=$d[COL_NMSATUAN]?></td>
        <td style="text-align: right"><?=number_format($d[COL_INVQTY])?></td>
        <td style="text-align: right"><?=number_format($d[COL_INVPRICE])?></td>
        <td style="text-align: right"><?=number_format($d[COL_INVTOTAL])?></td>
      </tr>
      <?php
    }
    ?>
  </table>
  <br />
  <br />
  <table width="100%">
    <tr>
      <td style="width: 50px" valign="top">Terbilang</td>
      <td style="width: 10px" valign="top">:</td>
      <td style="font-style: italic" valign="top"><?=terbilang($sumInv)?></td>
      <td style="width: 20%" rowspan="2">
        <table width="100%">
          <tr>
            <td style="width: 100px"><strong>Total</strong></td>
            <td style="width: 10px">:</td>
            <td style="text-align: right"><strong><?=number_format($sumInv)?></strong></td>
          </tr>
          <tr>
            <td style="width: 100px">Disc</td>
            <td style="width: 10px">:</td>
            <td style="text-align: right">-</td>
          </tr>
          <tr>
            <td style="width: 100px"><strong>Total Faktur</strong></td>
            <td style="width: 10px">:</td>
            <td style="text-align: right"><strong><?=number_format($sumInv)?></strong></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="width: 50px" valign="top">Keterangan</td>
      <td style="width: 10px" valign="top">:</td>
      <td></td>
    </tr>
  </table>
  <br />
  <br />
  <table width="100%">
    <tr>
      <td>
        Disiapkan:
        <br />
        <br />
        <br />
        <br />
        __________________
        <br />
        Tgl:
      </td>
      <td>
        Disetujui:
        <br />
        <br />
        <br />
        <br />
        __________________
        <br />
        Tgl:
      </td>
      <td>
        Diterima:
        <br />
        <br />
        <br />
        <br />
        __________________
        <br />
        Tgl:
      </td>
    </tr>
  </table>
</body>
</html>
