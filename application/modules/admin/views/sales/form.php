<style>
#tbl-items td, #tbl-items th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container {
  margin-right: 0 !important;
}
</style>
<form id="form-editor" method="post" action="#">
<div class="modal-header">
  <h5 class="modal-title">Form Pesanan Barang (SO)</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <p class="text-danger error-message"></p>
    <div class="row">
      <div class="col-sm-2">
        <div class="form-group">
          <label>TANGGAL</label>
          <input type="text" class="form-control datepicker text-right" name="<?=COL_SALESDATE?>" value="<?=!empty($data)?$data[COL_SALESDATE]:""?>" required />
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
          <label>CUSTOMER</label>
          <select class="form-control" name="<?=COL_IDCUSTOMER?>" style="width: 100%" required>
            <?=GetCombobox("SELECT * FROM mpelanggan ORDER BY NmCustomer", COL_ID, COL_NMCUSTOMER, (!empty($data)?$data[COL_IDCUSTOMER]:null))?>
          </select>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
          <label>NO. PESANAN</label>
          <input type="text" class="form-control" name="<?=COL_SALESNO?>" value="<?=!empty($data)?$data[COL_SALESNO]:""?>" />
        </div>
      </div>
      <div class="col-sm-12">
        <div class="form-group">
          <div class="form-group">
            <label>CATATAN</label>
            <textarea rows="2" class="form-control" name="<?=COL_SALESREMARKS?>"><?=!empty($data)?$data[COL_SALESREMARKS]:''?></textarea>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group row mb-0">
          <label class="col-sm-2">ITEM :</label>
          <div class="col-sm-10 text-right">
            <!--<button type="button" id="btn-load-request" class="btn btn-xs btn-outline-secondary"><i class="fa fa-search"></i> ISI BERDASARKAN PERMINTAAN</button>-->
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input type="hidden" name="DistItems" />
          <div class="row">
            <div class="col-sm-12">
              <table id="tbl-items" class="table table-bordered">
                <thead class="bg-info">
                  <tr>
                    <th>Nama Barang</th>
                    <th>QTY</th>
                    <th>Harga</th>
                    <th>Tax (%)</th>
                    <th>Subtotal</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
                <?php
                if(!isset($disabled) || empty($disabled)) {
                  ?>
                  <tfoot>
                    <tr>
                      <th style="white-space: nowrap; min-width: 250px">
                        <select name="<?=COL_IDSTOCK?>" class="form-control form-control-sm" style="width: 100%;">
                          <?=GetCombobox("SELECT * from mbarang order by NmStock", COL_ID, COL_NMSTOCK, null, true, false, '-- Pilih Nama Barang --')?>
                        </select>
                      </th>
                      <th style="white-space: nowrap; max-width: 100px">
                        <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_SALESQTY?>" />
                        <p class="m-0 text-sm d-none" id="label-satuan"></p>
                      </th>
                      <th style="white-space: nowrap; max-width: 100px">
                        <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_SALESPRICE?>" />
                      </th>
                      <th style="white-space: nowrap; max-width: 100px">
                        <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_SALESTAX?>" />
                      </th>
                      <th style="white-space: nowrap; max-width: 100px">
                        <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_SALESTOTAL?>" readonly />
                      </th>
                      <th style="white-space: nowrap;" class="text-center">
                        <button type="button" id="btn-add-item" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                      </th>
                    </tr>
                  </tfoot>
                  <?php
                }
                ?>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    if(!empty($data)) {
      ?>
      <p class="text-muted text-sm font-italic label-info mb-0 mt-3 pt-2">
        Diinput oleh: <b><?=$data['Nm_CreatedBy']?></b><br />
        Diinput pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_CREATEDON]))?></b>
      </p>
      <?php
    }
    ?>

</div>
<?php
if(!isset($disabled) || empty($disabled)) {
  ?>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">BATAL</button>
    <button type="submit" class="btn btn-outline-primary btn-ok">SIMPAN</button>
  </div>
  <?php
}
?>
</form>
<script>
$(document).ready(function() {
  var form = $('#form-editor');
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('[name=DistItems]', form).change(function() {
    writeItem('tbl-items', 'DistItems');
  }).val(encodeURIComponent('<?=$DistItems?>')).trigger('change');

  $('[name=IdStock]', form).change(function() {
    var val = $(this).val();
    $.ajax({
      url: "<?=site_url('admin/ajax/get-stock-uom')?>",
      method: "POST",
      data: {IdStock: val}
    }).done(function(data) {
      $('#label-satuan', form).text(data);
    });

    $.ajax({
      url: "<?=site_url('admin/ajax/get-stock-harga')?>",
      method: "POST",
      data: {IdStock: val}
    }).done(function(data) {
      $('[name=SalesPrice]', form).val(data);
    });
  });

  $('[name=SalesQty],[name=SalesPrice],[name=SalesTax]', form).change(function() {
    var SalesQty = $('[name=SalesQty]', form).val();
    var SalesPrice = $('[name=SalesPrice]', form).val();
    var SalesTax = $('[name=SalesTax]', form).val();

    var total = 0;
    if(SalesQty && SalesPrice) {
      total += parseFloat(SalesQty)*parseFloat(SalesPrice);
      if(SalesTax) {
        total += (parseFloat(SalesQty)*parseFloat(SalesPrice)*parseFloat(SalesTax)/100);
      }
    }

    $('[name=SalesTotal]', form).val(total);
  }).trigger('change');

  $('#btn-add-item', $('#tbl-items')).click(function() {
    var dis = $(this);
    var arr = $('[name=DistItems]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var IdStock = $('[name=IdStock]', row).val();
    var NmStock = $('[name=IdStock] option:selected').text();
    var SalesQty = $('[name=SalesQty]', row).val();
    var SalesPrice = $('[name=SalesPrice]', row).val();
    var SalesTax = $('[name=SalesTax]', row).val();
    var SalesTotal = (parseFloat(SalesQty)*parseFloat(SalesPrice)) + (parseFloat(SalesQty)*parseFloat(SalesPrice)*parseFloat(SalesTax)/100);
    var NmSatuan = $('#label-satuan', row).text();
    if(IdStock && SalesQty) {
      var exist = jQuery.grep(arr, function(a) {
        return a.IdReceipt == IdStock;
      });
      if(exist.length == 0) {
        arr.push({'IdStock': IdStock, 'NmStock':NmStock, 'SalesQty':SalesQty, 'SalesPrice':SalesPrice, 'SalesDisc':0, 'SalesTax':SalesTax, 'SalesTotal':SalesTotal, 'NmSatuan':NmSatuan});
        $('[name=DistItems]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      } else {
        alert('Item yang sama tidak bisa diinput berulang.');
        return false;
      }
    } else {
      alert('Harap isi kolom isian dengan benar.');
      return false;
    }
  });
  <?php
  if(!empty($disabled)) {
    ?>
    $('input,select,textarea,button', form).not('[data-dismiss=modal]').attr('disabled', true);
    <?php
  }
  ?>
});

function writeItem(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].NmStock+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].SalesQty, 0)+' '+arr[i].NmSatuan+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].SalesPrice, 0)+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].SalesTax, 0)+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].SalesTotal, 0)+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].IdStock+'" /></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-center">-</td>';
          <?php
        }
        ?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.IdStock != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="6"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="6"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
