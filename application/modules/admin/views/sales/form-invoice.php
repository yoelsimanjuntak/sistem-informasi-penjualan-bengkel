<style>
#tbl-items td, #tbl-items th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container {
  margin-right: 0 !important;
}
</style>
<form id="form-editor" method="post" action="#">
<div class="modal-header">
  <h5 class="modal-title">Form Faktur Penjualan</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <p class="text-danger error-message"></p>
    <div class="row">
      <div class="col-sm-2">
        <div class="form-group">
          <label>TANGGAL</label>
          <input type="text" class="form-control datepicker text-right" name="<?=COL_INVDATE?>" value="<?=!empty($data)?$data[COL_INVDATE]:""?>" required />
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label>NO. FAKTUR</label>
          <input type="text" class="form-control" name="<?=COL_INVNO?>" value="<?=!empty($data)?$data[COL_INVNO]:""?>" />
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label>NO. DO</label>
          <select class="form-control" name="<?=COL_DOID?>" style="width: 100%" required>
            <?=GetCombobox("SELECT tpengiriman.* FROM tpengiriman ORDER BY DONo", COL_DOID, array(COL_DODATE, COL_DONO), (!empty($data)?$data[COL_DOID]:null), true, false, '-- Pilih DO --')?>
          </select>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="form-group">
          <label>TEMPO</label>
          <input type="text" class="form-control datepicker text-right" name="<?=COL_INVDUE?>" value="<?=!empty($data)?$data[COL_INVDUE]:""?>" required />
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label>FREIGHT</label>
          <input type="text" class="form-control uang text-right" name="<?=COL_INVFREIGHT?>" value="<?=!empty($data)?$data[COL_INVFREIGHT]:""?>" />
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <div class="form-group">
            <label>CATATAN</label>
            <textarea rows="2" class="form-control" name="<?=COL_INVREMARKS?>"><?=!empty($data)?$data[COL_INVREMARKS]:''?></textarea>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <div class="form-group">
            <label>CATATAN PEMBAYARAN</label>
            <textarea rows="2" class="form-control" name="<?=COL_INVREMARKSPAYMENT?>"><?=!empty($data)?$data[COL_INVREMARKSPAYMENT]:''?></textarea>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group row mb-0">
          <label class="col-sm-2">ITEM :</label>
          <div class="col-sm-10 text-right">
            <!--<button type="button" id="btn-load-request" class="btn btn-xs btn-outline-secondary"><i class="fa fa-search"></i> ISI BERDASARKAN PERMINTAAN</button>-->
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input type="hidden" name="DistItems" />
          <div class="row">
            <div class="col-sm-12">
              <table id="tbl-items" class="table table-bordered">
                <thead class="bg-info">
                  <tr>
                    <th>Nama Barang</th>
                    <th>QTY</th>
                    <th>Harga</th>
                    <th>Tax (%)</th>
                    <th>Subtotal</th>
                    <th class="text-center" style="width: 40px">#</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    if(!empty($data)) {
      ?>
      <p class="text-muted text-sm font-italic label-info mb-0 mt-3 pt-2">
        Diinput oleh: <b><?=$data['Nm_CreatedBy']?></b><br />
        Diinput pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_CREATEDON]))?></b>
      </p>
      <?php
    }
    ?>

</div>
<?php
if(!isset($disabled) || empty($disabled)) {
  ?>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">BATAL</button>
    <button type="submit" class="btn btn-outline-primary btn-ok">SIMPAN</button>
  </div>
  <?php
}
?>
</form>
<script>
$(document).ready(function() {
  var form = $('#form-editor');
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('[name=DistItems]', form).change(function() {
    writeItem('tbl-items', 'DistItems');
  }).val(encodeURIComponent('<?=$DistItems?>')).trigger('change');

  $('[name=DOId]', form).change(function() {
    var val = $(this).val();
    $.ajax({
      url: "<?=site_url('admin/ajax/get-delivery-items')?>",
      method: "POST",
      data: {IdDO: val}
    }).done(function(data) {
      data = JSON.parse(data);
      var arr = [];
      for(var i=0; i<data.length; i++) {
        var qty = parseFloat(data[i].DOQty)-parseFloat(data[i].InvQty);
        var total = qty*parseFloat(data[i].SalesPrice);

        if(data[i].SalesTax) {
          total += total*parseFloat(data[i].SalesTax)/100;
        }
        if(qty > 0) {
          arr.push({'IdStock':data[i].IdStock,
                    'NmStock':data[i].NmStock,
                    'InvQty':qty,
                    'InvPrice':data[i].SalesPrice,
                    'InvTax':data[i].SalesTax,
                    'InvDisc':data[i].SalesDisc,
                    'InvTotal':total,
                    'NmSatuan':data[i].NmSatuan});
        }

      }
      $('[name=DistItems]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
    });
  });
  <?php
  if(!empty($disabled)) {
    ?>
    $('input,select,textarea,button', form).not('[data-dismiss=modal]').attr('disabled', true);
    <?php
  }
  ?>
});

function writeItem(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].NmStock+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-right" style="width: 150px"><a href="#" class="btn-edit-qty" style="text-decoration: underline; text-decoration-style: dotted;">'+$.number(arr[i].InvQty, 0)+' '+arr[i].NmSatuan+'</a><div class="input-group input-group-sm d-none input-qty" style="width: 150px"><input type="text" name="InvQty" class="form-control uang" value="'+$.number(arr[i].InvQty, 0)+'" /><span class="input-group-append"><button type="button" class="btn btn-outline-info btn-change-qty"><i class="far fa-check"></i></button></span></div></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-right">'+$.number(arr[i].InvQty, 0)+' '+arr[i].NmSatuan+'</td>';
          <?php
        }
        ?>

        html += '<td class="text-right">'+$.number(arr[i].InvPrice, 0)+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].InvTax, 0)+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].InvTotal, 0)+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].IdStock+'" /></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-center">-</td>';
          <?php
        }
        ?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.IdStock != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
      $('.btn-edit-qty', tbl).click(function() {
        var row = $(this).closest('tr');

        $(this).addClass('d-none');
        $('.input-qty', row).removeClass('d-none');
        return false;
      });
      $('.btn-change-qty', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        var qty = $('[name=InvQty]', row).val();

        $('.input-qty', row).addClass('d-none');
        $('.btn-edit-qty', row).removeClass('d-none');

        var arr = $('[name='+input+']').val();
        arr = JSON.parse(decodeURIComponent(arr));
        for(var i=0; i<arr.length; i++) {
          if(arr[i].IdStock == idx) {
            arr[i].InvQty = qty;
            arr[i].InvTotal = parseFloat(arr[i].InvQty)*parseFloat(arr[i].InvPrice);
            if(arr[i].InvTax) {
              arr[i].InvTotal += arr[i].InvTotal*parseFloat(arr[i].InvTax)/100;
            }
          }
        }

        $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        return false;
      });
    } else {
      tbl.html('<tr><td colspan="6"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="6"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
