<html>
<head>
  <title>Receipt Order</title>
  <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
  <style>
  table#tbl-item {
    border: 1px solid #000;
    border-left: none;
  }
  table#tbl-item td, table#tbl-item th {
    border: 1px solid #000;
    padding: .75rem;
  }
  table#tbl-item td, table#tbl-item th {
    border-right-width: 0;
    border-bottom-width: 0;
    padding: .5rem;
  }
  table#tbl-item tr:first-child td, table#tbl-item tr:first-child th {
    border-top: none;
  }
  </style>
</head>
<body>
  <table width="100%" style="border-bottom: 1px solid #000">
    <tr>
      <td width="80%">
        <strong><?=$this->setting_org_name?></strong><br />
        <?=$this->setting_org_address?><br /><br />
        <table width="100%">
          <tr>
            <td style="width: 60px" valign="top">SUPPLIER</td>
            <td style="width: 10px" valign="top">:</td>
            <td>
              <span style="font-weight: bold"><?=$data[COL_NMSUPPLIER]?></span><br />
              <span><?=$data[COL_NMALAMAT]?></span>
            </td>
          </tr>
        </table>
      </td>
      <td width="20%" valign="top">
        <strong style="text-decoration: underline">RECEIPT ORDER</strong><br />
        <table width="100%">
          <tr>
            <td style="width: 50px"><span>NO.RECEIPT</span></td>
            <td style="width: 10px"><span>:</span></td>
            <td><span><?=str_pad($data[COL_RECEIPTID], 5, "0", STR_PAD_LEFT)?></span></td>
          </tr>
          <tr>
            <td style="width: 50px"><span>TANGGAL</span></td>
            <td style="width: 10px"><span>:</span></td>
            <td><span><?=date('d-m-Y', strtotime($data[COL_RECEIPTDATE]))?></span></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br />
  <table id="tbl-item" width="100%" cellspacing="0">
    <tr style="font-weight: bold">
      <th>KODE</th>
      <th>NAMA BARANG</th>
      <th>SATUAN</th>
      <th>QTY</th>
    </tr>
    <?php
    $sumInv = 0;
    foreach($DistItems as $d) {
      ?>
      <tr>
        <td><?=$d[COL_NMKODE]?></td>
        <td><?=$d[COL_NMSTOCK]?></td>
        <td><?=$d[COL_NMSATUAN]?></td>
        <td style="text-align: right"><?=number_format($d[COL_RECEIPTQTY])?></td>
      </tr>
      <?php
    }
    ?>
  </table>
  <br />
  <br />
  <table width="100%">
    <tr>
      <td style="width: 50px" valign="top">Keterangan</td>
      <td style="width: 10px" valign="top">:</td>
      <td></td>
    </tr>
  </table>
  <br />
  <br />
  <table width="100%">
    <tr>
      <td>
        Disiapkan:
        <br />
        <br />
        <br />
        <br />
        __________________
        <br />
        Tgl:
      </td>
      <td>
        Disetujui:
        <br />
        <br />
        <br />
        <br />
        __________________
        <br />
        Tgl:
      </td>
      <td>
        Diterima:
        <br />
        <br />
        <br />
        <br />
        __________________
        <br />
        Tgl:
      </td>
    </tr>
  </table>
</body>
</html>
