<?php
/**
 * Created by PhpStorm.
 * Date: 30/01/2020
 * Time: 22:01
 */
$user = GetLoggedUser();
?>
<style>
div.dataTables_info {
    padding-top: 0 !important;
}
div.dataTables_length label {
    margin-bottom: 0 !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small class="text-sm font-weight-light"> Data</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <div class="card-tools">
                <a href="<?=site_url('admin/purchase/add')?>" type="button" class="btn btn-tool btn-add-data text-info"><i class="fas fa-plus"></i>&nbsp;TAMBAH PESANAN</a>
                <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
              </div>
            </div>
            <div class="card-body">
              <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 10px">AKSI</th>
                      <th>TANGGAL</th>
                      <th>SUPPLIER</th>
                      <th>NO. PO</th>
                      <th>KETERANGAN</th>
                      <th>ADMIN</th>
                      <th>TGL. INPUT</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </form>
            </div>
          </div>
          <div class="modal fade" id="modal-editor" role="dialog">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <div class="form-group row">
    <label class="col-sm-2 mb-0">SUPPLIER</label>
    <div class="col-sm-5">
      <select class="form-control" name="filterIdSupplier" style="width: 100%" required>
        <?=GetCombobox("SELECT * FROM mpemasok ORDER BY NmSupplier", COL_ID, COL_NMSUPPLIER, null, true, false, '-- SEMUA --')?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 mb-0">PERIODE</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateFrom" value="<?=date('Y-m-1')?>" />
    </div>
    <label class="col-sm-1 mb-0 text-center">s.d</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateTo" value="<?=date('Y-m-d')?>" />
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var modalData = $('#modal-editor');

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/purchase/index-load')?>",
      "type": 'POST',
      "data": function(data){
          var dateFrom = $('[name=filterDateFrom]', $('.datefilter')).val();
          var dateTo = $('[name=filterDateTo]', $('.datefilter')).val();
          var idSupplier = $('[name=filterIdSupplier]', $('.datefilter')).val();

          data.dateFrom = dateFrom;
          data.dateTo = dateTo;
          data.idSupplier = idSupplier;
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-8 datefilter'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 1, "desc" ]],
    "columnDefs": [{"targets":[0], "className":'nowrap text-center'}, {"targets":[1,6], "className":'dt-body-right'}],
    "columns": [
      {"orderable": false},
      {"orderable": true,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false},
      {"orderable": false},
      {"orderable": true}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-view', $(row)).click(function() {
        var href = $(this).attr('href');
        $('.modal-content', modalData).load(href, function() {
          modalData.modal('show');
        });
        return false;
      });
    }
  });
  $("div.datefilter").html($('#dom-filter').html());

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.datefilter")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function() {
    var href = $(this).attr('href');
    $('.modal-content', modalData).load(href, function() {
      modalData.modal('show');

      $('form', modalData).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', $(form));
          var txtSubmit = btnSubmit[0].innerHTML;
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          $(form).ajaxSubmit({
            url: href,
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                dt.DataTable().ajax.reload();
                modalData.modal('hide');
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
            }
          });
          return false;
        }
      });
    });
    return false;
  });
});
</script>
