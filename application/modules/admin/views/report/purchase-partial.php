<?php
if(!empty($cetak)) {
  //header("Content-type: application/vnd-ms-excel");
  //header("Content-Disposition: attachment; filename=SMS - Laporan Penjualan ".date('YmdHi').".xls");
  ?>
  <style>
  table.table-bordered, .table-bordered td, .table-bordered th {
    border: 1px solid #000;
    border-collapse: collapse;
  }
  .table-bordered td, .table-bordered th {
    padding: .25rem;
    font-size: 8.5pt !important;
  }
  .text-right {
    text-align: right !important;
  }
  </style>
  <?php
}
?>
<div class="card card-default">
  <div class="card-header">
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
    </div>
  </div>
  <div class="card-body p-0">
    <?php
    if(!empty($cetak)) {
      ?>
      <h4 style="text-align: center; margin: 0; text-decoration: underline"><?=$this->setting_org_name?></h4>
      <h5 style="text-align: center; margin: 0">RINCIAN PEMBELIAN</h5>
      <h5 style="text-align: center; margin: 0"><?=$filterDateFrom.' s.d '.$filterDateTo?></h5>
      <br />
      <?php
    }
    ?>
    <div class="table-responsive">
      <table class="table table-bordered" style="font-size: 10pt" width="100%">
        <thead>
          <tr>
            <th style="width: 10px">NO. </th>
            <th>TANGGAL</th>
            <th>NO. PEMBELIAN</th>
            <?php
            if(empty($IdSupplier)) {
              ?>
              <th>TOKO / PEMASOK</th>
              <?php
            }
            ?>
            <th>TOTAL PEMBELIAN</th>
            <th>PEMBAYARAN</th>
            <th>SISA</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $sumPurch = 0;
          $sumInv = 0;
          foreach($res as $r) {
            ?>
            <tr>
              <td style="white-space: nowrap; text-align: right"><?=$no?></td>
              <td class="text-right" style="width: 10px; white-space: nowrap"><?=$r[COL_PURCHDATE]?></td>
              <td style="white-space: nowrap"><a href="<?=site_url('admin/transaction/receipt-view/'.$r[COL_PURCHID])?>" class="btn-view"><?=$r[COL_PURCHNO]?></a></td>
              <?php
              if(empty($IdSupplier)) {
                ?>
                <td style="white-space: nowrap"><?=(!empty($r[COL_PURCHSUPPLIER])?$r[COL_PURCHSUPPLIER]:$r[COL_NMSUPPLIER])?></td>
                <?php
              }
              ?>
              <td>Rp. <span class="pull-right"><?=number_format($r['TotalPurch'])?></span></td>
              <td>Rp. <span class="pull-right"><?=number_format($r['TotalInv'])?></span></td>
              <td class="font-weight-bold">Rp. <span class="pull-right"><?=number_format($r['TotalPurch']-$r['TotalInv'])?></span></td>
            </tr>
            <?php
            $no++;
            $sumPurch += $r['TotalPurch'];
            $sumInv += $r['TotalInv'];
          }
          ?>
          <?php
          if(!empty($IdSupplier)) {
            ?>
            <tr>
              <th colspan="3" class="text-center">TOTAL</th>
              <th>Rp. <span class="pull-right"><?=number_format($sumPurch)?></span></th>
              <th>Rp. <span class="pull-right"><?=number_format($sumInv)?></span></th>
              <th>Rp. <span class="pull-right"><?=number_format($sumPurch-$sumInv)?></span></th>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
