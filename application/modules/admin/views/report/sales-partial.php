<?php
if(!empty($cetak)) {
  //header("Content-type: application/vnd-ms-excel");
  //header("Content-Disposition: attachment; filename=SMS - Laporan Penjualan ".date('YmdHi').".xls");
  ?>
  <style>
  table.table-bordered, .table-bordered td, .table-bordered th {
    border: 1px solid #000;
    border-collapse: collapse;
  }
  .table-bordered td, .table-bordered th {
    padding: .25rem;
    font-size: 8.5pt !important;
  }
  .text-right {
    text-align: right !important;
  }
  </style>
  <?php
}
?>
<div class="card card-default">
  <div class="card-header">
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
    </div>
  </div>
  <div class="card-body p-0">
    <?php
    if(!empty($cetak)) {
      ?>
      <h4 style="text-align: center; margin: 0; text-decoration: underline"><?=$this->setting_org_name?></h4>
      <h5 style="text-align: center; margin: 0">RINCIAN PENJUALAN</h5>
      <h5 style="text-align: center; margin: 0"><?=$filterDateFrom.' s.d '.$filterDateTo?></h5>
      <br />
      <?php
    }
    ?>
    <div class="table-responsive">
      <table class="table table-bordered" style="font-size: 10pt" width="100%">
        <thead>
          <tr>
            <th style="width: 10px">NO. </th>
            <th>TANGGAL</th>
            <th>NO. PENJUALAN</th>
            <th>PELANGGAN</th>
            <th>TOTAL PENJUALAN</th>
            <th>PEMBAYARAN</th>
            <th>SISA</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          foreach($res as $r) {
            ?>
            <tr>
              <td class="text-right" style="white-space: nowrap"><?=$no?></td>
              <td class="text-right" style="width: 10px; white-space: nowrap"><?=$r[COL_SALESDATE]?></td>
              <td style="white-space: nowrap"><a href="<?=site_url('admin/transaction/sales-view/'.$r[COL_SALESID])?>" class="btn-view"><?=$r[COL_SALESNO]?></a></td>
              <td style="white-space: nowrap"><?=(!empty($r[COL_SALESCUSTOMER])?$r[COL_SALESCUSTOMER]:$r[COL_NMCUSTOMER])?></td>
              <td>Rp. <span class="pull-right"><?=number_format($r['TotalSales'])?></span></td>
              <td>Rp. <span class="pull-right"><?=number_format($r['TotalInv'])?></span></td>
              <td class="font-weight-bold">Rp. <span class="pull-right"><?=number_format($r['TotalSales']-$r['TotalInv'])?></span></td>
            </tr>
            <?php
            $no++;
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
