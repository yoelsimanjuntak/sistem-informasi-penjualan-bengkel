<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal','method'=>'get'))?>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6 offset-sm-3">
                  <div class="form-group row">
                    <label class="control-label col-sm-3 mb-0">PERIODE</label>
                    <div class="col-sm-3">
                      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateFrom" value="<?=(!empty($filterDateFrom)?$filterDateFrom:date('Y-m-1'))?>" />
                    </div>
                    <label class="col-sm-1 mb-0 text-center">s.d</label>
                    <div class="col-sm-3">
                      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateTo" value="<?=(!empty($filterDateTo)?$filterDateTo:date('Y-m-d'))?>" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-3 mb-0">PELANGGAN</label>
                    <div class="col-sm-7">
                      <select class="form-control" name="<?=COL_IDCUSTOMER?>" style="width: 100%">
                        <?=GetCombobox("SELECT * FROM mpelanggan ORDER BY NmCustomer", COL_ID, COL_NMCUSTOMER, (!empty($IdCustomer)?$IdCustomer:null), true, false, '-- SEMUA --')?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-12" style="text-align: center">
                    <button type="submit" class="btn btn-sm btn-outline-success" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> CETAK</button>
                    <button type="submit" class="btn btn-sm btn-outline-primary" title="Lihat"><i class="fa fa-arrow-circle-right"></i> TAMPILKAN</button>
                </div>
              </div>
            </div>
            <?=form_close()?>
          </div>
      </div>
    </div>
    <?php
    $this->load->view('admin/report/sales-partial');
    ?>
  </div>
</section>
<div class="modal fade" id="modal-editor" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  var modalData = $('#modal-editor');

  $('.btn-view').click(function() {
    var href = $(this).attr('href');
    $('.modal-content', modalData).load(href, function() {
      modalData.modal('show');
    });
    return false;
  });
});
</script>
