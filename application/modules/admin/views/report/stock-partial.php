<?php
if(!empty($cetak)) {
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=SMS - Laporan Stock ".date('YmdHi').".xls");
  ?>
  <style>
  td, th {
    background: transparent;
    border: 0.5px solid #000;
  }
  </style>
  <?php
}
?>
<div class="card card-default">
  <div class="card-header">
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
    </div>
  </div>
  <div class="card-body p-0">
    <div class="table-responsive">
      <table class="table table-bordered" style="font-size: 10pt">
        <thead>
          <tr>
            <th style="width: 10px">NO.</th>
            <th>TANGGAL</th>
            <th>GUDANG</th>
            <?php
            if(empty($IdStock)) {
              ?>
              <th>NAMA BARANG</th>
              <?php
            }
            ?>
            <th>SUPPLIER / CUSTOMER</th>
            <th>NO. TRANSAKSI</th>
            <th>JUMLAH</th>
            <th>SATUAN</th>
            <th>KET.</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $sum = 0;
          $satuan = '';
          if(!empty($prevQty) && !empty($IdStock)) {
            $sum += $prevQty;
            ?>
            <tr>
              <td colspan="5">JUMLAH SEBELUMNYA</td>
              <td class="text-right"><?=number_format($prevQty)?></td>
              <td><?=$nmSatuan?></td>
            </tr>
            <?php
          }

          $no = 1;
          foreach($res as $r) {
            ?>
            <tr>
              <td style="white-space: nowrap"><?=$no?></td>
              <td class="text-right" style="white-space: nowrap"><?=$r[COL_MOVEDATE]?></td>
              <td style="white-space: nowrap"><?=$r[COL_NMWAREHOUSE]?></td>
              <?php
              if(empty($IdStock)) {
                ?>
                <td style="white-space: nowrap"><?=$r[COL_NMSTOCK]?></td>
                <?php
              }
              ?>
              <td><?=$r['CustSuppName']?></td>
              <td><?=$r['NmRefNo']?></td>
              <td class="text-right"><?=number_format($r[COL_MOVEQTY])?></td>
              <td><?=$r[COL_NMSATUAN]?></td>
              <td><?=$r[COL_MOVEREMARKS]?></td>
            </tr>
            <?php
            $no++;
            $sum += $r[COL_MOVEQTY];
            $satuan = $r[COL_NMSATUAN];
          }
          ?>
          <?php
          if(!empty($IdStock)) {
            ?>
            <tr>
              <th colspan="5" class="text-center">TOTAL</th>
              <th class="text-right"><?=number_format($sum)?></th>
              <th><?=$satuan?></th>
              <th></th>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>

  </div>
</div>
