<?php
$dateFrom = date('Y-m-01');
$dateTo = date('Y-m-t', strtotime($dateFrom));
$ruser = GetLoggedUser();

$rpur = $this->db
->query("
  select sum(purd.PurchTotal) as TotalPurch from tpembelian pur
  left join tpembelian_det purd on purd.PurchId=pur.PurchId
  where pur.PurchDate>='$dateFrom' and pur.PurchDate<='$dateTo'")
->row_array();

$rpurinv = $this->db
->query("
  select sum(inv.InvTotal) as TotalInv from tpembelian_inv inv
  inner join tpembelian pur on pur.PurchId=inv.PurchId
  where pur.PurchDate>='$dateFrom' and pur.PurchDate<='$dateTo'")
->row_array();

$rsal_sp = $this->db
->query("
  select sum(sald.SalesTotal) as TotalSales from tpenjualan sal
  left join tpenjualan_det sald on sald.SalesId=sal.SalesId
  left join mbarang st on st.Id=sald.IdStock
  left join mkategori kat on kat.Id=st.IdKategori
  where sal.SalesDate>='$dateFrom' and sal.SalesDate<='$dateTo' and kat.IsStock=1 and kat.IsSaleItem=1")
->row_array();

$rsal_sv = $this->db
->query("
  select sum(sald.SalesTotal) as TotalSales from tpenjualan sal
  left join tpenjualan_det sald on sald.SalesId=sal.SalesId
  left join mbarang st on st.Id=sald.IdStock
  left join mkategori kat on kat.Id=st.IdKategori
  where sal.SalesDate>='$dateFrom' and sal.SalesDate<='$dateTo' and kat.IsStock=0 and kat.IsSaleItem=1")
->row_array();

$rsalinv = $this->db
->query("
  select sum(inv.InvTotal) as TotalInv from tpenjualan_inv inv
  inner join tpenjualan sal on sal.SalesId=inv.SalesId
  where sal.SalesDate>='$dateFrom' and sal.SalesDate<='$dateTo'")
->row_array();

$rnetto = $this->db
->query("
  select sum(sald.SalesTotal-(IFNULL(st.Harga, 0)*sald.SalesQty)) as TotalSales from tpenjualan sal
  left join tpenjualan_det sald on sald.SalesId=sal.SalesId
  left join mbarang st on st.Id=sald.IdStock
  left join mkategori kat on kat.Id=st.IdKategori
  where sal.SalesDate>='$dateFrom' and sal.SalesDate<='$dateTo' and kat.IsStock=1 and kat.IsSaleItem=1")
->row_array();

$ros_invp = $this->db
->query("
  select
  sum(
  (select IFNULL(sum(purd.PurchTotal),0) from tpembelian_det purd where purd.PurchId = pur.PurchId) -
  (select IFNULL(sum(inv.InvTotal),0) from tpembelian_inv inv where inv.PurchId = pur.PurchId)
  ) as OS_Inv
  from tpembelian pur
  where pur.PurchDate<='$dateTo'")
->row_array();

$ros_invs = $this->db
->query("
  select
  sum(
  (select IFNULL(sum(sald.SalesTotal),0) from tpenjualan_det sald where sald.SalesId = sal.SalesId) -
  (select IFNULL(sum(inv.InvTotal),0) from tpenjualan_inv inv where inv.SalesId = sal.SalesId)
  ) as OS_Inv
  from tpenjualan sal
  where sal.SalesDate<='$dateTo'")
->row_array();
?>
<style>
.table-sm td, .table-sm th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <div class="card card-indigo">
          <div class="card-header">
            <h5 class="card-title text-sm w-100">
              PEMBELIAN - <strong><?=strtoupper(date('M')).' '.date('Y')?></strong>
              <span class="pull-right">
                <a href="<?=site_url('admin/report/purchase').'?filterDateFrom='.$dateFrom.'&filterDateTo='.$dateTo?>">RINCIAN&nbsp;&nbsp;<i class="fas fa-chevron-right"></i></a>
              </span>
            </h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-sm mb-0" style="border: 1px solid #dedede">
              <tbody>
                <tr>
                  <td style="width: 10px; white-space: nowrap">TOTAL</td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rpur['TotalPurch'])?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">PEMBAYARAN</td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rpurinv['TotalInv'])?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">HUTANG</td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rpur['TotalPurch']-$rpurinv['TotalInv'])?></strong></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card card-primary">
          <div class="card-header">
            <h5 class="card-title text-sm w-100">
              PENJUALAN - <strong><?=strtoupper(date('M')).' '.date('Y')?></strong>
              <span class="pull-right">
                <a href="<?=site_url('admin/report/sales').'?filterDateFrom='.$dateFrom.'&filterDateTo='.$dateTo?>">RINCIAN&nbsp;&nbsp;<i class="fas fa-chevron-right"></i></a>
              </span>
            </h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-sm mb-0" style="border: 1px solid #dedede">
              <tbody>
                <tr>
                  <td style="width: 10px; white-space: nowrap">SPAREPART</td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rsal_sp['TotalSales'])?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">JASA</td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rsal_sv['TotalSales'])?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">PEMBAYARAN</td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rsalinv['TotalInv'])?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">PIUTANG</td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rsal_sp['TotalSales']+$rsal_sv['TotalSales']-$rsalinv['TotalInv'])?></strong></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-success">
          <div class="card-header">
            <h5 class="card-title text-sm w-100">
              REKAPITULASI - <strong><?=strtoupper(date('M')).' '.date('Y')?></strong>
            </h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-sm mb-0" style="border: 1px solid #dedede">
              <tbody>
                <tr>
                  <td style="width: 10px; white-space: nowrap">PENDAPATAN NETTO</td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rnetto['TotalSales']+$rsal_sv['TotalSales'])?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">PENDAPATAN NETTO <small>(SPAREPART)</small></td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rnetto['TotalSales'])?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">PENDAPATAN NETTO <small>(JASA)</small></td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($rsal_sv['TotalSales'])?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">SISA HUTANG <small>s.d <strong><?=strtoupper(date('t M Y'))?></strong></small></td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($ros_invp['OS_Inv'])?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">SISA PIUTANG <small>s.d <strong><?=strtoupper(date('t M Y'))?></strong></small></td><td style="width: 10px">:</td>
                  <td><strong>Rp. <span class="pull-right"><?=number_format($ros_invs['OS_Inv'])?></strong></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
