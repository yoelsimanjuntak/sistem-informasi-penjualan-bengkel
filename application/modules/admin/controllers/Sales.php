<?php
class Sales extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Pesanan Barang";
    $this->template->load('main', 'admin/sales/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdCustomer = !empty($_POST['idCustomer'])?$_POST['idCustomer']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_SALESDATE=>'desc');
    $orderables = array(null,COL_SALESDATE,COL_NMCUSTOMER,COL_SALESNO,COL_SALESREMARKS,null,COL_CREATEDON);
    $cols = array(COL_SALESDATE,COL_NMCUSTOMER,COL_SALESNO,COL_SALESREMARKS,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = ".TBL_TPENJUALAN.".".COL_IDCUSTOMER,"left")
    ->get(TBL_TPENJUALAN);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMCUSTOMER) $item = 'cust.'.COL_NMCUSTOMER;
      if($item == COL_CREATEDBY) $item = TBL_TPENJUALAN.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TPENJUALAN.'.'.COL_SALESDATE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TPENJUALAN.'.'.COL_SALESDATE.' <= ', $dateTo);
    }
    if(!empty($IdCustomer)) {
      $this->db->where(TBL_TPENJUALAN.'.'.COL_IDCUSTOMER, $IdCustomer);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tpenjualan.*, cust.NmCustomer, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = ".TBL_TPENJUALAN.".".COL_IDCUSTOMER,"left")
    ->order_by(TBL_TPENJUALAN.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TPENJUALAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/sales/delete/'.$r[COL_SALESID]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/sales/view/'.$r[COL_SALESID]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/sales/sales-print/'.$r[COL_SALESID]).'" target="_blank" class="btn btn-xs btn-outline-success btn-print"><i class="fas fa-print"></i></a>',
        date('Y-m-d', strtotime($r[COL_SALESDATE])),
        $r[COL_NMCUSTOMER],
        $r[COL_SALESNO],
        $r[COL_SALESREMARKS],
        $r[COL_CREATEDBY],
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrDet = array();
      $det = $this->input->post("DistItems");
      $data = array(
        COL_IDCUSTOMER => $this->input->post(COL_IDCUSTOMER),
        COL_SALESNO => $this->input->post(COL_SALESNO),
        COL_SALESDATE => $this->input->post(COL_SALESDATE),
        COL_SALESADDR => $this->input->post(COL_SALESADDR),
        COL_SALESREMARKS => $this->input->post(COL_SALESREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_SALESNO, $this->input->post(COL_SALESNO))
        ->get(TBL_TPENJUALAN)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->insert(TBL_TPENJUALAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDist = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            $arrDet[] = array(
              COL_SALESID=>$idDist,
              COL_IDSTOCK=>$s->IdStock,
              COL_SALESQTY=>toNum($s->SalesQty),
              COL_SALESPRICE=>toNum($s->SalesPrice),
              COL_SALESDISC=>toNum($s->SalesDisc),
              COL_SALESTAX=>toNum($s->SalesTax),
              COL_SALESTOTAL=>toNum($s->SalesTotal)
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TPENJUALAN_DET, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['DistItems'] = json_encode(array());
      $this->load->view('admin/sales/form', $data);
    }
  }

  public function view($id) {
    $rdata = $this->db
    ->select('tpenjualan.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_CREATEDBY,"left")
    ->where(COL_SALESID, $id)
    ->get(TBL_TPENJUALAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpenjualan_det.*, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpenjualan_det.".COL_IDSTOCK,"left")
    ->where(COL_SALESID, $rdata[COL_SALESID])
    ->get(TBL_TPENJUALAN_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'SalesQty'=>$det[COL_SALESQTY],
        'SalesPrice'=>$det[COL_SALESPRICE],
        'SalesTax'=>$det[COL_SALESTAX],
        'SalesTotal'=>$det[COL_SALESTOTAL]
      );
    }
    $this->load->view('admin/sales/form', array('DistItems'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true));
  }

  public function sales_print($id) {
    $rdata = $this->db
    ->select('tpenjualan.*, cust.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = tpenjualan.".COL_IDCUSTOMER,"left")
    ->where(COL_SALESID, $id)
    ->get(TBL_TPENJUALAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpenjualan_det.*, st.NmKode, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpenjualan_det.".COL_IDSTOCK,"left")
    ->where(COL_SALESID, $rdata[COL_SALESID])
    ->get(TBL_TPENJUALAN_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmKode'=> $det[COL_NMKODE],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'SalesQty'=>$det[COL_SALESQTY],
        'SalesPrice'=>$det[COL_SALESPRICE],
        'SalesTax'=>$det[COL_SALESTAX],
        'SalesTotal'=>$det[COL_SALESTOTAL]
      );
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-L");

    $html = $this->load->view('admin/sales/print', array('DistItems'=>$arrdet,'data'=>$rdata), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Surat Jalan.pdf', 'I');
  }

  public function delete($id) {
    $res = $this->db->where(COL_SALESID, $id)->delete(TBL_TPENJUALAN);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }

  public function delivery() {
    $data['title'] = "Pengiriman Barang";
    $this->template->load('main', 'admin/sales/index-delivery', $data);
  }

  public function delivery_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdWarehouse = !empty($_POST['idWarehouse'])?$_POST['idWarehouse']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_PURCHDATE=>'desc');
    $orderables = array(null,COL_DODATE,COL_NMWAREHOUSE,COL_DONO,COL_SALESNO,null,COL_CREATEDON);
    $cols = array(COL_DODATE,COL_NMWAREHOUSE,COL_DONO,COL_SALESNO,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENGIRIMAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MGUDANG.' wh','wh.'.COL_ID." = ".TBL_TPENGIRIMAN.".".COL_IDWAREHOUSE,"left")
    ->join(TBL_TPENJUALAN.' sal','sal.'.COL_SALESID." = ".TBL_TPENGIRIMAN.".".COL_SALESID,"left")
    ->get(TBL_TPENGIRIMAN);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMWAREHOUSE) $item = 'wh.'.COL_NMWAREHOUSE;
      if($item == COL_CREATEDBY) $item = TBL_TPENGIRIMAN.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TPENGIRIMAN.'.'.COL_DODATE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TPENGIRIMAN.'.'.COL_DODATE.' <= ', $dateTo);
    }
    if(!empty($IdWarehouse)) {
      $this->db->where(TBL_TPENGIRIMAN.'.'.COL_IDWAREHOUSE, $IdWarehouse);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tpengiriman.*, sal.SalesNo, wh.NmWarehouse, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENGIRIMAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MGUDANG.' wh','wh.'.COL_ID." = ".TBL_TPENGIRIMAN.".".COL_IDWAREHOUSE,"left")
    ->join(TBL_TPENJUALAN.' sal','sal.'.COL_SALESID." = ".TBL_TPENGIRIMAN.".".COL_SALESID,"left")
    ->order_by(TBL_TPENGIRIMAN.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TPENGIRIMAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/sales/delivery-delete/'.$r[COL_DOID]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/sales/delivery-view/'.$r[COL_DOID]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/sales/delivery-print/'.$r[COL_DOID]).'" target="_blank" class="btn btn-xs btn-outline-success btn-print"><i class="fas fa-print"></i></a>',
        date('Y-m-d', strtotime($r[COL_DODATE])),
        $r[COL_NMWAREHOUSE],
        $r[COL_DONO],
        $r[COL_SALESNO],
        $r[COL_CREATEDBY],
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function delivery_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrDet = array();
      $arrMovement = array();
      $det = $this->input->post("DistItems");
      $data = array(
        COL_SALESID => $this->input->post(COL_SALESID),
        COL_IDWAREHOUSE => $this->input->post(COL_IDWAREHOUSE),
        COL_DODATE => $this->input->post(COL_DODATE),
        COL_DONO => $this->input->post(COL_DONO),
        COL_DOREMARKS => $this->input->post(COL_DOREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_DONO, $this->input->post(COL_DONO))
        ->get(TBL_TPENGIRIMAN)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->insert(TBL_TPENGIRIMAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDist = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            $qStock = @"
            select
            coalesce(sum(mvt.MoveQty), 0) as MoveQty,
            st.NmStock,
            st.NmSatuan
            from mbarang st
            left join tbarang mvt on mvt.IdStock = st.Id
            where
              st.Id = $s->IdStock
            group by st.Id
            ";
            $resStock = $this->db->query($qStock)->row_array();
            if(empty($resStock)) {
              throw new Exception('Barang tidak valid.');
            }
            if($resStock[COL_MOVEQTY]<toNum($s->DOQty)) {
              throw new Exception('Jumlah barang '.$resStock[COL_NMSTOCK].' tidak mencukupi. Silakan cek kondisi barang.');
            }

            $arrDet[] = array(
              COL_DOID=>$idDist,
              COL_IDSTOCK=>$s->IdStock,
              COL_DOQTY=>toNum($s->DOQty)
            );

            $arrMovement[] = array(
              COL_DOID=>$idDist,
              COL_IDWAREHOUSE=>$data[COL_IDWAREHOUSE],
              COL_IDSTOCK=>$s->IdStock,
              COL_MOVETYPE=>'SAL',
              COL_MOVEDATE=>$data[COL_DODATE],
              COL_MOVEQTY=>(-1)*toNum($s->DOQty),
              COL_MOVEREMARKS=>$data[COL_DOREMARKS],
              COL_CREATEDBY => $ruser[COL_USERNAME],
              COL_CREATEDON => date('Y-m-d H:i:s')
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TPENGIRIMAN_DET, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $res = $this->db->insert_batch(TBL_TBARANG, $arrMovement);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['DistItems'] = json_encode(array());
      $this->load->view('admin/sales/form-delivery', $data);
    }
  }

  public function delivery_view($id) {
    $rdata = $this->db
    ->select('tpengiriman.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENGIRIMAN.".".COL_CREATEDBY,"left")
    ->where(COL_DOID, $id)
    ->get(TBL_TPENGIRIMAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpengiriman_det.*, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpengiriman_det.".COL_IDSTOCK,"left")
    ->where(COL_DOID, $rdata[COL_DOID])
    ->get(TBL_TPENGIRIMAN_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'DOQty'=>$det[COL_DOQTY]
      );
    }
    $this->load->view('admin/sales/form-delivery', array('DistItems'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true));
  }

  public function delivery_print($id) {
    $rdata = $this->db
    ->select('tpengiriman.*, cust.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENGIRIMAN.".".COL_CREATEDBY,"left")
    ->join(TBL_TPENJUALAN.' so','so.'.COL_SALESID." = tpengiriman.".COL_SALESID,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = so.".COL_IDCUSTOMER,"left")
    ->where(COL_DOID, $id)
    ->get(TBL_TPENGIRIMAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpengiriman_det.*, st.NmKode, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpengiriman_det.".COL_IDSTOCK,"left")
    ->where("tpengiriman_det.".COL_DOID, $rdata[COL_DOID])
    ->get(TBL_TPENGIRIMAN_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmKode'=> $det[COL_NMKODE],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'DOQty'=>$det[COL_DOQTY]
      );
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-L");

    $html = $this->load->view('admin/sales/form-delivery-print', array('DistItems'=>$arrdet,'data'=>$rdata), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Surat Jalan.pdf', 'I');
  }

  public function delivery_delete($id) {
    $res = $this->db->where(COL_DOID, $id)->delete(TBL_TPENGIRIMAN);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }

  public function invoice() {
    $data['title'] = "Faktur Penjualan";
    $this->template->load('main', 'admin/sales/index-invoice', $data);
  }

  public function invoice_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdCustomer = !empty($_POST['idCustomer'])?$_POST['idCustomer']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_PURCHDATE=>'desc');
    $orderables = array(null,COL_INVDATE,COL_NMCUSTOMER,COL_DONO,COL_INVNO,COL_INVDUE,null,COL_CREATEDON);
    $cols = array(COL_INVDATE,COL_NMCUSTOMER,COL_DONO,COL_INVNO,COL_INVDUE,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TFAKTUR.".".COL_CREATEDBY,"left")
    ->join(TBL_TPENGIRIMAN.' do','do.'.COL_DOID." = ".TBL_TFAKTUR.".".COL_DOID,"left")
    ->join(TBL_TPENJUALAN.' sal','sal.'.COL_SALESID." = do.".COL_SALESID,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = sal.".COL_IDCUSTOMER,"left")
    ->where(TBL_TFAKTUR.'.'.COL_DOID.' IS NOT NULL', NULL, FALSE)
    ->get(TBL_TFAKTUR);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMCUSTOMER) $item = 'sal.'.COL_NMCUSTOMER;
      if($item == COL_DONO) $item = 'do.'.COL_DONO;
      if($item == COL_CREATEDBY) $item = TBL_TFAKTUR.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    $this->db->where(TBL_TFAKTUR.'.'.COL_DOID.' IS NOT NULL', NULL, FALSE);
    if(!empty($dateFrom)) {
      $this->db->where(TBL_TFAKTUR.'.'.COL_INVDATE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TFAKTUR.'.'.COL_INVDATE.' <= ', $dateTo);
    }
    if(!empty($IdSupplier)) {
      $this->db->where(TBL_TPENJUALAN.'.'.COL_IDCUSTOMER, $IdSupplier);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tfaktur.*, cust.NmCustomer, do.DONo, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TFAKTUR.".".COL_CREATEDBY,"left")
    ->join(TBL_TPENGIRIMAN.' do','do.'.COL_DOID." = ".TBL_TFAKTUR.".".COL_DOID,"left")
    ->join(TBL_TPENJUALAN.' sal','sal.'.COL_SALESID." = do.".COL_SALESID,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = sal.".COL_IDCUSTOMER,"left")
    ->order_by(TBL_TFAKTUR.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TFAKTUR, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/sales/invoice-delete/'.$r[COL_INVID]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/sales/invoice-view/'.$r[COL_INVID]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/sales/invoice-print/'.$r[COL_INVID]).'" target="_blank" class="btn btn-xs btn-outline-success btn-print"><i class="fas fa-print"></i></a>',
        date('Y-m-d', strtotime($r[COL_INVDATE])),
        $r[COL_INVNO],
        $r[COL_NMCUSTOMER],
        $r[COL_DONO],
        $r[COL_INVDUE],
        $r[COL_CREATEDBY],
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function invoice_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrDet = array();
      $det = $this->input->post("DistItems");
      $data = array(
        COL_DOID => $this->input->post(COL_DOID),
        COL_INVNO => $this->input->post(COL_INVNO),
        COL_INVDATE => $this->input->post(COL_INVDATE),
        COL_INVDUE => $this->input->post(COL_INVDUE),
        COL_INVREMARKS => $this->input->post(COL_INVREMARKS),
        COL_INVREMARKSPAYMENT => $this->input->post(COL_INVREMARKSPAYMENT),
        COL_INVFREIGHT => $this->input->post(COL_INVFREIGHT),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_INVNO, $this->input->post(COL_INVNO))
        ->get(TBL_TFAKTUR)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->insert(TBL_TFAKTUR, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDist = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            $arrDet[] = array(
              COL_INVID=>$idDist,
              COL_IDSTOCK=>$s->IdStock,
              COL_INVQTY=>toNum($s->InvQty),
              COL_INVPRICE=>toNum($s->InvPrice),
              COL_INVDISC=>toNum($s->InvDisc),
              COL_INVTAX=>toNum($s->InvTax),
              COL_INVTOTAL=>toNum($s->InvTotal)
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TFAKTUR_DET, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['DistItems'] = json_encode(array());
      $this->load->view('admin/sales/form-invoice', $data);
    }
  }

  public function invoice_view($id) {
    $rdata = $this->db
    ->select('tfaktur.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TFAKTUR.".".COL_CREATEDBY,"left")
    ->where(COL_INVID, $id)
    ->get(TBL_TFAKTUR)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tfaktur_det.*, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tfaktur_det.".COL_IDSTOCK,"left")
    ->where(COL_INVID, $rdata[COL_INVID])
    ->get(TBL_TFAKTUR_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'InvQty'=>$det[COL_INVQTY],
        'InvPrice'=>$det[COL_INVPRICE],
        'InvTax'=>$det[COL_INVTAX],
        'InvDisc'=>$det[COL_INVDISC],
        'InvTotal'=>$det[COL_INVTOTAL]
      );
    }
    $this->load->view('admin/sales/form-invoice', array('DistItems'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true));
  }

  public function invoice_delete($id) {
    $res = $this->db->where(COL_INVID, $id)->delete(TBL_TFAKTUR);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }

  public function invoice_print($id) {
    $rdata = $this->db
    ->select('tfaktur.*, cust.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TFAKTUR.".".COL_CREATEDBY,"left")
    ->join(TBL_TPENGIRIMAN.' do','do.'.COL_DOID." = ".TBL_TFAKTUR.".".COL_DOID,"left")
    ->join(TBL_TPENJUALAN.' so','so.'.COL_SALESID." = do.".COL_SALESID,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = so.".COL_IDCUSTOMER,"left")
    ->where(COL_INVID, $id)
    ->get(TBL_TFAKTUR)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tfaktur_det.*, st.NmKode, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tfaktur_det.".COL_IDSTOCK,"left")
    ->where(COL_INVID, $rdata[COL_INVID])
    ->get(TBL_TFAKTUR_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmKode'=> $det[COL_NMKODE],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'InvQty'=>$det[COL_INVQTY],
        'InvPrice'=>$det[COL_INVPRICE],
        'InvTax'=>$det[COL_INVTAX],
        'InvDisc'=>$det[COL_INVDISC],
        'InvTotal'=>$det[COL_INVTOTAL]
      );
    }
    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-L");

    $html = $this->load->view('admin/sales/form-invoice-print', array('DistItems'=>$arrdet,'data'=>$rdata), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Faktur Penjualan.pdf', 'I');

  }
}
