<?php
class Report extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }
  }

  public function stock() {
    $data['title'] = "Laporan Barang";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $data['res'] = array();

    $prevQty = 0;
    $nmSatuan = '';

    if(!empty($_GET)) {
      $dateFrom = !empty($_GET['filterDateFrom'])?$_GET['filterDateFrom']:date('Y-m-01');
      $dateTo = !empty($_GET['filterDateTo'])?$_GET['filterDateTo']:date('Y-m-d');
      $idWarehouse = $_GET['IdWarehouse'];
      $idStock = $_GET['IdStock'];
      $q = @"
      select
      mvt.MoveDate,
      wh.NmWarehouse,
      st.NmStock,
      mvt.MoveQty,
      st.NmSatuan,
      coalesce(pur.PurchNo, d.DONo) as NmRefNo,
      coalesce(pur.PurchSupplier, sal.SalesCustomer, supp.NmSupplier, cust.NmCustomer) as CustSuppName,
      mvt.MoveRemarks
      from tbarang mvt
      left join mgudang wh on wh.Id = mvt.IdWarehouse
      left join mbarang st on st.Id = mvt.IdStock
      left join tpenerimaan r on r.ReceiptId = mvt.ReceiptId
      left join tpembelian pur on pur.PurchId = r.PurchId
      left join tpengiriman d on d.DOId = mvt.DOId
      left join tpenjualan sal on sal.SalesId = d.SalesId
      left join mpemasok supp on supp.Id = pur.IdSupplier
      left join mpelanggan cust on cust.Id = sal.IdCustomer
      where 1=1
      ";
      if(!empty($dateFrom)) {
        $q .= " and mvt.MoveDate >= '$dateFrom' ";
      }
      if(!empty($dateTo)) {
        $q .= " and mvt.MoveDate <= '$dateTo' ";
      }
      if(!empty($idWarehouse)) {
        $q .= " and mvt.IdWarehouse = $idWarehouse ";
      }
      if(!empty($idStock)) {
        $q .= " and mvt.IdStock = $idStock ";
      }
      $data['res'] = $this->db->query($q." order by mvt.MoveDate desc")->result_array();

      if(!empty($dateFrom) && !empty($idStock)) {
        $qPrev = @"
        select
        coalesce(sum(mvt.MoveQty), 0) as MoveQty,
        st.NmSatuan
        from tbarang mvt
        left join mbarang st on st.Id = mvt.IdStock
        where
          mvt.IdStock = $idStock
          and mvt.MoveDate <= '$dateFrom'
        ";
        //echo $qPrev;
        //exit();
        $resPrev = $this->db->query($qPrev)->row_array();
        if(!empty($resPrev)) {
          $prevQty = $resPrev[COL_MOVEQTY];
          $nmSatuan = $resPrev[COL_NMSATUAN];
        }
      }
      $data['filterDateFrom'] = $dateFrom;
      $data['filterDateTo'] = $dateTo;
      $data['IdWarehouse'] = $idWarehouse;
      $data['IdStock'] = $idStock;
    }

    $data['prevQty'] = $prevQty;
    $data['nmSatuan'] = $nmSatuan;

    if($cetak) $this->load->view('admin/report/stock-partial', $data);
    else $this->template->load('main', 'admin/report/stock', $data);
  }

  public function sales() {
    $data['title'] = "Laporan Penjualan";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $data['res'] = array();

    $prevQty = 0;
    $nmSatuan = '';

    if(!empty($_GET)) {
      $dateFrom = !empty($_GET['filterDateFrom'])?$_GET['filterDateFrom']:date('Y-m-01');
      $dateTo = !empty($_GET['filterDateTo'])?$_GET['filterDateTo']:date('Y-m-d');
      $idCustomer = !empty($_GET['IdCustomer'])?$_GET['IdCustomer']:null;
      $q = @"
      select
        sal.*,
        cust.NmCustomer,
        (select sum(det1.SalesTotal) from tpenjualan_det det1 where det1.SalesId = sal.SalesId) as TotalSales,
        (select sum(det2.InvTotal) from tpenjualan_inv det2 where det2.SalesId = sal.SalesId) as TotalInv
      from tpenjualan sal
      left join mpelanggan cust on cust.Id = sal.IdCustomer
      where 1=1
      ";
      if(!empty($dateFrom)) {
        $q .= " and sal.SalesDate >= '$dateFrom' ";
      }
      if(!empty($dateTo)) {
        $q .= " and sal.SalesDate <= '$dateTo' ";
      }
      if(!empty($idCustomer)) {
        $q .= " and sal.IdCustomer = $idCustomer ";
      }
      $data['res'] = $this->db->query($q." order by sal.SalesDate desc")->result_array();


      $data['filterDateFrom'] = $dateFrom;
      $data['filterDateTo'] = $dateTo;
      $data['IdCustomer'] = $idCustomer;
    }

    if($cetak) {
      $this->load->library('Mypdf');
      $mpdf = new Mypdf();

      $html = $this->load->view('admin/report/sales-partial', $data, TRUE);
      //echo $html;
      //return;
      $mpdf->pdf->WriteHTML($html);
      $mpdf->pdf->SetTitle($this->setting_web_name.' - Cetak Laporan Penjualan');
      $mpdf->pdf->Output('Faktur Penjualan.pdf', 'D');
      exit();
    }
    else $this->template->load('main', 'admin/report/sales', $data);
  }

  public function purchase() {
    $data['title'] = "Laporan Pembelian";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $data['res'] = array();

    $prevQty = 0;
    $nmSatuan = '';

    if(!empty($_GET)) {
      $dateFrom = !empty($_GET['filterDateFrom'])?$_GET['filterDateFrom']:date('Y-m-01');
      $dateTo = !empty($_GET['filterDateTo'])?$_GET['filterDateTo']:date('Y-m-d');
      $idCustomer = !empty($_GET['IdSupplier'])?$_GET['IdSupplier']:null;
      $q = @"
      select
        pur.*,
        supp.NmSupplier,
        (select sum(det1.PurchTotal) from tpembelian_det det1 where det1.PurchId = pur.PurchId) as TotalPurch,
        (select sum(det2.InvTotal) from tpembelian_inv det2 where det2.PurchId = pur.PurchId) as TotalInv
      from tpembelian pur
      left join mpemasok supp on supp.Id = pur.IdSupplier
      where 1=1
      ";
      if(!empty($dateFrom)) {
        $q .= " and pur.PurchDate >= '$dateFrom' ";
      }
      if(!empty($dateTo)) {
        $q .= " and pur.PurchDate <= '$dateTo' ";
      }
      /*if(!empty($idCustomer)) {
        $q .= " and pur.IdSupplier = $idCustomer ";
      }*/
      if(!empty($idCustomer)) {
        $q .= " and pur.PurchSupplier = '$idCustomer' ";
      }
      $data['res'] = $this->db->query($q." order by pur.PurchDate desc")->result_array();

      $data['filterDateFrom'] = $dateFrom;
      $data['filterDateTo'] = $dateTo;
      $data['IdSupplier'] = $idCustomer;
    }

    if($cetak) {
      $this->load->library('Mypdf');
      $mpdf = new Mypdf();

      $html = $this->load->view('admin/report/purchase-partial', $data, TRUE);
      //echo $html;
      //return;
      $mpdf->pdf->WriteHTML($html);
      $mpdf->pdf->SetTitle($this->setting_web_name.' - Cetak Laporan Pembelian');
      $mpdf->pdf->Output('Faktur Penjualan.pdf', 'D');
      exit();
    }
    else $this->template->load('main', 'admin/report/purchase', $data);
  }
}
?>
