<?php
class Purchase extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Pesanan Barang";
    $this->template->load('main', 'admin/purchase/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdSupplier = !empty($_POST['idSupplier'])?$_POST['idSupplier']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_PURCHDATE=>'desc');
    $orderables = array(null,COL_PURCHDATE,COL_NMSUPPLIER,COL_PURCHNO,COL_PURCHREMARKS,null,COL_CREATEDON);
    $cols = array(COL_PURCHDATE,COL_NMSUPPLIER,COL_PURCHNO,COL_PURCHREMARKS,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPEMBELIAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = ".TBL_TPEMBELIAN.".".COL_IDSUPPLIER,"left")
    ->get(TBL_TPEMBELIAN);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMSUPPLIER) $item = 'supp.'.COL_NMSUPPLIER;
      if($item == COL_CREATEDBY) $item = TBL_TPEMBELIAN.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TPEMBELIAN.'.'.COL_PURCHDATE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TPEMBELIAN.'.'.COL_PURCHDATE.' <= ', $dateTo);
    }
    if(!empty($IdSupplier)) {
      $this->db->where(TBL_TPEMBELIAN.'.'.COL_IDSUPPLIER, $IdSupplier);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tpembelian.*, supp.NmSupplier, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPEMBELIAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = ".TBL_TPEMBELIAN.".".COL_IDSUPPLIER,"left")
    ->order_by(TBL_TPEMBELIAN.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TPEMBELIAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/purchase/delete/'.$r[COL_PURCHID]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/purchase/view/'.$r[COL_PURCHID]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/purchase/purchase-print/'.$r[COL_PURCHID]).'" target="_blank" class="btn btn-xs btn-outline-success btn-print"><i class="fas fa-print"></i></a>',
        date('Y-m-d', strtotime($r[COL_PURCHDATE])),
        $r[COL_NMSUPPLIER],
        $r[COL_PURCHNO],
        $r[COL_PURCHREMARKS],
        $r[COL_CREATEDBY],
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrDet = array();
      $det = $this->input->post("DistItems");
      $data = array(
        COL_IDSUPPLIER => $this->input->post(COL_IDSUPPLIER),
        COL_PURCHNO => $this->input->post(COL_PURCHNO),
        COL_PURCHDATE => $this->input->post(COL_PURCHDATE),
        COL_PURCHADDR => $this->input->post(COL_PURCHADDR),
        COL_PURCHREMARKS => $this->input->post(COL_PURCHREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_PURCHNO, $this->input->post(COL_PURCHNO))
        ->get(TBL_TPEMBELIAN)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->insert(TBL_TPEMBELIAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDist = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            $arrDet[] = array(
              COL_PURCHID=>$idDist,
              COL_IDSTOCK=>$s->IdStock,
              COL_PURCHQTY=>toNum($s->PurchQty),
              COL_PURCHPRICE=>toNum($s->PurchPrice),
              COL_PURCHDISC=>toNum($s->PurchDisc),
              COL_PURCHTAX=>toNum($s->PurchTax),
              COL_PURCHTOTAL=>toNum($s->PurchTotal)
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TPEMBELIAN_DET, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['DistItems'] = json_encode(array());
      $this->load->view('admin/purchase/form', $data);
    }
  }

  public function view($id) {
    $rdata = $this->db
    ->select('tpembelian.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPEMBELIAN.".".COL_CREATEDBY,"left")
    ->where(COL_PURCHID, $id)
    ->get(TBL_TPEMBELIAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpembelian_det.*, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpembelian_det.".COL_IDSTOCK,"left")
    ->where(COL_PURCHID, $rdata[COL_PURCHID])
    ->get(TBL_TPEMBELIAN_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'PurchQty'=>$det[COL_PURCHQTY],
        'PurchPrice'=>$det[COL_PURCHPRICE],
        'PurchTax'=>$det[COL_PURCHTAX],
        'PurchTotal'=>$det[COL_PURCHTOTAL]
      );
    }
    $this->load->view('admin/purchase/form', array('DistItems'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true));
  }

  public function purchase_print($id) {
    $rdata = $this->db
    ->select('tpembelian.*, supp.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPEMBELIAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = tpembelian.".COL_IDSUPPLIER,"left")
    ->where(COL_PURCHID, $id)
    ->get(TBL_TPEMBELIAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpembelian_det.*, st.NmKode, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpembelian_det.".COL_IDSTOCK,"left")
    ->where(COL_PURCHID, $rdata[COL_PURCHID])
    ->get(TBL_TPEMBELIAN_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmKode'=> $det[COL_NMKODE],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'PurchQty'=>$det[COL_PURCHQTY],
        'PurchPrice'=>$det[COL_PURCHPRICE],
        'PurchTax'=>$det[COL_PURCHTAX],
        'PurchTotal'=>$det[COL_PURCHTOTAL]
      );
    }
    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-L");

    $html = $this->load->view('admin/purchase/print', array('DistItems'=>$arrdet,'data'=>$rdata), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Purchase Order.pdf', 'I');
  }

  public function delete($id) {
    $res = $this->db->where(COL_PURCHID, $id)->delete(TBL_TPEMBELIAN);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }

  public function receipt() {
    $data['title'] = "Penerimaan Barang";
    $this->template->load('main', 'admin/purchase/index-receipt', $data);
  }

  public function receipt_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdWarehouse = !empty($_POST['idWarehouse'])?$_POST['idWarehouse']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_PURCHDATE=>'desc');
    $orderables = array(null,COL_RECEIPTDATE,COL_NMWAREHOUSE,COL_PURCHNO,COL_RECEIPTREMARKS,null,COL_CREATEDON);
    $cols = array(COL_RECEIPTDATE,COL_NMWAREHOUSE,COL_PURCHNO,COL_RECEIPTREMARKS,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENERIMAAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MGUDANG.' wh','wh.'.COL_ID." = ".TBL_TPENERIMAAN.".".COL_IDWAREHOUSE,"left")
    ->join(TBL_TPEMBELIAN.' pur','pur.'.COL_PURCHID." = ".TBL_TPENERIMAAN.".".COL_PURCHID,"left")
    ->get(TBL_TPENERIMAAN);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMWAREHOUSE) $item = 'wh.'.COL_NMWAREHOUSE;
      if($item == COL_CREATEDBY) $item = TBL_TPENERIMAAN.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TPENERIMAAN.'.'.COL_RECEIPTDATE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TPENERIMAAN.'.'.COL_RECEIPTDATE.' <= ', $dateTo);
    }
    if(!empty($IdWarehouse)) {
      $this->db->where(TBL_TPENERIMAAN.'.'.COL_IDWAREHOUSE, $IdWarehouse);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tpenerimaan.*, pur.PurchNo, wh.NmWarehouse, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENERIMAAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MGUDANG.' wh','wh.'.COL_ID." = ".TBL_TPENERIMAAN.".".COL_IDWAREHOUSE,"left")
    ->join(TBL_TPEMBELIAN.' pur','pur.'.COL_PURCHID." = ".TBL_TPENERIMAAN.".".COL_PURCHID,"left")
    ->order_by(TBL_TPENERIMAAN.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TPENERIMAAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/purchase/receipt-delete/'.$r[COL_RECEIPTID]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/purchase/receipt-view/'.$r[COL_RECEIPTID]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/purchase/receipt-print/'.$r[COL_RECEIPTID]).'" target="_blank" class="btn btn-xs btn-outline-success btn-print"><i class="fas fa-print"></i></a>',
        date('Y-m-d', strtotime($r[COL_RECEIPTDATE])),
        $r[COL_NMWAREHOUSE],
        $r[COL_PURCHNO],
        $r[COL_RECEIPTREMARKS],
        $r[COL_CREATEDBY],
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function receipt_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrDet = array();
      $arrMovement = array();
      $det = $this->input->post("DistItems");
      $data = array(
        COL_PURCHID => $this->input->post(COL_PURCHID),
        COL_IDWAREHOUSE => $this->input->post(COL_IDWAREHOUSE),
        COL_RECEIPTDATE => $this->input->post(COL_RECEIPTDATE),
        COL_RECEIPTREMARKS => $this->input->post(COL_RECEIPTREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPENERIMAAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDist = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            $arrDet[] = array(
              COL_RECEIPTID=>$idDist,
              COL_IDSTOCK=>$s->IdStock,
              COL_RECEIPTQTY=>toNum($s->ReceiptQty)
            );

            $arrMovement[] = array(
              COL_RECEIPTID=>$idDist,
              COL_IDWAREHOUSE=>$data[COL_IDWAREHOUSE],
              COL_IDSTOCK=>$s->IdStock,
              COL_MOVETYPE=>'PUR',
              COL_MOVEDATE=>$data[COL_RECEIPTDATE],
              COL_MOVEQTY=>toNum($s->ReceiptQty),
              COL_MOVEREMARKS=>$data[COL_RECEIPTREMARKS],
              COL_CREATEDBY => $ruser[COL_USERNAME],
              COL_CREATEDON => date('Y-m-d H:i:s')
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TPENERIMAAN_DET, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $res = $this->db->insert_batch(TBL_TBARANG, $arrMovement);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['DistItems'] = json_encode(array());
      $this->load->view('admin/purchase/form-receipt', $data);
    }
  }

  public function receipt_view($id) {
    $rdata = $this->db
    ->select('tpenerimaan.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENERIMAAN.".".COL_CREATEDBY,"left")
    ->where(COL_RECEIPTID, $id)
    ->get(TBL_TPENERIMAAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpenerimaan_det.*, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpenerimaan_det.".COL_IDSTOCK,"left")
    ->where(COL_RECEIPTID, $rdata[COL_RECEIPTID])
    ->get(TBL_TPENERIMAAN_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'ReceiptQty'=>$det[COL_RECEIPTQTY]
      );
    }
    $this->load->view('admin/purchase/form-receipt', array('DistItems'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true));
  }

  public function receipt_print($id) {
    $rdata = $this->db
    ->select('tpenerimaan.*, supp.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENERIMAAN.".".COL_CREATEDBY,"left")
    ->join(TBL_TPEMBELIAN.' po','po.'.COL_PURCHID." = tpenerimaan.".COL_PURCHID,"left")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = po.".COL_IDSUPPLIER,"left")
    ->where(COL_RECEIPTID, $id)
    ->get(TBL_TPENERIMAAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpenerimaan_det.*, st.NmKode, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpenerimaan_det.".COL_IDSTOCK,"left")
    ->where(COL_RECEIPTID, $rdata[COL_RECEIPTID])
    ->get(TBL_TPENERIMAAN_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmKode'=> $det[COL_NMKODE],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'ReceiptQty'=>$det[COL_RECEIPTQTY]
      );
    }
    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-L");

    $html = $this->load->view('admin/purchase/form-receipt-print', array('DistItems'=>$arrdet,'data'=>$rdata), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Receipt Order.pdf', 'I');
  }

  public function receipt_delete($id) {
    $res = $this->db->where(COL_RECEIPTID, $id)->delete(TBL_TPENERIMAAN);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }

  public function invoice() {
    $data['title'] = "Faktur Pembelian";
    $this->template->load('main', 'admin/purchase/index-invoice', $data);
  }

  public function invoice_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdSupplier = !empty($_POST['idSupplier'])?$_POST['idSupplier']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_PURCHDATE=>'desc');
    $orderables = array(null,COL_INVDATE,COL_NMSUPPLIER,COL_PURCHNO,COL_INVNO,COL_INVDUE,null,COL_CREATEDON);
    $cols = array(COL_INVDATE,COL_NMSUPPLIER,COL_PURCHNO,COL_INVNO,COL_INVDUE,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TFAKTUR.".".COL_CREATEDBY,"left")
    ->join(TBL_TPENERIMAAN.' recv','recv.'.COL_RECEIPTID." = ".TBL_TFAKTUR.".".COL_RECEIPTID,"left")
    ->join(TBL_TPEMBELIAN.' pur','pur.'.COL_PURCHID." = recv.".COL_PURCHID,"left")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = pur.".COL_IDSUPPLIER,"left")
    ->where(TBL_TFAKTUR.'.'.COL_RECEIPTID.' is NOT NULL', NULL, FALSE)
    ->get(TBL_TFAKTUR);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMSUPPLIER) $item = 'supp.'.COL_NMSUPPLIER;
      if($item == COL_PURCHNO) $item = 'pur.'.COL_PURCHNO;
      if($item == COL_CREATEDBY) $item = TBL_TFAKTUR.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    $this->db->where(TBL_TFAKTUR.'.'.COL_RECEIPTID.' is NOT NULL', NULL, FALSE);
    if(!empty($dateFrom)) {
      $this->db->where(TBL_TFAKTUR.'.'.COL_INVDATE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TFAKTUR.'.'.COL_INVDATE.' <= ', $dateTo);
    }
    if(!empty($IdSupplier)) {
      $this->db->where(TBL_TPEMBELIAN.'.'.COL_IDSUPPLIER, $IdSupplier);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tfaktur.*, supp.NmSupplier, pur.PurchNo, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TFAKTUR.".".COL_CREATEDBY,"left")
    ->join(TBL_TPENERIMAAN.' recv','recv.'.COL_RECEIPTID." = ".TBL_TFAKTUR.".".COL_RECEIPTID,"left")
    ->join(TBL_TPEMBELIAN.' pur','pur.'.COL_PURCHID." = recv.".COL_PURCHID,"left")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = pur.".COL_IDSUPPLIER,"left")
    ->order_by(TBL_TFAKTUR.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TFAKTUR, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/purchase/invoice-delete/'.$r[COL_INVID]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/purchase/invoice-view/'.$r[COL_INVID]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i></a>&nbsp;'.
        '<a href="'.site_url('admin/purchase/invoice-print/'.$r[COL_INVID]).'" target="_blank" class="btn btn-xs btn-outline-success btn-print"><i class="fas fa-print"></i></a>',
        date('Y-m-d', strtotime($r[COL_INVDATE])),
        $r[COL_INVNO],
        $r[COL_NMSUPPLIER],
        $r[COL_PURCHNO],
        $r[COL_INVDUE],
        $r[COL_CREATEDBY],
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function invoice_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrDet = array();
      $det = $this->input->post("DistItems");
      $data = array(
        COL_RECEIPTID => $this->input->post(COL_RECEIPTID),
        COL_INVNO => $this->input->post(COL_INVNO),
        COL_INVDATE => $this->input->post(COL_INVDATE),
        COL_INVDUE => $this->input->post(COL_INVDUE),
        COL_INVREMARKS => $this->input->post(COL_INVREMARKS),
        COL_INVREMARKSPAYMENT => $this->input->post(COL_INVREMARKSPAYMENT),
        COL_INVFREIGHT => $this->input->post(COL_INVFREIGHT),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_INVNO, $this->input->post(COL_INVNO))
        ->get(TBL_TFAKTUR)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->insert(TBL_TFAKTUR, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDist = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            $arrDet[] = array(
              COL_INVID=>$idDist,
              COL_IDSTOCK=>$s->IdStock,
              COL_INVQTY=>toNum($s->InvQty),
              COL_INVPRICE=>toNum($s->InvPrice),
              COL_INVDISC=>toNum($s->InvDisc),
              COL_INVTAX=>toNum($s->InvTax),
              COL_INVTOTAL=>toNum($s->InvTotal)
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TFAKTUR_DET, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['DistItems'] = json_encode(array());
      $this->load->view('admin/purchase/form-invoice', $data);
    }
  }

  public function invoice_view($id) {
    $rdata = $this->db
    ->select('tfaktur.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TFAKTUR.".".COL_CREATEDBY,"left")
    ->where(COL_INVID, $id)
    ->get(TBL_TFAKTUR)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tfaktur_det.*, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tfaktur_det.".COL_IDSTOCK,"left")
    ->where(COL_INVID, $rdata[COL_INVID])
    ->get(TBL_TFAKTUR_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'InvQty'=>$det[COL_INVQTY],
        'InvPrice'=>$det[COL_INVPRICE],
        'InvTax'=>$det[COL_INVTAX],
        'InvDisc'=>$det[COL_INVDISC],
        'InvTotal'=>$det[COL_INVTOTAL]
      );
    }
    $this->load->view('admin/purchase/form-invoice', array('DistItems'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true));
  }

  public function invoice_delete($id) {
    $res = $this->db->where(COL_INVID, $id)->delete(TBL_TFAKTUR);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }

  public function invoice_print($id) {
    $rdata = $this->db
    ->select('tfaktur.*, supp.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TFAKTUR.".".COL_CREATEDBY,"left")
    ->join(TBL_TPENERIMAAN.' rec','rec.'.COL_RECEIPTID." = ".TBL_TFAKTUR.".".COL_RECEIPTID,"left")
    ->join(TBL_TPEMBELIAN.' po','po.'.COL_PURCHID." = rec.".COL_PURCHID,"left")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = po.".COL_IDSUPPLIER,"left")
    ->where(COL_INVID, $id)
    ->get(TBL_TFAKTUR)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tfaktur_det.*, st.NmKode, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tfaktur_det.".COL_IDSTOCK,"left")
    ->where(COL_INVID, $rdata[COL_INVID])
    ->get(TBL_TFAKTUR_DET)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmKode'=> $det[COL_NMKODE],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'InvQty'=>$det[COL_INVQTY],
        'InvPrice'=>$det[COL_INVPRICE],
        'InvTax'=>$det[COL_INVTAX],
        'InvDisc'=>$det[COL_INVDISC],
        'InvTotal'=>$det[COL_INVTOTAL]
      );
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-L");

    $html = $this->load->view('admin/purchase/form-invoice-print', array('DistItems'=>$arrdet,'data'=>$rdata), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Faktur Pembelian.pdf', 'I');
  }
}
