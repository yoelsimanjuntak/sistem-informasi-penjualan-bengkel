<?php
class Master extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashbdoard');
    }
  }

  public function customer_index() {
    $data['title'] = "Customer";
    $this->db->select('*,uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MPELANGGAN.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MPELANGGAN.".".COL_UPDATEDBY,"left");
    $this->db->order_by(COL_NMCUSTOMER, 'asc');
    $data['res'] = $this->db->get(TBL_MPELANGGAN)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/index-customer', $data);
  }

  public function customer_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMCUSTOMER => $this->input->post(COL_NMCUSTOMER),
        COL_NMNAMA => $this->input->post(COL_NMNAMA),
        COL_NMNOHP => $this->input->post(COL_NMNOHP),
        COL_NMALAMAT => $this->input->post(COL_NMALAMAT),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MPELANGGAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function customer_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMCUSTOMER => $this->input->post(COL_NMCUSTOMER),
        COL_NMNAMA => $this->input->post(COL_NMNAMA),
        COL_NMNOHP => $this->input->post(COL_NMNOHP),
        COL_NMALAMAT => $this->input->post(COL_NMALAMAT),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_ID, $id)->update(TBL_MPELANGGAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function customer_delete() {
    $ruser = GetLoggedUser();
    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $res = $this->db
        ->where(COL_ID, $datum)
        ->delete(TBL_MPELANGGAN);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function supplier_index() {
    $data['title'] = "Supplier";
    $this->db->select('*,uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MPEMASOK.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MPEMASOK.".".COL_UPDATEDBY,"left");
    $this->db->order_by(COL_NMSUPPLIER, 'asc');
    $data['res'] = $this->db->get(TBL_MPEMASOK)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/index-supplier', $data);
  }

  public function supplier_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMSUPPLIER => $this->input->post(COL_NMSUPPLIER),
        COL_NMNAMA => $this->input->post(COL_NMNAMA),
        COL_NMNOHP => $this->input->post(COL_NMNOHP),
        COL_NMALAMAT => $this->input->post(COL_NMALAMAT),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MPEMASOK, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function supplier_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMSUPPLIER => $this->input->post(COL_NMSUPPLIER),
        COL_NMNAMA => $this->input->post(COL_NMNAMA),
        COL_NMNOHP => $this->input->post(COL_NMNOHP),
        COL_NMALAMAT => $this->input->post(COL_NMALAMAT),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_ID, $id)->update(TBL_MPEMASOK, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function supplier_delete() {
    $ruser = GetLoggedUser();
    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $res = $this->db
        ->where(COL_ID, $datum)
        ->delete(TBL_MPEMASOK);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function warehouse_index() {
    $data['title'] = "Gudang";
    $this->db->select('*,uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MGUDANG.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MGUDANG.".".COL_UPDATEDBY,"left");
    $this->db->order_by(COL_NMWAREHOUSE, 'asc');
    $data['res'] = $this->db->get(TBL_MGUDANG)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/warehouse', $data);
  }

  public function warehouse_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMWAREHOUSE => $this->input->post(COL_NMWAREHOUSE),
        COL_NMALAMAT => $this->input->post(COL_NMALAMAT),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MGUDANG, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function warehouse_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMWAREHOUSE => $this->input->post(COL_NMWAREHOUSE),
        COL_NMALAMAT => $this->input->post(COL_NMALAMAT),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_ID, $id)->update(TBL_MGUDANG, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function warehouse_delete($id) {
    $rdata = $this->db->where(COL_ID, $id)->get(TBL_MGUDANG)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }
    $res = $this->db->where(COL_ID, $id)->delete(TBL_MGUDANG);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Data gudang <strong>'.$rdata[COL_NMWAREHOUSE].'</strong> berhasil dihapus.');
  }

  public function stock_index() {
    $data['title'] = "Barang";
    /*$this->db->select('mbarang.*, uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MBARANG.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MBARANG.".".COL_UPDATEDBY,"left");
    $this->db->order_by(COL_NMSTOCK, 'asc');
    $data['res'] = $this->db->get(TBL_MBARANG)->result_array();*/
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/stock', $data);
  }

  public function stock_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterKategori = !empty($_POST['filterKategori'])?$_POST['filterKategori']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_NMSTOCK=>'asc');
    $orderables = array(null,COL_NMKATEGORI,COL_NMSTOCK,COL_NMSATUAN,null,COL_CREATEDON);
    $cols = array(COL_NMKATEGORI, COL_NMSTOCK, COL_NMSATUAN);

    $queryAll = $this->db
    ->join(TBL_MKATEGORI.' kat','kat.'.COL_ID." = ".TBL_MBARANG.".".COL_IDKATEGORI,"left")
    ->where_in('kat.'.COL_ISSTOCK, 1)
    ->get(TBL_MBARANG);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($filterKategori)) {
      $this->db->where(TBL_MBARANG.'.'.COL_IDKATEGORI, $filterKategori);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('mbarang.*, kat.NmKategori, uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MBARANG.".".COL_CREATEDBY,"left")
    ->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MBARANG.".".COL_UPDATEDBY,"left")
    ->join(TBL_MKATEGORI.' kat','kat.'.COL_ID." = ".TBL_MBARANG.".".COL_IDKATEGORI,"left")
    ->where_in('kat.'.COL_ISSTOCK, 1)
    ->order_by(COL_NMSTOCK, 'asc')
    ->get_compiled_select(TBL_MBARANG, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('admin/master/stock-delete/'.$r[COL_ID]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('admin/master/stock-edit/'.$r[COL_ID]).'" class="btn btn-xs btn-outline-primary modal-popup-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui" data-name="'.$r[COL_NMSTOCK].'" data-kategori="'.$r[COL_IDKATEGORI].'" data-satuan="'.$r[COL_NMSATUAN].'" data-harga="'.$r[COL_HARGA].'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';
      $data[] = array(
        $htmlBtn,
        $r[COL_NMKATEGORI],
        $r[COL_NMSTOCK],
        $r[COL_NMSATUAN],
        'Rp. '.number_format($r[COL_HARGA]),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function stock_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $isInitStock = $this->input->post('InitStock');
      $data = array(
        COL_NMSTOCK => $this->input->post(COL_NMSTOCK),
        COL_IDKATEGORI => $this->input->post(COL_IDKATEGORI),
        COL_NMSATUAN => $this->input->post(COL_NMSATUAN),
        COL_HARGA => toNum($this->input->post(COL_HARGA)),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MBARANG, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idStock = $this->db->insert_id();
        if($isInitStock && !empty($this->input->post('InitStockNum')) && !empty($this->input->post('InitStockWH'))) {
          $res = $this->db->insert(TBL_TBARANG, array(
            COL_IDWAREHOUSE=>$this->input->post('InitStockWH'),
            COL_IDSTOCK=>$idStock,
            COL_MOVETYPE=>'PUR',
            COL_MOVEDATE=>date('Y-m-d', strtotime($this->input->post('InitStockDate'))),
            COL_MOVEQTY=>toNum($this->input->post('InitStockNum')),
            COL_MOVEREMARKS=>'STOCK AWAL',
            COL_CREATEDBY => $ruser[COL_USERNAME],
            COL_CREATEDON => date('Y-m-d H:i:s')
          ));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Data barang <strong>'.$data[COL_NMSTOCK].'</strong> berhasil ditambahkan.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function stock_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMSTOCK => $this->input->post(COL_NMSTOCK),
        COL_IDKATEGORI => $this->input->post(COL_IDKATEGORI),
        COL_NMSATUAN => $this->input->post(COL_NMSATUAN),
        COL_HARGA => toNum($this->input->post(COL_HARGA)),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_ID, $id)->update(TBL_MBARANG, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Data barang <strong>'.$data[COL_NMSTOCK].'</strong> berhasil diubah.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function stock_delete($id) {
    $rdata = $this->db->where(COL_ID, $id)->get(TBL_MBARANG)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }
    $rtrans = $this->db
    ->where(COL_IDSTOCK,$id)
    ->where('(DOId is not null or ReceiptId is not null)')
    ->get(TBL_TBARANG)
    ->row_array();
    if($rtrans) {
      ShowJsonError('<strong>'.$rdata[COL_NMSTOCK].'</strong> tidak dapat dihapus karena sudah digunakan dalam transaksi penjualan / pembelian!');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(array(COL_IDSTOCK=>$id, COL_DOID=>null,COL_RECEIPTID=>null,COL_MOVETYPE=>'PUR'))->delete(TBL_TBARANG);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db->where(COL_ID, $id)->delete(TBL_MBARANG);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Data barang <strong>'.$rdata[COL_NMSTOCK].'</strong> berhasil dihapus.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }

    ShowJsonSuccess('Data barang <strong>'.$rdata[COL_NMSTOCK].'</strong> berhasil dihapus.');
  }
}
