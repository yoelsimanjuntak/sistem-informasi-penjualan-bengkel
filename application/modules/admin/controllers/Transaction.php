<?php
class Transaction extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }
  }

  public function receipt() {
    $data['title'] = "Pembelian";
    $this->template->load('main', 'admin/transaction/receipt', $data);
  }

  public function receipt_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_PURCHDATE=>'desc');
    $orderables = array(null,COL_PURCHDATE,COL_PURCHSUPPLIER,COL_PURCHNO,COL_PURCHREMARKS,COL_CREATEDON);
    $cols = array(COL_PURCHDATE,COL_PURCHSUPPLIER,COL_PURCHNO,COL_PURCHREMARKS);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENERIMAAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MGUDANG.' wh','wh.'.COL_ID." = ".TBL_TPENERIMAAN.".".COL_IDWAREHOUSE,"left")
    ->join(TBL_TPEMBELIAN.' pur','pur.'.COL_PURCHID." = ".TBL_TPENERIMAAN.".".COL_PURCHID,"left")
    ->get(TBL_TPENERIMAAN);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMWAREHOUSE) $item = 'wh.'.COL_NMWAREHOUSE;
      if($item == COL_CREATEDBY) $item = TBL_TPENERIMAAN.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TPENERIMAAN.'.'.COL_RECEIPTDATE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TPENERIMAAN.'.'.COL_RECEIPTDATE.' <= ', $dateTo);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('
      pur.*,
      pur.PurchNo,
      wh.NmWarehouse,
      uc.Nm_FullName as Nm_CreatedBy,
      (select sum(det1.PurchTotal) from tpembelian_det det1 where det1.PurchId = pur.PurchId) as TotalPurch,
      (select sum(det2.InvTotal) from tpembelian_inv det2 where det2.PurchId = pur.PurchId) as TotalInv
    ')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENERIMAAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MGUDANG.' wh','wh.'.COL_ID." = ".TBL_TPENERIMAAN.".".COL_IDWAREHOUSE,"left")
    ->join(TBL_TPEMBELIAN.' pur','pur.'.COL_PURCHID." = ".TBL_TPENERIMAAN.".".COL_PURCHID,"inner")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = pur.".COL_IDSUPPLIER,"left")
    ->order_by(TBL_TPENERIMAAN.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TPENERIMAAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlOpt = @'
      <div class="btn-group">
        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <i class="fas fa-list"></i>&nbsp;OPSI
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(68px, -2px, 0px);">
          <a class="dropdown-item text-sm text-danger btn-action" href="'.site_url('admin/transaction/receipt-delete/'.$r[COL_PURCHID]).'"><i class="far fa-times-circle"></i>&nbsp;HAPUS</a>
          <a class="dropdown-item text-sm text-success btn-popup-form" href="'.site_url('admin/transaction/receipt-edit/'.$r[COL_PURCHID]).'"><i class="far fa-pen-square"></i>&nbsp;UBAH</a>
          <a class="dropdown-item text-sm text-info btn-popup-form" href="'.site_url('admin/transaction/receipt-view/'.$r[COL_PURCHID]).'"><i class="far fa-info-circle"></i>&nbsp;INFO</a>
          <a class="dropdown-item text-sm text-primary btn-popup-form" href="'.site_url('admin/transaction/receipt-inv/'.$r[COL_PURCHID]).'"><i class="far fa-info-circle"></i>&nbsp;PEMBAYARAN</a>
        </div>
      </div>
      ';

      $data[] = array(
        $htmlOpt,
        date('Y-m-d', strtotime($r[COL_PURCHDATE])),
        '<p class="text-sm m-0">'.$r[COL_PURCHSUPPLIER].'<br />No. <strong>'.$r[COL_PURCHNO].'</strong></p>',
        $r[COL_NMWAREHOUSE],
        '<span class="pull-left">Rp. </span>'.number_format($r['TotalPurch']),
        '<span class="pull-left">Rp. </span>'.number_format($r['TotalInv']),
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function receipt_add() {
    $ruser = GetLoggedUser();
    if (!empty($_POST)) {
      $arrDetPurch = array();
      $arrDetReceipt = array();
      $arrMovement = array();
      $det = $this->input->post("DistItems");
      $dataPurch = array(
        COL_IDSUPPLIER => $this->input->post(COL_IDSUPPLIER),
        COL_PURCHSUPPLIER => $this->input->post(COL_PURCHSUPPLIER),
        COL_PURCHNO => $this->input->post(COL_PURCHNO),
        COL_PURCHDATE => $this->input->post(COL_PURCHDATE),
        COL_PURCHADDR => $this->input->post(COL_PURCHADDR),
        COL_PURCHREMARKS => $this->input->post(COL_PURCHREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );


      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_PURCHNO, $this->input->post(COL_PURCHNO))
        ->get(TBL_TPEMBELIAN)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->insert(TBL_TPEMBELIAN, $dataPurch);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idPurch = $this->db->insert_id();
        $dataReceipt = array(
          COL_PURCHID => $idPurch,
          COL_IDWAREHOUSE => $this->input->post(COL_IDWAREHOUSE),
          COL_RECEIPTDATE => $this->input->post(COL_PURCHDATE),
          COL_RECEIPTREMARKS => $this->input->post(COL_PURCHREMARKS),

          COL_CREATEDBY => $ruser[COL_USERNAME],
          COL_CREATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->insert(TBL_TPENERIMAAN, $dataReceipt);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
        $idReceipt = $this->db->insert_id();

        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            $arrDetPurch[] = array(
              COL_PURCHID=>$idPurch,
              COL_IDSTOCK=>$s->IdStock,
              COL_PURCHQTY=>toNum($s->PurchQty),
              COL_PURCHPRICE=>toNum($s->PurchPrice),
              COL_PURCHDISC=>toNum($s->PurchDisc),
              COL_PURCHTAX=>toNum($s->PurchTax),
              COL_PURCHTOTAL=>toNum($s->PurchTotal)
            );

            $arrDetReceipt[] = array(
              COL_RECEIPTID=>$idReceipt,
              COL_IDSTOCK=>$s->IdStock,
              COL_RECEIPTQTY=>toNum($s->PurchQty),
            );

            $arrMovement[] = array(
              COL_RECEIPTID=>$idReceipt,
              COL_IDWAREHOUSE=>$dataReceipt[COL_IDWAREHOUSE],
              COL_IDSTOCK=>$s->IdStock,
              COL_MOVETYPE=>'PUR',
              COL_MOVEDATE=>$dataReceipt[COL_RECEIPTDATE],
              COL_MOVEQTY=>toNum($s->PurchQty),
              COL_MOVEREMARKS=>$dataPurch[COL_PURCHREMARKS],
              COL_CREATEDBY => $ruser[COL_USERNAME],
              COL_CREATEDON => date('Y-m-d H:i:s')
            );
          }
        }

        if(!empty($arrDetPurch) && !empty($arrDetReceipt) && !empty($arrMovement)) {
          $res = $this->db->insert_batch(TBL_TPEMBELIAN_DET, $arrDetPurch);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
          $res = $this->db->insert_batch(TBL_TPENERIMAAN_DET, $arrDetReceipt);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
          $res = $this->db->insert_batch(TBL_TBARANG, $arrMovement);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menginput data pembelian.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    }  else {
      $data['DistItems'] = json_encode(array());
      $this->load->view('admin/transaction/receipt-form', $data);
    }
  }

  public function receipt_edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->select('tpembelian.*, uc.Nm_FullName as Nm_CreatedBy, ue.Nm_FullName as Nm_UpdatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPEMBELIAN.".".COL_CREATEDBY,"left")
    ->join(TBL__USERINFORMATION.' ue','ue.'.COL_USERNAME." = ".TBL_TPEMBELIAN.".".COL_UPDATEDBY,"left")
    ->where(COL_PURCHID, $id)
    ->get(TBL_TPEMBELIAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }
    $rreceipt = $this->db
    ->where(COL_PURCHID, $id)
    ->get(TBL_TPENERIMAAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $rinv = $this->db
    ->where(COL_PURCHID, $id)
    ->order_by(COL_INVDATE, 'asc')
    ->order_by(COL_CREATEDON, 'asc')
    ->get(TBL_TPEMBELIAN_INV)
    ->result_array();

    if (!empty($_POST)) {
      $idPurch = $id;
      $idReceipt = $rreceipt[COL_RECEIPTID];

      $arrDetPurch = array();
      $arrDetReceipt = array();
      $arrMovement = array();
      $det = $this->input->post("DistItems");
      $dataPurch = array(
        COL_IDSUPPLIER => $this->input->post(COL_IDSUPPLIER),
        COL_PURCHSUPPLIER => $this->input->post(COL_PURCHSUPPLIER),
        COL_PURCHNO => $this->input->post(COL_PURCHNO),
        COL_PURCHDATE => $this->input->post(COL_PURCHDATE),
        COL_PURCHADDR => $this->input->post(COL_PURCHADDR),
        COL_PURCHREMARKS => $this->input->post(COL_PURCHREMARKS),

        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );
      $dataReceipt = array(
        COL_IDWAREHOUSE => $this->input->post(COL_IDWAREHOUSE),
        COL_RECEIPTDATE => $this->input->post(COL_PURCHDATE),
        COL_RECEIPTREMARKS => $this->input->post(COL_PURCHREMARKS),

        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_PURCHID.' != ', $id)
        ->where(COL_PURCHNO, $this->input->post(COL_PURCHNO))
        ->get(TBL_TPEMBELIAN)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->where(COL_PURCHID, $id)->update(TBL_TPEMBELIAN, $dataPurch);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
        $res = $this->db->where(COL_PURCHID, $id)->update(TBL_TPENERIMAAN, $dataReceipt);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if(empty($rinv)) {
          $res = $this->db->where(COL_PURCHID, $idPurch)->delete(TBL_TPEMBELIAN_DET);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $res = $this->db->where(COL_RECEIPTID, $idReceipt)->delete(TBL_TPENERIMAAN_DET);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $res = $this->db->where(COL_RECEIPTID, $idReceipt)->delete(TBL_TBARANG);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          if(!empty($det)) {
            $det = json_decode(urldecode($det));
            foreach($det as $s) {
              $arrDetPurch[] = array(
                COL_PURCHID=>$idPurch,
                COL_IDSTOCK=>$s->IdStock,
                COL_PURCHQTY=>toNum($s->PurchQty),
                COL_PURCHPRICE=>toNum($s->PurchPrice),
                COL_PURCHDISC=>toNum($s->PurchDisc),
                COL_PURCHTAX=>toNum($s->PurchTax),
                COL_PURCHTOTAL=>toNum($s->PurchTotal)
              );

              $arrDetReceipt[] = array(
                COL_RECEIPTID=>$idReceipt,
                COL_IDSTOCK=>$s->IdStock,
                COL_RECEIPTQTY=>toNum($s->PurchQty),
              );

              $arrMovement[] = array(
                COL_RECEIPTID=>$idReceipt,
                COL_IDWAREHOUSE=>$dataReceipt[COL_IDWAREHOUSE],
                COL_IDSTOCK=>$s->IdStock,
                COL_MOVETYPE=>'PUR',
                COL_MOVEDATE=>$dataReceipt[COL_RECEIPTDATE],
                COL_MOVEQTY=>toNum($s->PurchQty),
                COL_MOVEREMARKS=>$dataPurch[COL_PURCHREMARKS],
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
              );
            }
          }

          if(!empty($arrDetPurch) && !empty($arrDetReceipt) && !empty($arrMovement)) {
            $res = $this->db->insert_batch(TBL_TPEMBELIAN_DET, $arrDetPurch);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }
            $res = $this->db->insert_batch(TBL_TPENERIMAAN_DET, $arrDetReceipt);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }
            $res = $this->db->insert_batch(TBL_TBARANG, $arrMovement);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }
          } else {
            throw new Exception('Item tidak boleh kosong.');
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil mengubah data pembelian.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $arrdet = array();
      $rdet = $this->db
      ->select('tpembelian_det.*, st.NmStock, st.NmSatuan')
      ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpembelian_det.".COL_IDSTOCK,"left")
      ->where(COL_PURCHID, $id)
      ->get(TBL_TPEMBELIAN_DET)
      ->result_array();
      foreach($rdet as $det) {
        $arrdet[] = array(
          'IdStock'=> $det[COL_IDSTOCK],
          'NmStock'=> $det[COL_NMSTOCK],
          'NmSatuan'=> $det[COL_NMSATUAN],
          'PurchQty'=>$det[COL_PURCHQTY],
          'PurchPrice'=>$det[COL_PURCHPRICE],
          'PurchDisc'=>$det[COL_PURCHDISC],
          'PurchTax'=>$det[COL_PURCHTAX],
          'PurchTotal'=>$det[COL_PURCHTOTAL]
        );
      }
      $data['data'] = $rdata;
      $data['DistItems'] = json_encode($arrdet);
      $data['disabled'] = !empty($rinv);
      $this->load->view('admin/transaction/receipt-form', $data);
    }
  }

  public function receipt_delete($id) {
    $rdata = $this->db->where(COL_PURCHID, $id)->get(TBL_TPEMBELIAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_PURCHID, $id)->delete(TBL_TPEMBELIAN);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }
      $res = $this->db->query("delete from tbarang where ReceiptId in (select ReceiptId from tpenerimaan where PurchId = $id)");
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
    $this->db->trans_commit();
    ShowJsonSuccess('Data pembelian no. <strong>'.$rdata[COL_PURCHNO].'</strong> berhasil dihapus.');
    exit();
  }

  public function receipt_view($id) {
    $rdata = $this->db
    ->select('pur.*, pur.PurchNo, wh.NmWarehouse, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENERIMAAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MGUDANG.' wh','wh.'.COL_ID." = ".TBL_TPENERIMAAN.".".COL_IDWAREHOUSE,"left")
    ->join(TBL_TPEMBELIAN.' pur','pur.'.COL_PURCHID." = ".TBL_TPENERIMAAN.".".COL_PURCHID,"left")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = pur.".COL_IDSUPPLIER,"left")
    ->where('pur.'.COL_PURCHID, $id)
    ->get(TBL_TPENERIMAAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpembelian_det.*, st.NmStock, st.NmSatuan')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpembelian_det.".COL_IDSTOCK,"left")
    ->where(COL_PURCHID, $rdata[COL_PURCHID])
    ->get(TBL_TPEMBELIAN_DET)
    ->result_array();
    $this->load->view('admin/transaction/receipt-view', array('det'=>$rdet,'data'=>$rdata));
  }

  public function receipt_inv($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->select('pur.*, pur.PurchNo, wh.NmWarehouse, uc.Nm_FullName as Nm_CreatedBy, (select sum(det1.PurchTotal) from tpembelian_det det1 where det1.PurchId = pur.PurchId) as TotalPurch')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENERIMAAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MGUDANG.' wh','wh.'.COL_ID." = ".TBL_TPENERIMAAN.".".COL_IDWAREHOUSE,"left")
    ->join(TBL_TPEMBELIAN.' pur','pur.'.COL_PURCHID." = ".TBL_TPENERIMAAN.".".COL_PURCHID,"left")
    ->join(TBL_MPEMASOK.' supp','supp.'.COL_ID." = pur.".COL_IDSUPPLIER,"left")
    ->where('pur.'.COL_PURCHID, $id)
    ->get(TBL_TPENERIMAAN)
    ->row_array();
    if(empty($rdata)) {
      echo '<div class="modal-body"><p class="text-center mb-0">DATA TIDAK DITEMUKAN!</p></div>';
      exit();
    }

    if(!empty($_POST)) {
      $rinv = $this->db
      ->select_sum(COL_INVTOTAL)
      ->where(COL_PURCHID, $id)
      ->order_by(COL_INVDATE, 'desc')
      ->get(TBL_TPEMBELIAN_INV)
      ->row_array();

      if($rinv[COL_INVTOTAL]+toNum($this->input->post(COL_INVTOTAL)) > $rdata['TotalPurch']) {
        ShowJsonError('Nominal pembayaran yang diinput melebihi total pembelian!');
        exit();
      }

      $data = array(
        COL_PURCHID => $id,
        COL_INVNO => $this->input->post(COL_INVNO),
        COL_INVDATE => date('Y-m-d', strtotime($this->input->post(COL_INVDATE))) ,
        COL_INVTOTAL => toNum($this->input->post(COL_INVTOTAL)),
        COL_INVREMARKS => $this->input->post(COL_INVREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPEMBELIAN_INV, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menginput data pembayaran.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $rinv = $this->db
      ->where(COL_PURCHID, $id)
      ->order_by(COL_INVDATE, 'asc')
      ->order_by(COL_CREATEDON, 'asc')
      ->get(TBL_TPEMBELIAN_INV)
      ->result_array();

      $this->load->view('admin/transaction/receipt-inv', array('data'=>$rdata, 'inv'=>$rinv));
    }
  }

  public function receipt_inv_delete($id) {
    $rdata = $this->db->where(COL_INVID, $id)->get(TBL_TPEMBELIAN_INV)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_INVID, $id)->delete(TBL_TPEMBELIAN_INV);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
    $this->db->trans_commit();
    ShowJsonSuccess('Data pembayaran no. <strong>'.$rdata[COL_INVNO].'</strong> berhasil dihapus.');
    exit();
  }

  public function sales() {
    $data['title'] = "Penjualan Sparepart & Jasa";
    $this->template->load('main', 'admin/transaction/sales', $data);
  }

  public function sales_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_SALESDATE=>'desc');
    $orderables = array(null,COL_SALESDATE,COL_SALESCUSTOMER,COL_SALESNO,COL_SALESREMARKS,COL_CREATEDON);
    $cols = array(COL_SALESDATE,COL_SALESCUSTOMER,COL_SALESNO,COL_SALESREMARKS);

    $queryAll = $this->db->get(TBL_TPENJUALAN);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_CREATEDBY) $item = TBL_TPENJUALAN.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TPENJUALAN.'.'.COL_SALESDATE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TPENJUALAN.'.'.COL_SALESDATE.' <= ', $dateTo);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tpenjualan.*, cust.NmCustomer, uc.Nm_FullName as Nm_CreatedBy')
    ->select('
      tpenjualan.*,
      cust.NmCustomer,
      uc.Nm_FullName as Nm_CreatedBy,
      (select sum(det1.SalesTotal) from tpenjualan_det det1 where det1.SalesId = tpenjualan.SalesId) as TotalSales,
      (select sum(det2.InvTotal) from tpenjualan_inv det2 where det2.SalesId = tpenjualan.SalesId) as TotalInv
    ')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = ".TBL_TPENJUALAN.".".COL_IDCUSTOMER,"left")
    ->order_by(TBL_TPENJUALAN.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TPENJUALAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlOpt = @'
      <div class="btn-group">
        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <i class="fas fa-list"></i>&nbsp;OPSI
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(68px, -2px, 0px);">
          <a class="dropdown-item text-sm text-danger btn-action" href="'.site_url('admin/transaction/sales-delete/'.$r[COL_SALESID]).'"><i class="far fa-times-circle"></i>&nbsp;HAPUS</a>
          <a class="dropdown-item text-sm text-success btn-popup-form" href="'.site_url('admin/transaction/sales-edit/'.$r[COL_SALESID]).'"><i class="far fa-pen-square"></i>&nbsp;UBAH </a>
          <a class="dropdown-item text-sm text-info btn-popup-form" href="'.site_url('admin/transaction/sales-view/'.$r[COL_SALESID]).'"><i class="far fa-info-circle"></i>&nbsp;INFO</a>
          <a class="dropdown-item text-sm text-primary btn-popup-form" href="'.site_url('admin/transaction/sales-inv/'.$r[COL_SALESID]).'"><i class="far fa-info-circle"></i>&nbsp;PEMBAYARAN</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item text-sm text-success" target="_blank" href="'.site_url('admin/transaction/sales-print/'.$r[COL_SALESID]).'"><i class="far fa-print"></i>&nbsp;CETAK FAKTUR</a>
        </div>
      </div>
      ';

      $data[] = array(
        $htmlOpt,
        date('Y-m-d', strtotime($r[COL_SALESDATE])),
        'No. <strong>'.$r[COL_SALESNO].'</strong> - an. <strong>'.$r[COL_SALESCUSTOMER].'</strong>',
        '<span class="pull-left">Rp. </span>'.number_format($r['TotalSales']),
        '<span class="pull-left">Rp. </span>'.number_format($r['TotalInv']),
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function sales_add() {
    $ruser = GetLoggedUser();
    if (!empty($_POST)) {
      $arrDetSales = array();
      $arrServ = array();
      $arrDetDeliv = array();
      $arrMovement = array();
      $det = $this->input->post("DistItems");
      $serv = $this->input->post("DistServiceItems");
      $dataDeliv = array();
      $dataSales = array(
        COL_IDCUSTOMER => $this->input->post(COL_IDCUSTOMER),
        COL_SALESCUSTOMER => $this->input->post(COL_SALESCUSTOMER),
        COL_SALESNO => $this->input->post(COL_SALESNO),
        COL_SALESDATE => $this->input->post(COL_SALESDATE),
        COL_SALESADDR => $this->input->post(COL_SALESADDR),
        COL_SALESREMARKS => $this->input->post(COL_SALESREMARKS),
        COL_SALESCUSTOMERNO => $this->input->post(COL_SALESCUSTOMERNO),
        COL_SALESCUSTOMERNOPLAT => $this->input->post(COL_SALESCUSTOMERNOPLAT),
        COL_SALESCUSTOMERNMVEHICLE => $this->input->post(COL_SALESCUSTOMERNMVEHICLE),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_SALESNO, $this->input->post(COL_SALESNO))
        ->get(TBL_TPENJUALAN)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->insert(TBL_TPENJUALAN, $dataSales);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idSales = $this->db->insert_id();

        $dataDeliv = array(
          COL_SALESID => $idSales,
          COL_IDWAREHOUSE => $this->input->post(COL_IDWAREHOUSE),
          COL_DODATE => $this->input->post(COL_SALESDATE),
          COL_DONO => $this->input->post(COL_SALESNO),
          COL_DOREMARKS => $this->input->post(COL_SALESREMARKS),

          COL_CREATEDBY => $ruser[COL_USERNAME],
          COL_CREATEDON => date('Y-m-d H:i:s')
        );
        $res = $this->db->insert(TBL_TPENGIRIMAN, $dataDeliv);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDeliv = $this->db->insert_id();

        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            $arrDetSales[] = array(
              COL_SALESID=>$idSales,
              COL_IDSTOCK=>$s->IdStock,
              COL_SALESQTY=>toNum($s->SalesQty),
              COL_SALESPRICE=>toNum($s->SalesPrice),
              COL_SALESDISC=>toNum($s->SalesDisc),
              COL_SALESTAX=>toNum($s->SalesTax),
              COL_SALESTOTAL=>toNum($s->SalesTotal)
            );
            $arrDetDeliv[] = array(
              COL_DOID=>$idDeliv,
              COL_IDSTOCK=>$s->IdStock,
              COL_DOQTY=>toNum($s->SalesQty)
            );
            $arrMovement[] = array(
              COL_DOID=>$idDeliv,
              COL_IDWAREHOUSE=>$this->input->post(COL_IDWAREHOUSE),
              COL_IDSTOCK=>$s->IdStock,
              COL_MOVETYPE=>'SAL',
              COL_MOVEDATE=>$this->input->post(COL_SALESDATE),
              COL_MOVEQTY=>(-1)*toNum($s->SalesQty),
              COL_MOVEREMARKS=>$this->input->post(COL_SALESREMARKS),
              COL_CREATEDBY => $ruser[COL_USERNAME],
              COL_CREATEDON => date('Y-m-d H:i:s')
            );
          }
        }
        if(!empty($serv)) {
          $serv = json_decode(urldecode($serv));
          $rserv = $this->db->query("select mbarang.* from mbarang left join mkategori on mkategori.Id = mbarang.IdKategori where mkategori.IsStock=0 and mkategori.IsSaleItem=1")->row_array();
          if(empty($rserv)) {
            throw new Exception('Parameter tidak valid.');
          }

          foreach($serv as $s) {
            $arrServ[] = array(
              COL_SALESID=>$idSales,
              COL_IDSTOCK=>$rserv[COL_ID],
              COL_SALESQTY=>1,
              COL_SALESPRICE=>toNum($s->SalesTotal),
              COL_SALESDISC=>0,
              COL_SALESTAX=>0,
              COL_SALESTOTAL=>toNum($s->SalesTotal),
              COL_SALESDESC=>toNum($s->SalesDesc),
            );
            $arrDetDeliv[] = array(
              COL_DOID=>$idDeliv,
              COL_IDSTOCK=>$rserv[COL_ID],
              COL_DOQTY=>1
            );
          }
        }
        if(empty($arrDetSales) && empty($arrServ)) {
          throw new Exception('Item tidak boleh kosong.');
        }

        if(!empty($arrDetSales)) {
          $res = $this->db->insert_batch(TBL_TPENJUALAN_DET, $arrDetSales);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($arrServ)) {
          $res = $this->db->insert_batch(TBL_TPENJUALAN_DET, $arrServ);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($arrDetDeliv)) {
          $res = $this->db->insert_batch(TBL_TPENGIRIMAN_DET, $arrDetDeliv);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }
        if(!empty($arrMovement)) {
          $res = $this->db->insert_batch(TBL_TBARANG, $arrMovement);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menginput data penjualan.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    }  else {
      $data['DistItems'] = json_encode(array());
      $data['DistItemServices'] = json_encode(array());
      $this->load->view('admin/transaction/sales-form', $data);
    }
  }

  public function sales_edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->select('tpenjualan.*, uc.Nm_FullName as Nm_CreatedBy, ue.Nm_FullName as Nm_UpdatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_CREATEDBY,"left")
    ->join(TBL__USERINFORMATION.' ue','ue.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_UPDATEDBY,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = tpenjualan.".COL_IDCUSTOMER,"left")
    ->where('tpenjualan.'.COL_SALESID, $id)
    ->get(TBL_TPENJUALAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $rdeliv = $this->db
    ->where(COL_SALESID, $id)
    ->get(TBL_TPENGIRIMAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $rinv = $this->db
    ->where(COL_SALESID, $id)
    ->order_by(COL_INVDATE, 'asc')
    ->order_by(COL_CREATEDON, 'asc')
    ->get(TBL_TPENJUALAN_INV)
    ->result_array();

    if (!empty($_POST)) {
      $idSales = $id;
      $idDeliv = $rdeliv[COL_DOID];

      $arrDetSales = array();
      $arrServ = array();
      $arrDetDeliv = array();
      $arrMovement = array();
      $det = $this->input->post("DistItems");
      $serv = $this->input->post("DistServiceItems");
      $dataDeliv = array();
      $dataSales = array(
        COL_IDCUSTOMER => $this->input->post(COL_IDCUSTOMER),
        COL_SALESCUSTOMER => $this->input->post(COL_SALESCUSTOMER),
        COL_SALESNO => $this->input->post(COL_SALESNO),
        COL_SALESDATE => $this->input->post(COL_SALESDATE),
        COL_SALESADDR => $this->input->post(COL_SALESADDR),
        COL_SALESREMARKS => $this->input->post(COL_SALESREMARKS),
        COL_SALESCUSTOMERNO => $this->input->post(COL_SALESCUSTOMERNO),
        COL_SALESCUSTOMERNOPLAT => $this->input->post(COL_SALESCUSTOMERNOPLAT),
        COL_SALESCUSTOMERNMVEHICLE => $this->input->post(COL_SALESCUSTOMERNMVEHICLE),

        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );
      $dataDeliv = array(
        COL_SALESID => $idSales,
        COL_IDWAREHOUSE => $this->input->post(COL_IDWAREHOUSE),
        COL_DODATE => $this->input->post(COL_SALESDATE),
        COL_DONO => $this->input->post(COL_SALESNO),
        COL_DOREMARKS => $this->input->post(COL_SALESREMARKS),

        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_SALESNO, $this->input->post(COL_SALESNO))
        ->where(COL_SALESID.' != ', $id)
        ->get(TBL_TPENJUALAN)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->where(COL_SALESID, $id)->update(TBL_TPENJUALAN, $dataSales);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
        $res = $this->db->where(COL_SALESID, $id)->update(TBL_TPENGIRIMAN, $dataDeliv);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if(empty($rinv)) {
          $res = $this->db->where(COL_SALESID, $idSales)->delete(TBL_TPENJUALAN_DET);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $res = $this->db->where(COL_DOID, $idDeliv)->delete(TBL_TPENGIRIMAN_DET);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $res = $this->db->where(COL_DOID, $idDeliv)->delete(TBL_TBARANG);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          if(!empty($det)) {
            $det = json_decode(urldecode($det));
            foreach($det as $s) {
              $arrDetSales[] = array(
                COL_SALESID=>$idSales,
                COL_IDSTOCK=>$s->IdStock,
                COL_SALESQTY=>toNum($s->SalesQty),
                COL_SALESPRICE=>toNum($s->SalesPrice),
                COL_SALESDISC=>toNum($s->SalesDisc),
                COL_SALESTAX=>toNum($s->SalesTax),
                COL_SALESTOTAL=>toNum($s->SalesTotal)
              );
              $arrDetDeliv[] = array(
                COL_DOID=>$idDeliv,
                COL_IDSTOCK=>$s->IdStock,
                COL_DOQTY=>toNum($s->SalesQty)
              );
              $arrMovement[] = array(
                COL_DOID=>$idDeliv,
                COL_IDWAREHOUSE=>$this->input->post(COL_IDWAREHOUSE),
                COL_IDSTOCK=>$s->IdStock,
                COL_MOVETYPE=>'SAL',
                COL_MOVEDATE=>$this->input->post(COL_SALESDATE),
                COL_MOVEQTY=>(-1)*toNum($s->SalesQty),
                COL_MOVEREMARKS=>$this->input->post(COL_SALESREMARKS),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
              );
            }
          }
          if(!empty($serv)) {
            $serv = json_decode(urldecode($serv));
            $rserv = $this->db->query("select mbarang.* from mbarang left join mkategori on mkategori.Id = mbarang.IdKategori where mkategori.IsStock=0 and mkategori.IsSaleItem=1")->row_array();
            if(empty($rserv)) {
              throw new Exception('Parameter tidak valid.');
            }

            foreach($serv as $s) {
              $arrServ[] = array(
                COL_SALESID=>$idSales,
                COL_IDSTOCK=>$rserv[COL_ID],
                COL_SALESQTY=>1,
                COL_SALESPRICE=>toNum($s->SalesTotal),
                COL_SALESDISC=>0,
                COL_SALESTAX=>0,
                COL_SALESTOTAL=>toNum($s->SalesTotal),
                COL_SALESDESC=>toNum($s->SalesDesc),
              );
              $arrDetDeliv[] = array(
                COL_DOID=>$idDeliv,
                COL_IDSTOCK=>$rserv[COL_ID],
                COL_DOQTY=>1
              );
            }
          }
          if(empty($arrDetSales) && empty($arrServ)) {
            throw new Exception('Item tidak boleh kosong.');
          }

          if(!empty($arrDetSales)) {
            $res = $this->db->insert_batch(TBL_TPENJUALAN_DET, $arrDetSales);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }
          }
          if(!empty($arrServ)) {
            $res = $this->db->insert_batch(TBL_TPENJUALAN_DET, $arrServ);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }
          }
          if(!empty($arrDetDeliv)) {
            $res = $this->db->insert_batch(TBL_TPENGIRIMAN_DET, $arrDetDeliv);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }
          }
          if(!empty($arrMovement)) {
            $res = $this->db->insert_batch(TBL_TBARANG, $arrMovement);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }
          }
        }
        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menginput data penjualan.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }

    } else {
      $arrdet = array();
      $arrsrv = array();
      $rdet = $this->db
      ->select('tpenjualan_det.*, st.NmStock, st.NmSatuan')
      ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpenjualan_det.".COL_IDSTOCK,"left")
      ->join(TBL_MKATEGORI.' kat','kat.'.COL_ID." = st.".COL_IDKATEGORI,"left")
      ->where(COL_SALESID, $id)
      ->where('kat.'.COL_ISSALEITEM, 1)
      ->where('kat.'.COL_ISSTOCK, 1)
      ->get(TBL_TPENJUALAN_DET)
      ->result_array();
      foreach($rdet as $det) {
        $arrdet[] = array(
          'IdStock'=> $det[COL_IDSTOCK],
          'NmStock'=> $det[COL_NMSTOCK],
          'NmSatuan'=> $det[COL_NMSATUAN],
          'SalesQty'=>$det[COL_SALESQTY],
          'SalesPrice'=>$det[COL_SALESPRICE],
          'SalesDisc'=>$det[COL_SALESDISC],
          'SalesTax'=>$det[COL_SALESTAX],
          'SalesTotal'=>$det[COL_SALESTOTAL]
        );
      }
      $rserv = $this->db
      ->select('tpenjualan_det.*, st.NmStock, st.NmSatuan')
      ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpenjualan_det.".COL_IDSTOCK,"left")
      ->join(TBL_MKATEGORI.' kat','kat.'.COL_ID." = st.".COL_IDKATEGORI,"left")
      ->where(COL_SALESID, $id)
      ->where('kat.'.COL_ISSALEITEM, 1)
      ->where('kat.'.COL_ISSTOCK, 0)
      ->get(TBL_TPENJUALAN_DET)
      ->result_array();
      foreach($rserv as $det) {
        $arrsrv[] = array(
          'SalesDesc'=>$det[COL_SALESDESC],
          'SalesTotal'=>$det[COL_SALESTOTAL]
        );
      }
      $data['data'] = $rdata;
      $data['DistItems'] = json_encode($arrdet);
      $data['DistItemServices'] = json_encode($arrsrv);
      $data['disabled'] = !empty($rinv);
      $this->load->view('admin/transaction/sales-form', $data);
    }
  }

  public function sales_delete($id) {
    $rdata = $this->db->where(COL_SALESID, $id)->get(TBL_TPENJUALAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_SALESID, $id)->delete(TBL_TPENJUALAN);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }
      $res = $this->db->query("delete from tbarang where DOId in (select DOId from tpengiriman where SalesId = $id)");
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
    $this->db->trans_commit();
    ShowJsonSuccess('Data penjualan no. <strong>'.$rdata[COL_SALESNO].'</strong> berhasil dihapus.');
    exit();
  }

  public function sales_view($id) {
    $rdata = $this->db
    ->select('tpenjualan.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = tpenjualan.".COL_IDCUSTOMER,"left")
    ->where('tpenjualan.'.COL_SALESID, $id)
    ->get(TBL_TPENJUALAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpenjualan_det.*, st.NmStock, st.NmSatuan, st.IdKategori, kat.IsStock')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpenjualan_det.".COL_IDSTOCK,"left")
    ->join(TBL_MKATEGORI.' kat','kat.'.COL_ID." = st.".COL_IDKATEGORI,"left")
    ->where(COL_SALESID, $rdata[COL_SALESID])
    ->order_by('st.'.COL_ID,'desc')
    ->get(TBL_TPENJUALAN_DET)
    ->result_array();
    $this->load->view('admin/transaction/sales-view', array('det'=>$rdet,'data'=>$rdata));
  }

  public function sales_inv($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->select('tpenjualan.*, uc.Nm_FullName as Nm_CreatedBy, (select sum(det1.SalesTotal) from tpenjualan_det det1 where det1.SalesId = tpenjualan.SalesId) as TotalSales')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_CREATEDBY,"left")
    ->where(COL_SALESID, $id)
    ->get(TBL_TPENJUALAN)
    ->row_array();
    if(empty($rdata)) {
      echo '<div class="modal-body"><p class="text-center mb-0">DATA TIDAK DITEMUKAN!</p></div>';
      exit();
    }

    if(!empty($_POST)) {
      $rinv = $this->db
      ->select_sum(COL_INVTOTAL)
      ->where(COL_SALESID, $id)
      ->order_by(COL_INVDATE, 'desc')
      ->get(TBL_TPENJUALAN_INV)
      ->row_array();

      if($rinv[COL_INVTOTAL]+toNum($this->input->post(COL_INVTOTAL)) > $rdata['TotalSales']) {
        ShowJsonError('Nominal pembayaran yang diinput melebihi total penjualan!');
        exit();
      }

      $data = array(
        COL_SALESID => $id,
        COL_INVNO => $this->input->post(COL_INVNO),
        COL_INVDATE => date('Y-m-d', strtotime($this->input->post(COL_INVDATE))) ,
        COL_INVTOTAL => toNum($this->input->post(COL_INVTOTAL)),
        COL_INVREMARKS => $this->input->post(COL_INVREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      if(!empty($this->input->post(COL_DEPOSITID))) {
        $data[COL_DEPOSITID] = $this->input->post(COL_DEPOSITID);
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TPENJUALAN_INV, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menginput data pembayaran.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $rinv = $this->db
      ->where(COL_SALESID, $id)
      ->order_by(COL_INVDATE, 'asc')
      ->order_by(COL_CREATEDON, 'asc')
      ->get(TBL_TPENJUALAN_INV)
      ->result_array();

      $this->load->view('admin/transaction/sales-inv', array('data'=>$rdata, 'inv'=>$rinv));
    }
  }

  public function sales_inv_delete($id) {
    $rdata = $this->db->where(COL_INVID, $id)->get(TBL_TPENJUALAN_INV)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_INVID, $id)->delete(TBL_TPENJUALAN_INV);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
    $this->db->trans_commit();
    ShowJsonSuccess('Data pembayaran no. <strong>'.$rdata[COL_INVNO].'</strong> berhasil dihapus.');
    exit();
  }

  public function sales_print($id) {
    $rdata = $this->db
    ->select('tpenjualan.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TPENJUALAN.".".COL_CREATEDBY,"left")
    ->join(TBL_MPELANGGAN.' cust','cust.'.COL_ID." = tpenjualan.".COL_IDCUSTOMER,"left")
    ->where('tpenjualan.'.COL_SALESID, $id)
    ->get(TBL_TPENJUALAN)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tpenjualan_det.*, st.NmStock, st.NmSatuan, st.IdKategori, kat.IsStock')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = tpenjualan_det.".COL_IDSTOCK,"left")
    ->join(TBL_MKATEGORI.' kat','kat.'.COL_ID." = st.".COL_IDKATEGORI,"left")
    ->where(COL_SALESID, $rdata[COL_SALESID])
    ->order_by('st.'.COL_ID,'desc')
    ->get(TBL_TPENJUALAN_DET)
    ->result_array();

    $sumInv = $this->db
    ->select_sum(COL_INVTOTAL)
    ->where(COL_SALESID, $id)
    ->order_by(COL_INVDATE, 'asc')
    ->order_by(COL_CREATEDON, 'asc')
    ->get(TBL_TPENJUALAN_INV)
    ->row_array();

    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-P");

    $html = $this->load->view('admin/transaction/sales-print', array('det'=>$rdet,'data'=>$rdata,'inv'=>$sumInv), TRUE);
    //echo $html;
    //exit();
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Faktur '.$this->setting_web_name.' - '.$rdata[COL_SALESNO].'.pdf', 'I');
  }

  public function deposit() {
    $data['title'] = "Deposit Pelanggan";
    $this->template->load('main', 'admin/transaction/deposit', $data);
  }

  public function deposit_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_SALESDATE=>'desc');
    $orderables = array(null,COL_DEPOSITDATE,COL_DEPOSITNAME,COL_DEPOSITNO,COL_DEPOSITREMARKS,COL_CREATEDON);
    $cols = array(COL_DEPOSITDATE,COL_DEPOSITNAME,COL_DEPOSITNO,COL_DEPOSITREMARKS);

    $queryAll = $this->db->get(TBL_TDEPOSIT);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_CREATEDBY) $item = TBL_TDEPOSIT.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TDEPOSIT.'.'.COL_DEPOSITDATE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TDEPOSIT.'.'.COL_DEPOSITDATE.' <= ', $dateTo);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tdeposit.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TDEPOSIT.".".COL_CREATEDBY,"left")
    ->order_by(TBL_TDEPOSIT.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TDEPOSIT, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlOpt = @'
      <div class="btn-group">
        <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <i class="fas fa-list"></i>&nbsp;OPSI
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(68px, -2px, 0px);">
          <a class="dropdown-item text-sm text-danger btn-action" href="'.site_url('admin/transaction/deposit-delete/'.$r[COL_DEPOSITID]).'"><i class="far fa-times-circle"></i>&nbsp;HAPUS</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item text-sm text-success" target="_blank" href="'.site_url('admin/transaction/deposit-print/'.$r[COL_DEPOSITID]).'"><i class="far fa-print"></i>&nbsp;CETAK</a>
        </div>
      </div>
      ';

      $data[] = array(
        $htmlOpt,
        date('Y-m-d', strtotime($r[COL_DEPOSITDATE])),
        'No. <strong>'.$r[COL_DEPOSITNO].'</strong> - an. <strong>'.$r[COL_DEPOSITNAME].'</strong>',
        $r[COL_DEPOSITREMARKS],
        '<span class="pull-left">Rp. </span>'.number_format($r[COL_DEPOSITAMOUNT]),
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function deposit_add() {
    $ruser = GetLoggedUser();
    if (!empty($_POST)) {
      $data = array(
        COL_DEPOSITNAME => $this->input->post(COL_DEPOSITNAME),
        COL_DEPOSITNO => $this->input->post(COL_DEPOSITNO),
        COL_DEPOSITDATE => date('Y-m-d', strtotime($this->input->post(COL_DEPOSITDATE))),
        COL_DEPOSITREMARKS => $this->input->post(COL_DEPOSITREMARKS),
        COL_DEPOSITAMOUNT => toNum($this->input->post(COL_DEPOSITAMOUNT)),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $rcheck = $this->db
        ->where(COL_DEPOSITNO, $this->input->post(COL_DEPOSITNO))
        ->get(TBL_TDEPOSIT)
        ->row_array();
        if(!empty($rcheck)) {
          throw new Exception('NO. REFERENSI yang anda input sudah digunakan. Silakan periksa kembali.');
        }

        $res = $this->db->insert(TBL_TDEPOSIT, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil menginput data deposit.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    }  else {
      $this->load->view('admin/transaction/deposit-form');
    }
  }

  public function deposit_browse() {
    $qbrowse = @"
    select *
    from tdeposit
    where tdeposit.DepositId not in (select distinct inv.DepositId from tpenjualan_inv inv where inv.DepositId is not null)
    order by tdeposit.DepositDate desc
    ";

    $rdeposit = $this->db->query($qbrowse)->result_array();
    $this->load->view('admin/transaction/deposit-browse', array('deposit'=>$rdeposit));
  }

  public function deposit_delete($id) {
    $rdata = $this->db->where(COL_DEPOSITID, $id)->get(TBL_TDEPOSIT)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $this->db->trans_begin();
    try {
      $rcheck = $this->db
      ->select('tpenjualan_inv.*, sal.SalesNo')
      ->where(TBL_TPENJUALAN_INV.'.'.COL_DEPOSITID, $id)
      ->join(TBL_TPENJUALAN.' sal','sal.'.COL_SALESID." = ".TBL_TPENJUALAN_INV.".".COL_SALESID,"inner")
      ->get(TBL_TPENJUALAN_INV)
      ->row_array();
      if(!empty($rcheck)) {
        throw new Exception('Data deposit ini tidak dapat dihapus karena sudah digunakan pada transaksi penjualan no. <strong>'.$rcheck[COL_SALESNO].'</strong>');
      }

      $res = $this->db->where(COL_DEPOSITID, $id)->delete(TBL_TDEPOSIT);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }

    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
    $this->db->trans_commit();
    ShowJsonSuccess('Data deposit no. <strong>'.$rdata[COL_DEPOSITNO].'</strong> berhasil dihapus.');
    exit();
  }

  public function deposit_print($id) {
    $rdata = $this->db
    ->select('tdeposit.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TDEPOSIT.".".COL_CREATEDBY,"left")
    ->where('tdeposit.'.COL_DEPOSITID, $id)
    ->get(TBL_TDEPOSIT)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-P");

    $html = $this->load->view('admin/transaction/deposit-print', array('data'=>$rdata), TRUE);
    //echo $html;
    //exit();
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output('Tanda Terima '.$this->setting_web_name.' - '.$rdata[COL_DEPOSITNO].'.pdf', 'I');
  }
}
