<?php
class Ajax extends MY_Controller {
  public function get_available_receipt() {
    $IdStock = $this->input->post('IdStock');
    $Date = $this->input->post('Tanggal');

    $q = @"
select
st.NmStock,
st.KdStock,
st.NmSatuan,
tbl.*,
tbl.Jumlah - tbl.JlhDist as JlhSisa
from (
	select *, coalesce((select sum(i.Jumlah) from tstockdistribution_items i where i.`IdReceipt` = r.`Uniq`),0) as JlhDist
	from tstockreceipt r
	where
		r.IdStock = ?
		and r.DateReceipt <= ?
) tbl
left join mbarang st on st.IdStock = tbl.`IdStock`
where
	tbl.Jumlah - tbl.JlhDist > 0
order by
  tbl.DateReceipt desc
    ";
    $rreceipt = $this->db->query($q, array($IdStock, $Date))->result_array();
    $this->load->view('admin/ajax/get-available-receipt', array('res'=>$rreceipt));
  }

  public function get_stock_uom() {
    $IdStock = $this->input->post('IdStock');
    $rstock = $this->db
    ->where(COL_ID, $IdStock)
    ->get(TBL_MBARANG)
    ->row_array();

    if(!empty($rstock)) {
      echo $rstock[COL_NMSATUAN];
    }
  }

  public function get_stock_harga() {
    $IdStock = $this->input->post('IdStock');
    $rstock = $this->db
    ->where(COL_ID, $IdStock)
    ->get(TBL_MBARANG)
    ->row_array();

    if(!empty($rstock)) {
      echo $rstock[COL_HARGA];
    }
  }

  public function get_stock_name_by_id() {
    $IdStock = $this->input->post('IdStock');
    $rstock = $this->db
    ->where(COL_IDSTOCK, $IdStock)
    ->get(TBL_MBARANG)
    ->row_array();

    if(!empty($rstock)) {
      echo $rstock[COL_NMSTOCK];
    }
  }

  public function get_available_dist() {
    $IdPuskesmas = $this->input->post('IdPuskesmas');
    $Date = $this->input->post('Tanggal');

    $q = @"
select
tbl.Uniq,
pus.NmPuskesmas,
r.NmBatch,
st.NmStock,
tbl.DateDistribution,
r.DateExpired,
st.NmSatuan,
tbl.Jumlah as JlhDist,
tbl.JlhPakai,
tbl.Jumlah - tbl.JlhPakai as JlhSisa
from (
	select
  i.Uniq,
  dist.DateDistribution,
	dist.IdPuskesmas,
	i.IdReceipt,
	i.Jumlah,
	coalesce((select sum(is_.Jumlah) from tstockissue is_ where is_.IdItem = i.Uniq),0) as JlhPakai
	from tstockdistribution_items i
	inner join tstockdistribution dist on dist.Uniq = i.IdDistribution
	where
    dist.DateDistribution <= ?
    and dist.IdPuskesmas = ?
) tbl
inner join tstockreceipt r on r.Uniq = tbl.IdReceipt
left join mbarang st on st.IdStock = r.IdStock
left join mpuskesmas pus on pus.IdPuskesmas = tbl.IdPuskesmas
where
	tbl.Jumlah - tbl.JlhPakai > 0
order by
  tbl.DateDistribution,
  st.NmStock,
  r.NmBatch,
  r.DateExpired
    ";
    $rdist = $this->db->query($q, array($Date, $IdPuskesmas))->result_array();
    $this->load->view('admin/ajax/get-available-dist', array('res'=>$rdist));
  }

  public function get_available_request() {
    $IdPuskesmas = $this->input->post('IdPuskesmas');

    $q = @"
select
req.*,
(select count(*) from trequest_items i_ where i_.IdRequest = req.Uniq) as Jlh
from trequest req
where
  IdPuskesmas = ?
    ";
    $rrequest = $this->db->query($q, array($IdPuskesmas))->result_array();
    $this->load->view('admin/ajax/get-available-request', array('res'=>$rrequest));
  }

  public function get_request_item() {
    $IdRequest = $this->input->post('IdRequest');

    $q = @"
select
i.*
from trequest_items i
left join mbarang st on st.IdStock = i.IdStock
where
  i.IdRequest = ?
    ";
    $rrequest = $this->db->query($q, array($IdRequest))->result_array();
    echo json_encode($rrequest);
  }

  public function changepassword() {
    $data['data'] = array();
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'NewPassword',
          'label' => 'NewPassword',
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[NewPassword]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password Baru.')
        )
      ));

      if($ruser[COL_PASSWORD] != md5($this->input->post('OldPassword'))) {
        ShowJsonError('Password Lama tidak valid.');
        return false;
      }

      if($this->form_validation->run()) {
        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post('NewPassword'))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password berhasil diperbarui.');
        return false;
      } else {
        ShowJsonError(validation_errors());
        return false;
      }
    } else {
      $this->load->view('admin/ajax/changepassword', $data);
    }
  }

  public function get_purchase_items() {
    $IdPurch = $this->input->post('IdPurchase');
    $rstock = $this->db
    ->select('tpembelian_det.*, st.NmStock, st.NmSatuan, coalesce((select sum(rec_.ReceiptQty) from tpenerimaan_det rec_ where rec_.IdStock = tpembelian_det.IdStock and rec_.ReceiptId in (select rec.ReceiptId from tpenerimaan rec where rec.PurchId = pur.PurchId)), 0) as ReceiptQty')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = ".TBL_TPEMBELIAN_DET.".".COL_IDSTOCK,"left")
    ->join(TBL_TPEMBELIAN.' pur','pur.'.COL_PURCHID." = ".TBL_TPEMBELIAN_DET.".".COL_PURCHID,"left")
    ->where(TBL_TPEMBELIAN_DET.".".COL_PURCHID, $IdPurch)
    ->get(TBL_TPEMBELIAN_DET)
    ->result_array();

    echo json_encode($rstock);
  }

  public function get_receipt_items() {
    $IdRecv = $this->input->post('IdReceipt');
    $rstock = $this->db
    ->select('tpenerimaan_det.*, pur.PurchPrice, pur.PurchDisc, pur.PurchTax, pur.PurchDisc, st.NmStock, st.NmSatuan, coalesce((select sum(inv_.InvQty) from tfaktur_det inv_ where inv_.IdStock = tpenerimaan_det.IdStock and inv_.InvId in (select inv.InvId from tfaktur inv where inv.ReceiptId = r.ReceiptId)), 0) as InvQty')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = ".TBL_TPENERIMAAN_DET.".".COL_IDSTOCK,"left")
    ->join(TBL_TPENERIMAAN.' r','r.'.COL_RECEIPTID." = ".TBL_TPENERIMAAN_DET.".".COL_RECEIPTID,"left")
    ->join(TBL_TPEMBELIAN_DET.' pur','pur.'.COL_PURCHID." = r.".COL_PURCHID." and pur.IdStock = tpenerimaan_det.IdStock","left")
    ->where(TBL_TPENERIMAAN_DET.".".COL_RECEIPTID, $IdRecv)
    ->get(TBL_TPENERIMAAN_DET)
    ->result_array();

    echo json_encode($rstock);
  }

  public function get_sales_items() {
    $IdSales = $this->input->post('IdSales');
    $rstock = $this->db
    ->select('tpenjualan_det.*, st.NmStock, st.NmSatuan, coalesce((select sum(rec_.DOQty) from tpengiriman_det rec_ where rec_.IdStock = tpenjualan_det.IdStock and rec_.DOId in (select rec.DOId from tpengiriman rec where rec.SalesId = sal.SalesId)), 0) as DOQty')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = ".TBL_TPENJUALAN_DET.".".COL_IDSTOCK,"left")
    ->join(TBL_TPENJUALAN.' sal','sal.'.COL_SALESID." = ".TBL_TPENJUALAN_DET.".".COL_SALESID,"left")
    ->where(TBL_TPENJUALAN_DET.".".COL_SALESID, $IdSales)
    ->get(TBL_TPENJUALAN_DET)
    ->result_array();

    echo json_encode($rstock);
  }

  public function get_delivery_items() {
    $IdDO = $this->input->post('IdDO');
    $rstock = $this->db
    ->select('tpengiriman_det.*, sal.SalesPrice, sal.SalesDisc, sal.SalesTax, sal.SalesDisc, st.NmStock, st.NmSatuan, coalesce((select sum(inv_.InvQty) from tfaktur_det inv_ where inv_.IdStock = tpengiriman_det.IdStock and inv_.InvId in (select inv.InvId from tfaktur inv where inv.DOId = do.DOId)), 0) as InvQty')
    ->join(TBL_MBARANG.' st','st.'.COL_ID." = ".TBL_TPENGIRIMAN_DET.".".COL_IDSTOCK,"left")
    ->join(TBL_TPENGIRIMAN.' do','do.'.COL_DOID." = ".TBL_TPENGIRIMAN_DET.".".COL_DOID,"left")
    ->join(TBL_TPENJUALAN_DET.' sal','sal.'.COL_SALESID." = do.".COL_SALESID." and sal.IdStock = tpengiriman_det.IdStock","left")
    ->where(TBL_TPENGIRIMAN_DET.".".COL_DOID, $IdDO)
    ->get(TBL_TPENGIRIMAN_DET)
    ->result_array();

    echo json_encode($rstock);
  }
}
