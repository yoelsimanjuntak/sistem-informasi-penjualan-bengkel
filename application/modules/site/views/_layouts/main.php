
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">-->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>" />

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    <!-- datatable reorder _ buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.css">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>

    <!-- my css -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/pallete.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/my.css">
    <style>
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
    }

    #footer-section::after {
      background: rgba(0, 0, 0, 0) url(<?=MY_IMAGEURL?>footer-map-bg.png) no-repeat scroll center center / 75% auto;
      content: "";
      height: 100%;
      left: 0;
      opacity: 0.1;
      position: absolute;
      top: 0;
      width: 100%;
      z-index: -1;
    }
    #footer-section table td {
      border-top: none !important;
    }
    .custom-file {
      overflow-x: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
    }
    .bootstrap-datetimepicker-widget table td {
      border: none !important
    }
    .omniauth-btn {
      width: 48%;
    }
    .omniauth-divider::before, .omniauth-divider::after {
      content: '';
      flex: 1;
      border-bottom: 1px solid #e1e1e1;
      margin: 24px 0;
    }
    .omniauth-divider::before {
      margin-right: 16px;
    }
    .omniauth-divider::after {
      margin-left: 16px;
    }
    .toast-message>p {
      margin: 0 !important;
    }

    .content-header h1>small {
      font-size: 15px;
      display: inline-block;
      padding-left: 4px;
      font-weight: 300;
    }
    .rating {
      display: inline-block;
      position: relative;
      font-size: 20pt;
    }

    .rating label {
      position: absolute;
      top: 0;
      left: 0;
      height: 100%;
      cursor: pointer;
    }

    .rating label:last-child {
      position: static;
    }

    .rating label:nth-child(1) {
      z-index: 5;
    }

    .rating label:nth-child(2) {
      z-index: 4;
    }

    .rating label:nth-child(3) {
      z-index: 3;
    }

    .rating label:nth-child(4) {
      z-index: 2;
    }

    .rating label:nth-child(5) {
      z-index: 1;
    }

    .rating label input {
      position: absolute;
      top: 0;
      left: 0;
      opacity: 0;
    }

    .rating label .icon {
      float: left;
      color: transparent;
    }

    .rating label:last-child .icon {
      color: #000;
    }

    .rating:not(:hover) label input:checked ~ .icon,
    .rating:hover label:hover input ~ .icon {
      color: #ffc107;
    }

    .rating label input:focus:not(:checked) ~ .icon:last-child {
      color: #ffc107;
      text-shadow: 0 0 5px #ffc107;
    }}
    </style>
</head>
<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NM_FULLNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_NM_PROFILEIMAGE] ? MY_UPLOADURL.$ruser[COL_NM_PROFILEIMAGE] : MY_IMAGEURL.'user.jpg';
}
?>
<body class="hold-transition <?=$bodyclass?>" style="background-color: #f4f6f9 !important">
  <div class="se-pre-con"></div>
  <?php
  if($is_plain) {
    echo $content;
  } else {
    ?>
    <div class="wrapper">
      <nav class="main-header navbar navbar-expand navbar-light navbar-white">
        <div class="container">
            <a href="<?=site_url()?>" class="navbar-brand">
                <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image" style="opacity: .8">
                <span class="brand-text font-weight-light"><?=$this->setting_web_name?> <!--<small class="font-weight-light d-none d-sm-inline-block"><?=$this->setting_web_desc?> <sup class="text-danger text-bold">ver <?=$this->setting_web_version?></sup></small>--></span>
            </a>
            <ul class="navbar-nav">
              <?php
              $isLogin = IsLogin();
              if($isLogin) {
                ?>
                <?php
              }
              ?>
            </ul>

            <ul class="navbar-nav ml-auto mr-2">
              <?php
              if(IsLogin()) {
                ?>
                <li class="nav-item dropdown">
                  <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                      <img src="<?=$displaypicture?>" class="user-image img-circle elevation-1" alt="<?=$displayname?>" style="width: 20px !important; height: 20px !important">
                      &nbsp; <?=$displayname?>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="<?=site_url('site/home/changepassword')?>" class="dropdown-item"><i class="fad fa-key"></i>&nbsp;&nbsp;Ubah Password</a></li>
                    <li><a href="<?=site_url('site/home/logout')?>" class="dropdown-item"><i class="fad fa-sign-out"></i>&nbsp;&nbsp;Keluar</a></li>
                  </ul>
                </li>
                <?php
              } else {
                ?>
                <?php
              }
              ?>
              <li class="nav-item d-sm-none">
                  <a class="nav-link" href="#"><i class="fad fa-bars"></i></a>
              </li>
            </ul>
        </div>
      </nav>
      <?=$content?>
      <footer class="main-footer bg-pallete-4 bg-dark">
          <div class="float-right d-none d-sm-inline">
              Version <strong><?=$this->setting_web_version?></strong>
          </div>
          <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>.
      </footer>
    </div>
    <?php
  }
  ?>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.motio.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/icheck.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
  <script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>
  <script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/inputmask/jquery.inputmask.bundle.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/moment/moment.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/chart.js/Chart.min.js"></script>
  <script type="text/javascript">
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });
  $(document).ready(function() {
      /*var element = document.querySelector('.content-wrapper');
      var panning = new Motio(element, {
        fps: 30,
        speedX: 30
      });
      panning.play();*/
      $('a[href="<?=current_url()?>"]').addClass('active');
      $('a[href="<?=current_url()?>"]').closest('li.has-treeview').addClass('menu-open').children('.nav-link').addClass('active');

      $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('input[type=file]').on('change',function(){
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
      });
      $('.datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1900,//parseInt(moment().subtract(moment.duration(14, 'Y')).format('YYYY'), 10),
        maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
      $(".uang").number(true, 0, '.', ',');
      /*$('.timepicker').datetimepicker({
        format: 'HH:mm'
      });
      $('input.datetimepicker-input', $('.timepicker')).focus(function() {
        $(this).closest('.timepicker').datetimepicker('toggle');
      });*/
      $('[data-toggle="datetimepicker"]').datetimepicker({
        format: 'HH:mm',
        useCurrent: false
      });

      $('[data-mask]').inputmask();
      jQuery.extend(jQuery.validator.messages, {
        required: "Masi kosong ini coeg.",
        /*remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")*/
      });
      var elements = document.getElementsByTagName("input");
      for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
          e.target.setCustomValidity("");
          if (!e.target.validity.valid) {
            if(e.srcElement.type == "email") {
              e.target.setCustomValidity("Harap isi dengan format email yang benar.");
            } else {
              e.target.setCustomValidity("Kolom ini wajib diisi.");
            }
          }
        };
        elements[i].oninput = function(e) {
          e.target.setCustomValidity("");
        };
      }
  });
  </script>
</body>
</html>
