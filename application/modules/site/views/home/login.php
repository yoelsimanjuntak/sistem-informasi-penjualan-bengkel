<div class="login-box">
  <div class="login-logo">
    <a href="<?=site_url()?>">
      <img class="user-image" src="<?=MY_IMAGEURL.'logo.png'?>" style="width: 60px" alt="Logo"><br />
      <!--<img src="<?=MY_IMAGEURL.'logo-tt.png'?>" style="width: 60px" alt="Logo Kota Tebing Tinggi"><br  />-->
      <small class="text-muted font-weight-bold"><?=$this->setting_web_name?></small>
      <p style="font-size: 12pt;"><?=$this->setting_web_desc?></p>
    </a>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
      <?=form_open(site_url('site/ajax/login'),array('role'=>'form','id'=>'form-login','class'=>'form-horizontal'))?>
      <div class="row">
        <div class="col-sm-12">
          <div class="form-group row">
            <div class="input-group">
              <input type="text" name="<?=COL_USERNAME?>" class="form-control" placeholder="Username" required />
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="input-group">
              <input type="password" name="<?=COL_PASSWORD?>" class="form-control" placeholder="Password" required />
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-key"></span>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="icheck-primary">
              <input type="checkbox" id="RememberMe" name="RememberMe">
              <label for="RememberMe">Ingat Saya</label>
            </div>
          </div>
          <div class="form-group row">
            <button type="submit" class="btn btn-block btn-outline-primary mb-1"><i class="fad fa-sign-in"></i>&nbsp;&nbsp;LOGIN</button>
          </div>
        </div>
      </div>
      <?=form_close()?>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $('#form-login').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
