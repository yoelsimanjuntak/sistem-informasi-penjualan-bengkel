<?php
class Ajax extends MY_Controller {

  public function __construct()
  {
      parent::__construct();
      setlocale (LC_TIME, 'id_ID');
  }

  public function login() {
    $this->form_validation->set_rules(array(
      array(
        'field' => 'UserName',
        'label' => 'UserName',
        'rules' => 'required'
      ),
      array(
        'field' => 'Password',
        'label' => 'Password',
        'rules' => 'required'
      )
    ));
    if($this->form_validation->run()) {
      $this->load->model('muser');
      $username = $this->input->post(COL_USERNAME);
      $password = $this->input->post(COL_PASSWORD);

      if($this->muser->authenticate($username, $password)) {
        if($this->muser->IsSuspend($username)) {
          ShowJsonError('Akun anda di suspend.');
          return;
        }

        $userdetails = $this->muser->getdetails($username);
        /*if(empty($userdetails[COL_IS_EMAILVERIFIED]) || $userdetails[COL_IS_EMAILVERIFIED] != 1) {
          ShowJsonError('Maaf, akun kamu belum diverifikasi. Silakan periksa link verifikasi yang dikirimkan melalui email kamu.');
          return;
        }*/

        $this->db->where(COL_USERNAME, $username);
        $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));
        SetLoginSession($userdetails);
        ShowJsonSuccess('Selamat datang, <strong>'.$userdetails[COL_NM_FULLNAME].'</strong>!', array('redirect'=>site_url('admin/dashboard')));
      } else {
        ShowJsonError('Username / password tidak tepat.');
      }
    }
  }
}
