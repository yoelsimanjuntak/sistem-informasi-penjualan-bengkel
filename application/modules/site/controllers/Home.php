<?php
class Home extends MY_Controller {
  function __construct() {
    parent::__construct();
  }

  public function _404() {
    $data['title'] = 'Error';
    $this->template->load('main' , 'home/_error', $data);
  }

  public function index() {
    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] == ROLEADMIN) {
      redirect('admin/dashboard');
    }
    $data = array();
		$this->template->set('title', 'Beranda');
		$this->template->load('main' , 'home/index', $data);
  }

  public function login() {
    $data['title'] = "Login";
    $this->template->load('main' , 'home/login', $data, false, true, 'login-page');
  }
  public function logout(){
      UnsetLoginSession();
      redirect(site_url());
  }
  public function changepassword() {
    if(!IsLogin()) {
      redirect('site/home');
    }

    $ruser = GetLoggedUser();
    $data['email'] = $ruser[COL_NM_EMAIL];
    $data['title'] = "Ubah Password";
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => COL_PASSWORD,
          'label' => COL_PASSWORD,
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[Password]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password.')
        )
      ));

      if($this->form_validation->run()) {
        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post(COL_PASSWORD))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password berhasil diperbarui. Silakan login kembali.', array('redirect'=>site_url('site/home/logout')));
        return false;
      }
    }
    $this->template->load('main', 'home/resetpassword', $data);
  }
}
 ?>
