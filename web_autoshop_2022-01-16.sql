# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: web_autoshop
# Generation Time: 2022-01-16 10:07:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','BENGKEL ERIC'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Auto Service Management System'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','ERIC AUTO SERVICE & SPAREPART'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Imam Bonjol Tebing Tinggi'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','082368309119'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','Partopi Tao'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `IdUnit` bigint(10) DEFAULT NULL,
  `Email` varchar(50) NOT NULL DEFAULT '',
  `NM_FullName` varchar(50) DEFAULT NULL,
  `NM_ProfileImage` varchar(250) DEFAULT NULL,
  `DATE_Registered` datetime DEFAULT NULL,
  `IS_EmailVerified` tinyint(1) DEFAULT NULL,
  `IS_LoginViaGoogle` tinyint(1) DEFAULT NULL,
  `IS_LoginViaFacebook` tinyint(1) DEFAULT NULL,
  `NM_GoogleOAuthID` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  CONSTRAINT `FK_Userinformation_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `IdUnit`, `Email`, `NM_FullName`, `NM_ProfileImage`, `DATE_Registered`, `IS_EmailVerified`, `IS_LoginViaGoogle`, `IS_LoginViaFacebook`, `NM_GoogleOAuthID`)
VALUES
	('admin',NULL,'yoelrolas@gmail.com','Partopi Tao',NULL,'2018-08-17 00:00:00',NULL,NULL,NULL,NULL),
	('admin.manda',NULL,'admin.manda','Manda',NULL,'2022-01-16 00:00:00',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2022-01-16 09:21:11','::1'),
	('admin.manda','f6f8097b050bfcbd7b14a733fd918f75',1,0,'2022-01-16 17:02:21','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mbarang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mbarang`;

CREATE TABLE `mbarang` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdKategori` bigint(10) unsigned NOT NULL,
  `NmStock` varchar(100) NOT NULL DEFAULT '',
  `NmKode` varchar(100) DEFAULT NULL,
  `NmSatuan` varchar(10) DEFAULT NULL,
  `NmKeterangan` text,
  `Harga` double DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mbarang` WRITE;
/*!40000 ALTER TABLE `mbarang` DISABLE KEYS */;

INSERT INTO `mbarang` (`Id`, `IdKategori`, `NmStock`, `NmKode`, `NmSatuan`, `NmKeterangan`, `Harga`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,3,'Perawatan / Perbaikan',NULL,'KALI',NULL,0,'admin','2022-01-15 12:00:00',NULL,NULL),
	(4,1,'Oli Shell Helix HX3',NULL,'KALENG',NULL,53000,'admin','2022-01-15 13:34:18','admin','2022-01-15 16:00:30'),
	(8,2,'Kunci Pas',NULL,'BUAH',NULL,0,'admin','2022-01-15 15:59:24',NULL,NULL),
	(9,1,'Oli Shell Helix HX5',NULL,'KALENG',NULL,68500,'admin','2022-01-15 16:00:19',NULL,NULL),
	(11,1,'TEMP',NULL,'PCS',NULL,99999,'admin','2022-01-16 16:09:16',NULL,NULL);

/*!40000 ALTER TABLE `mbarang` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mgudang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mgudang`;

CREATE TABLE `mgudang` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmWarehouse` varchar(100) NOT NULL DEFAULT '',
  `NmAlamat` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mgudang` WRITE;
/*!40000 ALTER TABLE `mgudang` DISABLE KEYS */;

INSERT INTO `mgudang` (`Id`, `NmWarehouse`, `NmAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'GUDANG UTAMA','Jl. Imam Bonjol, Tebing Tinggi, Sumatera Utara','admin','2021-01-30 18:44:22','admin','2022-01-15 16:09:58');

/*!40000 ALTER TABLE `mgudang` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mkategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mkategori`;

CREATE TABLE `mkategori` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmKategori` varchar(100) NOT NULL DEFAULT '',
  `IsStock` tinyint(1) NOT NULL DEFAULT '1',
  `IsSaleItem` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mkategori` WRITE;
/*!40000 ALTER TABLE `mkategori` DISABLE KEYS */;

INSERT INTO `mkategori` (`Id`, `NmKategori`, `IsStock`, `IsSaleItem`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'SPAREPART',1,1,'admin','2022-01-15 12:00:00',NULL,NULL),
	(2,'PERLENGKAPAN / PERALATAN',1,0,'admin','2022-01-15 12:00:00',NULL,NULL),
	(3,'JASA',0,1,'admin','2022-01-15 12:00:00',NULL,NULL);

/*!40000 ALTER TABLE `mkategori` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpelanggan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpelanggan`;

CREATE TABLE `mpelanggan` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmCustomer` varchar(100) NOT NULL DEFAULT '',
  `NmNama` varchar(100) DEFAULT NULL,
  `NmNoHP` varchar(100) DEFAULT NULL,
  `NmAlamat` varchar(100) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpelanggan` WRITE;
/*!40000 ALTER TABLE `mpelanggan` DISABLE KEYS */;

INSERT INTO `mpelanggan` (`Id`, `NmCustomer`, `NmNama`, `NmNoHP`, `NmAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'Lainnya','-','-','-','admin','2022-01-16 12:00:00',NULL,NULL);

/*!40000 ALTER TABLE `mpelanggan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpemasok
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpemasok`;

CREATE TABLE `mpemasok` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmSupplier` varchar(100) NOT NULL DEFAULT '',
  `NmNama` varchar(100) DEFAULT NULL,
  `NmNoHP` varchar(100) DEFAULT NULL,
  `NmAlamat` varchar(100) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpemasok` WRITE;
/*!40000 ALTER TABLE `mpemasok` DISABLE KEYS */;

INSERT INTO `mpemasok` (`Id`, `NmSupplier`, `NmNama`, `NmNoHP`, `NmAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'Lainnya','-','-','-','admin','2022-01-15 12:00:00',NULL,NULL);

/*!40000 ALTER TABLE `mpemasok` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbarang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbarang`;

CREATE TABLE `tbarang` (
  `MovId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdWarehouse` bigint(10) unsigned NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  `DOId` bigint(10) unsigned DEFAULT NULL,
  `ReceiptId` bigint(10) unsigned DEFAULT NULL,
  `MoveType` varchar(10) NOT NULL DEFAULT '',
  `MoveDate` date NOT NULL,
  `MoveQty` double NOT NULL,
  `MoveRemarks` text,
  `CreatedBy` varchar(10) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`MovId`),
  KEY `FK_MOV_STOCK` (`IdStock`),
  KEY `FK_MOV_WAREHOUSE` (`IdWarehouse`),
  KEY `FK_MOV_DO` (`DOId`),
  KEY `FK_MOV_RECEIPT` (`ReceiptId`),
  CONSTRAINT `FK_MOV_DO` FOREIGN KEY (`DOId`) REFERENCES `tpengiriman` (`DOId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MOV_RECEIPT` FOREIGN KEY (`ReceiptId`) REFERENCES `tpenerimaan` (`ReceiptId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MOV_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_MOV_WAREHOUSE` FOREIGN KEY (`IdWarehouse`) REFERENCES `mgudang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbarang` WRITE;
/*!40000 ALTER TABLE `tbarang` DISABLE KEYS */;

INSERT INTO `tbarang` (`MovId`, `IdWarehouse`, `IdStock`, `DOId`, `ReceiptId`, `MoveType`, `MoveDate`, `MoveQty`, `MoveRemarks`, `CreatedBy`, `CreatedOn`)
VALUES
	(7,1,4,NULL,6,'PUR','2022-01-16',120,'P001','admin','2022-01-16 09:45:09'),
	(8,1,9,NULL,6,'PUR','2022-01-16',50,'P001','admin','2022-01-16 09:45:09'),
	(13,1,9,7,NULL,'SAL','2022-01-16',-1,'DEV','admin','2022-01-16 13:08:50'),
	(14,1,11,NULL,NULL,'PUR','2022-01-16',12,'STOCK AWAL','admin','2022-01-16 16:09:16');

/*!40000 ALTER TABLE `tbarang` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tfaktur
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tfaktur`;

CREATE TABLE `tfaktur` (
  `InvId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DOId` bigint(10) unsigned DEFAULT NULL,
  `ReceiptId` bigint(10) unsigned DEFAULT NULL,
  `InvNo` varchar(50) DEFAULT NULL,
  `InvDate` date NOT NULL,
  `InvDue` date NOT NULL,
  `InvRemarks` text,
  `InvRemarksPayment` text,
  `InvFreight` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`InvId`),
  KEY `FK_INVOICE_RECEIPT` (`ReceiptId`),
  KEY `FK_INVOICE_DELIVERY` (`DOId`),
  CONSTRAINT `FK_INVOICE_DELIVERY` FOREIGN KEY (`DOId`) REFERENCES `tpengiriman` (`DOId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_INVOICE_RECEIPT` FOREIGN KEY (`ReceiptId`) REFERENCES `tpenerimaan` (`ReceiptId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tfaktur_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tfaktur_det`;

CREATE TABLE `tfaktur_det` (
  `InvDetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `InvId` bigint(10) unsigned NOT NULL,
  `InvQty` double NOT NULL,
  `InvPrice` double NOT NULL,
  `InvDisc` double NOT NULL,
  `InvTax` double NOT NULL,
  `InvTotal` double NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`InvDetId`),
  KEY `FK_DET_INVOICE` (`InvId`),
  KEY `FK_INVOICE_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_INVOICE` FOREIGN KEY (`InvId`) REFERENCES `tfaktur` (`InvId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_INVOICE_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tpembelian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpembelian`;

CREATE TABLE `tpembelian` (
  `PurchId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdSupplier` bigint(10) unsigned NOT NULL,
  `PurchSupplier` text,
  `PurchNo` varchar(50) DEFAULT NULL,
  `PurchDate` date NOT NULL,
  `PurchAddr` text,
  `PurchRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`PurchId`),
  KEY `FK_PURCHASE_SUPP` (`IdSupplier`),
  CONSTRAINT `FK_PURCHASE_SUPP` FOREIGN KEY (`IdSupplier`) REFERENCES `mpemasok` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpembelian` WRITE;
/*!40000 ALTER TABLE `tpembelian` DISABLE KEYS */;

INSERT INTO `tpembelian` (`PurchId`, `IdSupplier`, `PurchSupplier`, `PurchNo`, `PurchDate`, `PurchAddr`, `PurchRemarks`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(6,1,'UD. TIMBUL TENGGELAM','P001','2022-01-16',NULL,'DEV','admin','2022-01-16 09:45:09',NULL,NULL);

/*!40000 ALTER TABLE `tpembelian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpembelian_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpembelian_det`;

CREATE TABLE `tpembelian_det` (
  `PurchDetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `PurchId` bigint(10) unsigned NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  `PurchQty` double NOT NULL,
  `PurchPrice` double NOT NULL,
  `PurchDisc` double NOT NULL,
  `PurchTax` double NOT NULL,
  `PurchTotal` double NOT NULL,
  PRIMARY KEY (`PurchDetId`),
  KEY `FK_DET_PURCHASE` (`PurchId`),
  KEY `FK_PURCHASE_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_PURCHASE` FOREIGN KEY (`PurchId`) REFERENCES `tpembelian` (`PurchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PURCHASE_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpembelian_det` WRITE;
/*!40000 ALTER TABLE `tpembelian_det` DISABLE KEYS */;

INSERT INTO `tpembelian_det` (`PurchDetId`, `PurchId`, `IdStock`, `PurchQty`, `PurchPrice`, `PurchDisc`, `PurchTax`, `PurchTotal`)
VALUES
	(11,6,4,120,38500,0,10,5082000),
	(12,6,9,50,43600,0,10,2398000);

/*!40000 ALTER TABLE `tpembelian_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpembelian_inv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpembelian_inv`;

CREATE TABLE `tpembelian_inv` (
  `InvId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `PurchId` bigint(10) unsigned NOT NULL,
  `InvNo` varchar(50) DEFAULT NULL,
  `InvDate` date NOT NULL,
  `InvTotal` double NOT NULL,
  `InvRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`InvId`),
  KEY `FK_INV_PUR` (`PurchId`),
  CONSTRAINT `FK_INV_PUR` FOREIGN KEY (`PurchId`) REFERENCES `tpembelian` (`PurchId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpembelian_inv` WRITE;
/*!40000 ALTER TABLE `tpembelian_inv` DISABLE KEYS */;

INSERT INTO `tpembelian_inv` (`InvId`, `PurchId`, `InvNo`, `InvDate`, `InvTotal`, `InvRemarks`, `CreatedBy`, `CreatedOn`)
VALUES
	(9,6,'P001-1','2022-01-16',3300000,'DEV','admin','2022-01-16 09:45:43');

/*!40000 ALTER TABLE `tpembelian_inv` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenerimaan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenerimaan`;

CREATE TABLE `tpenerimaan` (
  `ReceiptId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdWarehouse` bigint(10) unsigned NOT NULL,
  `PurchId` bigint(10) unsigned NOT NULL,
  `ReceiptDate` date NOT NULL,
  `ReceiptRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ReceiptId`),
  KEY `FK_RECEIPT_PURCHASE` (`PurchId`),
  KEY `FK_RECEIPT_WAREHOUSE` (`IdWarehouse`),
  CONSTRAINT `FK_RECEIPT_PURCHASE` FOREIGN KEY (`PurchId`) REFERENCES `tpembelian` (`PurchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RECEIPT_WAREHOUSE` FOREIGN KEY (`IdWarehouse`) REFERENCES `mgudang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenerimaan` WRITE;
/*!40000 ALTER TABLE `tpenerimaan` DISABLE KEYS */;

INSERT INTO `tpenerimaan` (`ReceiptId`, `IdWarehouse`, `PurchId`, `ReceiptDate`, `ReceiptRemarks`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(6,1,6,'2022-01-16','DEV','admin','2022-01-16 09:45:09','',NULL);

/*!40000 ALTER TABLE `tpenerimaan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenerimaan_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenerimaan_det`;

CREATE TABLE `tpenerimaan_det` (
  `ReceiptDetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ReceiptId` bigint(10) unsigned NOT NULL,
  `ReceiptQty` double NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`ReceiptDetId`),
  KEY `FK_DET_RECEIPT` (`ReceiptId`),
  KEY `FK_RECEIPT_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_RECEIPT` FOREIGN KEY (`ReceiptId`) REFERENCES `tpenerimaan` (`ReceiptId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RECEIPT_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenerimaan_det` WRITE;
/*!40000 ALTER TABLE `tpenerimaan_det` DISABLE KEYS */;

INSERT INTO `tpenerimaan_det` (`ReceiptDetId`, `ReceiptId`, `ReceiptQty`, `IdStock`)
VALUES
	(11,6,120,4),
	(12,6,50,9);

/*!40000 ALTER TABLE `tpenerimaan_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpengiriman
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpengiriman`;

CREATE TABLE `tpengiriman` (
  `DOId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdWarehouse` bigint(10) unsigned NOT NULL,
  `SalesId` bigint(10) unsigned NOT NULL,
  `DONo` varchar(100) DEFAULT NULL,
  `DODate` date NOT NULL,
  `DORemarks` text,
  `DOAddr` text NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`DOId`),
  KEY `FK_DO_WAREHOUSE` (`IdWarehouse`),
  KEY `FK_DO_SALES` (`SalesId`),
  CONSTRAINT `FK_DO_SALES` FOREIGN KEY (`SalesId`) REFERENCES `tpenjualan` (`SalesId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DO_WAREHOUSE` FOREIGN KEY (`IdWarehouse`) REFERENCES `mgudang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpengiriman` WRITE;
/*!40000 ALTER TABLE `tpengiriman` DISABLE KEYS */;

INSERT INTO `tpengiriman` (`DOId`, `IdWarehouse`, `SalesId`, `DONo`, `DODate`, `DORemarks`, `DOAddr`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(7,1,9,'S001','2022-01-16','DEV','','admin','2022-01-16 13:08:50',NULL,NULL),
	(8,1,10,'S002','2022-01-16','DEV','','admin','2022-01-16 13:31:18',NULL,NULL);

/*!40000 ALTER TABLE `tpengiriman` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpengiriman_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpengiriman_det`;

CREATE TABLE `tpengiriman_det` (
  `DODetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DOId` bigint(10) unsigned NOT NULL,
  `DOQty` double NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`DODetId`),
  KEY `FK_DET_DO` (`DOId`),
  KEY `FK_DO_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_DO` FOREIGN KEY (`DOId`) REFERENCES `tpengiriman` (`DOId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DO_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpengiriman_det` WRITE;
/*!40000 ALTER TABLE `tpengiriman_det` DISABLE KEYS */;

INSERT INTO `tpengiriman_det` (`DODetId`, `DOId`, `DOQty`, `IdStock`)
VALUES
	(6,7,1,9),
	(7,7,1,1),
	(8,7,1,1),
	(9,8,1,1),
	(10,8,1,1),
	(11,8,1,1),
	(12,8,1,1);

/*!40000 ALTER TABLE `tpengiriman_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenjualan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenjualan`;

CREATE TABLE `tpenjualan` (
  `SalesId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdCustomer` bigint(10) unsigned NOT NULL,
  `SalesCustomer` varchar(50) DEFAULT NULL,
  `SalesNo` varchar(50) DEFAULT NULL,
  `SalesDate` date NOT NULL,
  `SalesAddr` text,
  `SalesRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SalesId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenjualan` WRITE;
/*!40000 ALTER TABLE `tpenjualan` DISABLE KEYS */;

INSERT INTO `tpenjualan` (`SalesId`, `IdCustomer`, `SalesCustomer`, `SalesNo`, `SalesDate`, `SalesAddr`, `SalesRemarks`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(9,1,'Bpk. ARIS','S001','2022-01-16',NULL,'DEV','admin','2022-01-16 13:08:50',NULL,NULL),
	(10,1,'Yoel Simanjuntak','S002','2022-01-16',NULL,'DEV','admin','2022-01-16 13:31:18',NULL,NULL);

/*!40000 ALTER TABLE `tpenjualan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenjualan_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenjualan_det`;

CREATE TABLE `tpenjualan_det` (
  `SalesDetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `SalesId` bigint(10) unsigned NOT NULL,
  `SalesQty` double NOT NULL,
  `SalesPrice` double NOT NULL,
  `SalesDisc` double NOT NULL,
  `SalesTax` double NOT NULL,
  `SalesTotal` double NOT NULL,
  `SalesDesc` text,
  `IdStock` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`SalesDetId`),
  KEY `FK_DET_SALES` (`SalesId`),
  KEY `FK_SALES_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_SALES` FOREIGN KEY (`SalesId`) REFERENCES `tpenjualan` (`SalesId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenjualan_det` WRITE;
/*!40000 ALTER TABLE `tpenjualan_det` DISABLE KEYS */;

INSERT INTO `tpenjualan_det` (`SalesDetId`, `SalesId`, `SalesQty`, `SalesPrice`, `SalesDisc`, `SalesTax`, `SalesTotal`, `SalesDesc`, `IdStock`)
VALUES
	(6,9,1,68500,0,10,75350,NULL,9),
	(7,9,1,50000,0,0,50000,'Ganti Oli',1),
	(8,9,1,100000,0,0,100000,'Ganti Filter Oli',1),
	(9,10,1,150000,0,0,150000,'Cek Mesin',1),
	(10,10,1,100000,0,0,100000,'Cek Transmisi',1),
	(11,10,1,100000,0,0,100000,'Cek Kelistrikan',1),
	(12,10,1,50000,0,0,50000,'Cek AC',1);

/*!40000 ALTER TABLE `tpenjualan_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenjualan_inv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenjualan_inv`;

CREATE TABLE `tpenjualan_inv` (
  `InvId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `SalesId` bigint(10) unsigned NOT NULL,
  `InvNo` varchar(50) NOT NULL DEFAULT '',
  `InvDate` date NOT NULL,
  `InvTotal` double NOT NULL,
  `InvRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`InvId`),
  KEY `FK_INV_SAL` (`SalesId`),
  CONSTRAINT `FK_INV_SAL` FOREIGN KEY (`SalesId`) REFERENCES `tpenjualan` (`SalesId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenjualan_inv` WRITE;
/*!40000 ALTER TABLE `tpenjualan_inv` DISABLE KEYS */;

INSERT INTO `tpenjualan_inv` (`InvId`, `SalesId`, `InvNo`, `InvDate`, `InvTotal`, `InvRemarks`, `CreatedBy`, `CreatedOn`)
VALUES
	(1,10,'S002-1','2022-01-16',150000,'DEV','admin','2022-01-16 13:43:06');

/*!40000 ALTER TABLE `tpenjualan_inv` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
