# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: web_autoshop_20220128
# Generation Time: 2022-09-17 05:56:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','BENGKEL ERIC'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Auto Service Management System'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','ERIC AUTO SERVICE & SPAREPART'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Imam Bonjol Tebing Tinggi'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','082368309119'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','Partopi Tao'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `IdUnit` bigint(10) DEFAULT NULL,
  `Email` varchar(50) NOT NULL DEFAULT '',
  `NM_FullName` varchar(50) DEFAULT NULL,
  `NM_ProfileImage` varchar(250) DEFAULT NULL,
  `DATE_Registered` datetime DEFAULT NULL,
  `IS_EmailVerified` tinyint(1) DEFAULT NULL,
  `IS_LoginViaGoogle` tinyint(1) DEFAULT NULL,
  `IS_LoginViaFacebook` tinyint(1) DEFAULT NULL,
  `NM_GoogleOAuthID` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  CONSTRAINT `FK_Userinformation_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `IdUnit`, `Email`, `NM_FullName`, `NM_ProfileImage`, `DATE_Registered`, `IS_EmailVerified`, `IS_LoginViaGoogle`, `IS_LoginViaFacebook`, `NM_GoogleOAuthID`)
VALUES
	('admin',NULL,'yoelrolas@gmail.com','Partopi Tao',NULL,'2018-08-17 00:00:00',NULL,NULL,NULL,NULL),
	('admin.manda',NULL,'admin.manda','Manda',NULL,'2022-01-16 00:00:00',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2022-09-17 12:56:19','::1'),
	('admin.manda','f6f8097b050bfcbd7b14a733fd918f75',1,0,'2022-01-28 17:26:52','114.122.6.245');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mbarang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mbarang`;

CREATE TABLE `mbarang` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdKategori` bigint(10) unsigned NOT NULL,
  `NmStock` varchar(100) NOT NULL DEFAULT '',
  `NmKode` varchar(100) DEFAULT NULL,
  `NmSatuan` varchar(10) DEFAULT NULL,
  `NmKeterangan` text,
  `Harga` double DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mbarang` WRITE;
/*!40000 ALTER TABLE `mbarang` DISABLE KEYS */;

INSERT INTO `mbarang` (`Id`, `IdKategori`, `NmStock`, `NmKode`, `NmSatuan`, `NmKeterangan`, `Harga`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,3,'Perawatan / Perbaikan',NULL,'KALI',NULL,0,'admin','2022-01-15 12:00:00',NULL,NULL),
	(8,2,'Kunci Pas',NULL,'BUAH',NULL,0,'admin','2022-01-15 15:59:24',NULL,NULL),
	(11,1,'TEMP',NULL,'PCS',NULL,99999,'admin','2022-01-16 16:09:16',NULL,NULL),
	(13,1,'PAKING DEKSEL XTRAIL T30',NULL,'PCS',NULL,195000,'admin.manda','2022-01-18 15:27:33',NULL,NULL),
	(14,1,'OLI TMO 10W-40 1L',NULL,'Liter',NULL,71000,'admin.manda','2022-01-24 09:21:11',NULL,NULL),
	(24,1,'RACK END G-MAX',NULL,'pcs',NULL,0,'admin.manda','2022-01-24 10:09:22',NULL,NULL),
	(55,1,'BALJOINT LOW AVZ',NULL,'pcs',NULL,140000,'admin.manda','2022-01-26 08:58:02',NULL,NULL),
	(56,1,'MATAHARI INV BENSIN',NULL,'pcs',NULL,700000,'admin.manda','2022-01-26 09:01:33',NULL,NULL),
	(58,1,'KAIN CLOS INV BENSIN',NULL,'pcs',NULL,800000,'admin.manda','2022-01-26 09:02:48',NULL,NULL),
	(59,1,'MATAHARI VIOS',NULL,'pcs',NULL,550000,'admin.manda','2022-01-26 09:04:07',NULL,NULL),
	(60,1,'KAIN CLOS VIOS',NULL,'pcs',NULL,650000,'admin.manda','2022-01-26 09:04:58',NULL,NULL),
	(61,1,'LAHAR CLOS VIOS',NULL,'pcs',NULL,225000,'admin.manda','2022-01-26 09:10:48',NULL,NULL),
	(62,1,'TMO BENSIN 1 LITER',NULL,'Liter',NULL,71000,'admin.manda','2022-01-26 09:12:37',NULL,NULL),
	(64,1,'OLI FASTRON 10W-40 1L',NULL,'Liter',NULL,0,'admin.manda','2022-01-26 09:14:46','admin.manda','2022-01-26 15:29:49'),
	(65,1,'SEPATU REM BELAKANG G-MAX',NULL,'set',NULL,165000,'admin.manda','2022-01-26 09:15:36',NULL,NULL),
	(66,1,'SILANG 4 KJG',NULL,'pcs',NULL,95000,'admin.manda','2022-01-26 09:16:09',NULL,NULL),
	(67,1,'SILANG 4 INV/AVZ',NULL,'pcs',NULL,145000,'admin.manda','2022-01-26 09:16:55',NULL,NULL),
	(69,1,'TIE ROD STABIL APV',NULL,'pcs',NULL,60000,'admin.manda','2022-01-26 09:20:01',NULL,NULL),
	(70,1,'LEM SIKA',NULL,'pcs',NULL,33500,'admin.manda','2022-01-26 09:21:00',NULL,NULL),
	(71,1,'SEPATU REM BELAKANG INNOVA',NULL,'set',NULL,185000,'admin.manda','2022-01-26 09:22:35',NULL,NULL),
	(72,1,'SEPATU REM DEPAN KIJANG',NULL,'set',NULL,105000,'admin.manda','2022-01-26 09:23:15',NULL,NULL),
	(73,1,'TALI KIPAS HONDA JAZZ',NULL,'pcs',NULL,135000,'admin.manda','2022-01-26 09:24:09',NULL,NULL),
	(75,1,'COIL INNOVA',NULL,'pcs',NULL,360000,'admin.manda','2022-01-26 09:26:04',NULL,NULL),
	(76,1,'KARET BELAH AVZ',NULL,'pcs',NULL,7500,'admin.manda','2022-01-26 09:26:49',NULL,NULL),
	(77,1,'TALI KIPAS AVZ A/N',NULL,'pcs',NULL,110000,'admin.manda','2022-01-26 09:28:34',NULL,NULL),
	(78,1,'RACK END AVZ OLD ',NULL,'pcs',NULL,145000,'admin.manda','2022-01-26 09:30:41',NULL,NULL),
	(79,1,'RACK END G-MAX',NULL,'pcs',NULL,145000,'admin.manda','2022-01-26 09:31:17',NULL,NULL),
	(81,1,'KARET BELAH APV',NULL,'pcs',NULL,15000,'admin.manda','2022-01-26 09:32:37',NULL,NULL),
	(82,1,'SEAL POLY APV',NULL,'pcs',NULL,45000,'admin.manda','2022-01-26 09:33:48',NULL,NULL),
	(83,1,'BOSENTRIK BESAR HONDA JAZZ',NULL,'pcs',NULL,120000,'admin.manda','2022-01-26 09:37:57',NULL,NULL),
	(84,1,'BOSENTRIK BESAR AVANZA',NULL,'pcs',NULL,100000,'admin.manda','2022-01-26 09:38:26',NULL,NULL),
	(85,1,'BALL JOINT BAWAH INNOVA',NULL,'pcs',NULL,160000,'admin.manda','2022-01-26 09:39:31',NULL,NULL),
	(86,1,'RACK END INNOVA',NULL,'pcs',NULL,160000,'admin.manda','2022-01-26 09:40:20',NULL,NULL),
	(87,1,'SARINGAN SOLAR INNOVA REBORN',NULL,'pcs',NULL,100000,'admin.manda','2022-01-26 09:40:50',NULL,NULL),
	(88,1,'POMPA BENSIN AVZ',NULL,'pcs',NULL,185000,'admin.manda','2022-01-26 09:41:40',NULL,NULL),
	(89,1,'POMPA BENSIN INV',NULL,'pcs',NULL,200000,'admin.manda','2022-01-26 09:42:12',NULL,NULL),
	(90,1,'KIT KLOS LOW 5K',NULL,'pcs',NULL,50000,'admin.manda','2022-01-26 09:43:10',NULL,NULL),
	(91,1,'KIT KLOS LOW 7K',NULL,'pcs',NULL,60000,'admin.manda','2022-01-26 09:44:08',NULL,NULL),
	(92,1,'KIT KLOS ATAS 5K',NULL,'pcs',NULL,70000,'admin.manda','2022-01-26 09:45:01',NULL,NULL),
	(93,1,'KIT KLOS ATAS 7K/21',NULL,'pcs',NULL,60000,'admin.manda','2022-01-26 09:45:34',NULL,NULL),
	(94,1,'KARET MASTER REM KJG',NULL,'set',NULL,55000,'admin.manda','2022-01-26 09:46:23',NULL,NULL),
	(95,1,'KARET MASTER REM AVZ',NULL,'set',NULL,65000,'admin.manda','2022-01-26 09:47:12',NULL,NULL),
	(96,1,'KARET MASTER REM INV',NULL,'set',NULL,75000,'admin.manda','2022-01-26 09:47:48',NULL,NULL),
	(97,1,'TEMING BELT APV',NULL,'pcs',NULL,175000,'admin.manda','2022-01-26 09:48:51',NULL,NULL),
	(98,1,'SARINGAN HAWA INNOVA',NULL,'pcs',NULL,265000,'admin.manda','2022-01-26 09:49:36',NULL,NULL),
	(99,1,'SEAL POLY AVZ',NULL,'pcs',NULL,40000,'admin.manda','2022-01-26 09:50:14',NULL,NULL),
	(100,1,'LAHAR DEPAN INNOVA',NULL,'pcs',NULL,205000,'admin.manda','2022-01-26 09:50:45',NULL,NULL),
	(101,1,'BOSENTRIK BESAR KIJANG',NULL,'pcs',NULL,70000,'admin.manda','2022-01-26 09:51:26',NULL,NULL),
	(102,1,'BOSENTRIK KECIL AVANZA',NULL,'pcs',NULL,80000,'admin.manda','2022-01-26 09:52:12',NULL,NULL),
	(103,1,'BALJOINT LOW KJG',NULL,'pcs',NULL,160000,'admin.manda','2022-01-26 09:52:48',NULL,NULL),
	(104,1,'OLI FILTER AVZ',NULL,'pcs',NULL,33000,'admin.manda','2022-01-26 09:54:37',NULL,NULL),
	(105,1,'OLI FILTER INNOVA',NULL,'pcs',NULL,38000,'admin.manda','2022-01-26 09:55:11',NULL,NULL),
	(106,1,'MATAHARI G-MAX',NULL,'pcs',NULL,285000,'admin.manda','2022-01-26 09:55:42',NULL,NULL),
	(107,1,'KAIN CLOS G-MAX',NULL,'pcs',NULL,425000,'admin.manda','2022-01-26 09:56:20',NULL,NULL),
	(108,1,'MATAHARI AVZ ',NULL,'pcs',NULL,360000,'admin.manda','2022-01-26 09:57:08',NULL,NULL),
	(109,1,'LAHAR CLOS AVZ',NULL,'pcs',NULL,175000,'admin.manda','2022-01-26 09:57:41',NULL,NULL),
	(110,1,'OLI RORED 90',NULL,'btl',NULL,45000,'admin.manda','2022-01-27 09:28:25',NULL,NULL),
	(111,1,'OLI PRIMA XP 4ltr',NULL,'gln',NULL,148000,'admin.manda','2022-01-27 09:30:02',NULL,NULL),
	(112,1,'BUSI BKR6E',NULL,'bh',NULL,16000,'admin.manda','2022-01-27 09:31:29',NULL,NULL),
	(113,1,'KARET REM 13/16',NULL,'bh',NULL,8000,'admin.manda','2022-01-27 09:33:08',NULL,NULL),
	(114,1,'MEGACOOL',NULL,'btl',NULL,30000,'admin.manda','2022-01-27 09:34:06',NULL,NULL),
	(115,1,'SARINGAN HAWA  APV',NULL,'bh',NULL,70000,'admin.manda','2022-01-27 09:34:47','admin.manda','2022-01-27 13:05:39'),
	(116,1,'BOSPER FTR OHK',NULL,'bh',NULL,100000,'admin.manda','2022-01-27 09:35:53',NULL,NULL),
	(117,1,'OLI SHELL HELIX HX3 4ltr',NULL,'gln',NULL,165000,'admin.manda','2022-01-27 09:37:40',NULL,NULL),
	(118,1,'OLI UNION 140 1ltr',NULL,'gln',NULL,49000,'admin.manda','2022-01-27 09:51:18',NULL,NULL),
	(119,1,'KARET REM ',NULL,'bh',NULL,10000,'admin.manda','2022-01-27 09:52:58',NULL,NULL),
	(120,1,'MINYAK REM WAGNER ',NULL,'btl',NULL,22000,'admin.manda','2022-01-27 09:53:50',NULL,NULL),
	(121,1,'SARINGAN OLI C.1204 SAKURA',NULL,'pcs',NULL,21120,'admin.manda','2022-01-27 10:29:50',NULL,NULL),
	(122,1,'AIR BATRAI',NULL,'btl',NULL,2500,'admin.manda','2022-01-27 10:30:26',NULL,NULL),
	(123,1,'OLI CASTROL MAGNATEC 5W-40 1L',NULL,'Liter',NULL,87000,'admin.manda','2022-01-27 15:25:32',NULL,NULL),
	(124,1,'OLI CASTROL MAGNATEC 10W-40 4L',NULL,'gln',NULL,341000,'admin.manda','2022-01-27 15:26:03',NULL,NULL),
	(125,1,'OLI TMO 10W-40 4L',NULL,'gln',NULL,276000,'admin.manda','2022-01-27 15:29:23',NULL,NULL),
	(126,1,'RADIATOR COOLANT ',NULL,'gln',NULL,0,'admin.manda','2022-01-27 16:11:16',NULL,NULL),
	(127,1,'SILANG 4 STIUR',NULL,'bh',NULL,100000,'admin.manda','2022-01-27 17:06:12',NULL,NULL),
	(130,1,'KARET STABIL',NULL,'pcs',NULL,2000,'admin.manda','2022-01-28 11:48:08',NULL,NULL),
	(132,1,'KARET SHOCK INV',NULL,'pcs',NULL,10000,'admin.manda','2022-01-28 11:50:22',NULL,NULL),
	(133,1,'PAKING TUTUP KLEP INNOVA BENSIN ASLI',NULL,'pcs',NULL,145000,'admin.manda','2022-01-28 11:55:06',NULL,NULL),
	(134,1,'PAKING BUSI ASLI ',NULL,'pcs',NULL,145000,'admin.manda','2022-01-28 11:55:55',NULL,NULL),
	(135,1,'KARBON CLEANER',NULL,'btl',NULL,18000,'admin.manda','2022-01-28 12:06:32',NULL,NULL),
	(136,1,'KLEP MINYAK',NULL,'gln',NULL,145000,'admin.manda','2022-01-28 12:13:23',NULL,NULL),
	(140,1,'seal asklep',NULL,'pcs',NULL,150000,'admin.manda','2022-01-28 12:44:52',NULL,NULL),
	(143,1,'KIT STABIL ASLI',NULL,'ktk',NULL,20000,'admin.manda','2022-01-28 13:11:20',NULL,NULL);

/*!40000 ALTER TABLE `mbarang` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mgudang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mgudang`;

CREATE TABLE `mgudang` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmWarehouse` varchar(100) NOT NULL DEFAULT '',
  `NmAlamat` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mgudang` WRITE;
/*!40000 ALTER TABLE `mgudang` DISABLE KEYS */;

INSERT INTO `mgudang` (`Id`, `NmWarehouse`, `NmAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'GUDANG UTAMA','Jl. Imam Bonjol, Tebing Tinggi, Sumatera Utara','admin','2021-01-30 18:44:22','admin.manda','2022-01-24 09:16:10');

/*!40000 ALTER TABLE `mgudang` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mkategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mkategori`;

CREATE TABLE `mkategori` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmKategori` varchar(100) NOT NULL DEFAULT '',
  `IsStock` tinyint(1) NOT NULL DEFAULT '1',
  `IsSaleItem` tinyint(1) NOT NULL DEFAULT '1',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mkategori` WRITE;
/*!40000 ALTER TABLE `mkategori` DISABLE KEYS */;

INSERT INTO `mkategori` (`Id`, `NmKategori`, `IsStock`, `IsSaleItem`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'SPAREPART',1,1,'admin','2022-01-15 12:00:00',NULL,NULL),
	(3,'JASA',0,1,'admin','2022-01-15 12:00:00',NULL,NULL);

/*!40000 ALTER TABLE `mkategori` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpelanggan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpelanggan`;

CREATE TABLE `mpelanggan` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmCustomer` varchar(100) NOT NULL DEFAULT '',
  `NmNama` varchar(100) DEFAULT NULL,
  `NmNoHP` varchar(100) DEFAULT NULL,
  `NmAlamat` varchar(100) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpelanggan` WRITE;
/*!40000 ALTER TABLE `mpelanggan` DISABLE KEYS */;

INSERT INTO `mpelanggan` (`Id`, `NmCustomer`, `NmNama`, `NmNoHP`, `NmAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'Lainnya','-','-','-','admin','2022-01-16 12:00:00',NULL,NULL);

/*!40000 ALTER TABLE `mpelanggan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpemasok
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpemasok`;

CREATE TABLE `mpemasok` (
  `Id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmSupplier` varchar(100) NOT NULL DEFAULT '',
  `NmNama` varchar(100) DEFAULT NULL,
  `NmNoHP` varchar(100) DEFAULT NULL,
  `NmAlamat` varchar(100) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpemasok` WRITE;
/*!40000 ALTER TABLE `mpemasok` DISABLE KEYS */;

INSERT INTO `mpemasok` (`Id`, `NmSupplier`, `NmNama`, `NmNoHP`, `NmAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,'Lainnya','-','-','-','admin','2022-01-15 12:00:00',NULL,NULL);

/*!40000 ALTER TABLE `mpemasok` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbarang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbarang`;

CREATE TABLE `tbarang` (
  `MovId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdWarehouse` bigint(10) unsigned NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  `DOId` bigint(10) unsigned DEFAULT NULL,
  `ReceiptId` bigint(10) unsigned DEFAULT NULL,
  `MoveType` varchar(10) NOT NULL DEFAULT '',
  `MoveDate` date NOT NULL,
  `MoveQty` double NOT NULL,
  `MoveRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`MovId`),
  KEY `FK_MOV_STOCK` (`IdStock`),
  KEY `FK_MOV_WAREHOUSE` (`IdWarehouse`),
  KEY `FK_MOV_DO` (`DOId`),
  KEY `FK_MOV_RECEIPT` (`ReceiptId`),
  CONSTRAINT `FK_MOV_DO` FOREIGN KEY (`DOId`) REFERENCES `tpengiriman` (`DOId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MOV_RECEIPT` FOREIGN KEY (`ReceiptId`) REFERENCES `tpenerimaan` (`ReceiptId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MOV_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_MOV_WAREHOUSE` FOREIGN KEY (`IdWarehouse`) REFERENCES `mgudang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbarang` WRITE;
/*!40000 ALTER TABLE `tbarang` DISABLE KEYS */;

INSERT INTO `tbarang` (`MovId`, `IdWarehouse`, `IdStock`, `DOId`, `ReceiptId`, `MoveType`, `MoveDate`, `MoveQty`, `MoveRemarks`, `CreatedBy`, `CreatedOn`)
VALUES
	(14,1,11,NULL,NULL,'PUR','2022-01-16',12,'STOCK AWAL','admin','2022-01-16 16:09:16'),
	(17,1,13,NULL,NULL,'PUR','2022-01-01',10,'STOCK AWAL','admin.mand','2022-01-18 15:27:33'),
	(26,1,14,NULL,NULL,'PUR','2022-01-24',10,'STOCK AWAL','admin.mand','2022-01-24 09:21:11'),
	(27,1,24,NULL,NULL,'PUR','2022-01-24',2,'STOCK AWAL','admin.mand','2022-01-24 10:09:22'),
	(28,1,55,NULL,NULL,'PUR','2022-01-26',1,'STOCK AWAL','admin.mand','2022-01-26 08:58:02'),
	(29,1,62,NULL,NULL,'PUR','2022-01-25',10,'STOCK AWAL','admin.mand','2022-01-26 09:12:37'),
	(30,1,104,NULL,NULL,'PUR','2022-01-26',5,'STOCK AWAL','admin.mand','2022-01-26 09:54:37'),
	(42,1,104,NULL,12,'PUR','2022-01-26',5,'01/01/26/2022','admin.mand','2022-01-26 14:55:17'),
	(43,1,105,NULL,12,'PUR','2022-01-26',5,'01/01/26/2022','admin.mand','2022-01-26 14:55:17'),
	(44,1,106,NULL,12,'PUR','2022-01-26',1,'01/01/26/2022','admin.mand','2022-01-26 14:55:17'),
	(45,1,107,NULL,12,'PUR','2022-01-26',1,'01/01/26/2022','admin.mand','2022-01-26 14:55:17'),
	(46,1,108,NULL,12,'PUR','2022-01-26',1,'01/01/26/2022','admin.mand','2022-01-26 14:55:17'),
	(47,1,109,NULL,12,'PUR','2022-01-26',2,'01/01/26/2022','admin.mand','2022-01-26 14:55:17'),
	(48,1,110,NULL,NULL,'PUR','2022-01-27',2,'STOCK AWAL','admin.mand','2022-01-27 09:28:25'),
	(49,1,116,NULL,13,'PUR','2022-01-27',1,'01/01/27/2022','admin.mand','2022-01-27 09:41:53'),
	(50,1,111,NULL,13,'PUR','2022-01-27',2,'01/01/27/2022','admin.mand','2022-01-27 09:41:53'),
	(51,1,112,NULL,13,'PUR','2022-01-27',4,'01/01/27/2022','admin.mand','2022-01-27 09:41:53'),
	(52,1,113,NULL,13,'PUR','2022-01-27',4,'01/01/27/2022','admin.mand','2022-01-27 09:41:53'),
	(53,1,110,NULL,13,'PUR','2022-01-27',2,'01/01/27/2022','admin.mand','2022-01-27 09:41:53'),
	(54,1,114,NULL,13,'PUR','2022-01-27',1,'01/01/27/2022','admin.mand','2022-01-27 09:41:53'),
	(55,1,115,NULL,13,'PUR','2022-01-27',1,'01/01/27/2022','admin.mand','2022-01-27 09:41:53'),
	(56,1,117,NULL,14,'PUR','2022-01-27',1,'02/01/27/2022','admin.mand','2022-01-27 10:17:46'),
	(57,1,118,NULL,14,'PUR','2022-01-27',2,'02/01/27/2022','admin.mand','2022-01-27 10:17:46'),
	(58,1,119,NULL,14,'PUR','2022-01-27',2,'02/01/27/2022','admin.mand','2022-01-27 10:17:46'),
	(59,1,120,NULL,14,'PUR','2022-01-27',1,'02/01/27/2022','admin.mand','2022-01-27 10:17:46'),
	(63,1,117,17,NULL,'SAL','2022-01-27',-1,'','admin.mand','2022-01-27 10:41:09'),
	(64,1,121,17,NULL,'SAL','2022-01-27',-1,'','admin.mand','2022-01-27 10:41:09'),
	(65,1,122,17,NULL,'SAL','2022-01-27',-1,'','admin.mand','2022-01-27 10:41:09'),
	(66,1,121,18,NULL,'SAL','2022-01-27',-1,'BON MANDOR','admin.mand','2022-01-27 13:09:24'),
	(67,1,115,18,NULL,'SAL','2022-01-27',-1,'BON MANDOR','admin.mand','2022-01-27 13:09:24'),
	(68,1,111,18,NULL,'SAL','2022-01-27',-1,'BON MANDOR','admin.mand','2022-01-27 13:09:24'),
	(71,1,55,NULL,15,'PUR','2022-01-27',1,'03/01/27/2022','admin.mand','2022-01-27 17:05:13'),
	(72,1,77,NULL,15,'PUR','2022-01-27',1,'03/01/27/2022','admin.mand','2022-01-27 17:05:13'),
	(73,1,127,NULL,16,'PUR','2022-01-27',1,'04/01/27/2022','admin.mand','2022-01-27 17:07:42'),
	(74,1,55,21,NULL,'SAL','2022-01-27',-1,'LUNAS','admin.mand','2022-01-27 17:10:42'),
	(75,1,76,21,NULL,'SAL','2022-01-27',-2,'LUNAS','admin.mand','2022-01-27 17:10:42'),
	(76,1,130,NULL,NULL,'PUR','2022-01-28',2,'STOCK AWAL','admin.mand','2022-01-28 11:48:08'),
	(77,1,133,NULL,17,'PUR','2022-01-28',1,'01/01/28/2022','admin.mand','2022-01-28 11:59:53'),
	(78,1,134,NULL,17,'PUR','2022-01-28',1,'01/01/28/2022','admin.mand','2022-01-28 11:59:53'),
	(79,1,114,NULL,17,'PUR','2022-01-28',1,'01/01/28/2022','admin.mand','2022-01-28 11:59:53'),
	(80,1,122,NULL,17,'PUR','2022-01-28',6,'01/01/28/2022','admin.mand','2022-01-28 11:59:53'),
	(81,1,132,NULL,17,'PUR','2022-01-28',4,'01/01/28/2022','admin.mand','2022-01-28 11:59:53'),
	(82,1,130,NULL,17,'PUR','2022-01-28',2,'01/01/28/2022','admin.mand','2022-01-28 11:59:53'),
	(83,1,136,NULL,NULL,'PUR','2022-01-28',1,'STOCK AWAL','admin.mand','2022-01-28 12:13:23'),
	(84,1,140,NULL,NULL,'PUR','2022-01-28',2,'STOCK AWAL','admin.mand','2022-01-28 12:44:52'),
	(85,1,143,NULL,18,'PUR','2022-01-28',1,'02/01/28/2022','admin.mand','2022-01-28 13:12:25'),
	(89,1,132,26,NULL,'SAL','2022-01-28',-4,'LUNAS','admin.mand','2022-01-28 18:23:31'),
	(90,1,130,26,NULL,'SAL','2022-01-28',-4,'LUNAS','admin.mand','2022-01-28 18:23:31'),
	(98,1,112,NULL,20,'PUR','2022-01-28',3,'Pembelian Barang','admin','2022-01-29 00:17:42'),
	(101,1,112,25,NULL,'SAL','2022-01-28',-3,'DEV','admin','2022-01-29 00:52:39');

/*!40000 ALTER TABLE `tbarang` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tdeposit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tdeposit`;

CREATE TABLE `tdeposit` (
  `DepositId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DepositNo` varchar(50) NOT NULL DEFAULT '',
  `DepositDate` date NOT NULL,
  `DepositName` varchar(50) DEFAULT NULL,
  `DepositRemarks` text,
  `DepositAmount` double NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`DepositId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tfaktur
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tfaktur`;

CREATE TABLE `tfaktur` (
  `InvId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DOId` bigint(10) unsigned DEFAULT NULL,
  `ReceiptId` bigint(10) unsigned DEFAULT NULL,
  `InvNo` varchar(50) DEFAULT NULL,
  `InvDate` date NOT NULL,
  `InvDue` date NOT NULL,
  `InvRemarks` text,
  `InvRemarksPayment` text,
  `InvFreight` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`InvId`),
  KEY `FK_INVOICE_RECEIPT` (`ReceiptId`),
  KEY `FK_INVOICE_DELIVERY` (`DOId`),
  CONSTRAINT `FK_INVOICE_DELIVERY` FOREIGN KEY (`DOId`) REFERENCES `tpengiriman` (`DOId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_INVOICE_RECEIPT` FOREIGN KEY (`ReceiptId`) REFERENCES `tpenerimaan` (`ReceiptId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tfaktur_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tfaktur_det`;

CREATE TABLE `tfaktur_det` (
  `InvDetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `InvId` bigint(10) unsigned NOT NULL,
  `InvQty` double NOT NULL,
  `InvPrice` double NOT NULL,
  `InvDisc` double NOT NULL,
  `InvTax` double NOT NULL,
  `InvTotal` double NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`InvDetId`),
  KEY `FK_DET_INVOICE` (`InvId`),
  KEY `FK_INVOICE_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_INVOICE` FOREIGN KEY (`InvId`) REFERENCES `tfaktur` (`InvId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_INVOICE_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tpembelian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpembelian`;

CREATE TABLE `tpembelian` (
  `PurchId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdSupplier` bigint(10) unsigned NOT NULL,
  `PurchSupplier` text,
  `PurchNo` varchar(50) DEFAULT NULL,
  `PurchDate` date NOT NULL,
  `PurchAddr` text,
  `PurchRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`PurchId`),
  KEY `FK_PURCHASE_SUPP` (`IdSupplier`),
  CONSTRAINT `FK_PURCHASE_SUPP` FOREIGN KEY (`IdSupplier`) REFERENCES `mpemasok` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpembelian` WRITE;
/*!40000 ALTER TABLE `tpembelian` DISABLE KEYS */;

INSERT INTO `tpembelian` (`PurchId`, `IdSupplier`, `PurchSupplier`, `PurchNo`, `PurchDate`, `PurchAddr`, `PurchRemarks`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(12,1,'AUTO MAKMUR','01/01/26/2022','2022-01-26',NULL,'Pembelian Barang','admin.manda','2022-01-26 14:55:17',NULL,NULL),
	(13,1,'JAYA MOTOR','01/01/27/2022','2022-01-27',NULL,'','admin.manda','2022-01-27 09:41:53',NULL,NULL),
	(14,1,'JAYA MOTOR','02/01/27/2022','2022-01-27',NULL,'','admin.manda','2022-01-27 10:17:46',NULL,NULL),
	(15,1,'JAYA MOTOR','03/01/27/2022','2022-01-27',NULL,'Pembelian Barang','admin.manda','2022-01-27 17:05:13',NULL,NULL),
	(16,1,'JAYA MOTOR','04/01/27/2022','2022-01-27',NULL,'Pembelian Barang','admin.manda','2022-01-27 17:07:42','admin','2022-01-29 00:18:47'),
	(17,1,'JAYA MOTOR','01/01/28/2022','2022-01-28',NULL,'Pembelian Barang','admin.manda','2022-01-28 11:59:53',NULL,NULL),
	(18,1,'JAYA MOTOR','02/01/28/2022','2022-01-28',NULL,'Pembelian Barang','admin.manda','2022-01-28 13:12:25',NULL,NULL),
	(20,1,'JAYA MOTOR','03/01/28/2022','2022-01-28',NULL,'Pembelian Barang','admin.manda','2022-01-28 14:49:04','admin','2022-01-29 00:17:42');

/*!40000 ALTER TABLE `tpembelian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpembelian_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpembelian_det`;

CREATE TABLE `tpembelian_det` (
  `PurchDetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `PurchId` bigint(10) unsigned NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  `PurchQty` double NOT NULL,
  `PurchPrice` double NOT NULL,
  `PurchDisc` double NOT NULL,
  `PurchTax` double NOT NULL,
  `PurchTotal` double NOT NULL,
  PRIMARY KEY (`PurchDetId`),
  KEY `FK_DET_PURCHASE` (`PurchId`),
  KEY `FK_PURCHASE_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_PURCHASE` FOREIGN KEY (`PurchId`) REFERENCES `tpembelian` (`PurchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PURCHASE_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpembelian_det` WRITE;
/*!40000 ALTER TABLE `tpembelian_det` DISABLE KEYS */;

INSERT INTO `tpembelian_det` (`PurchDetId`, `PurchId`, `IdStock`, `PurchQty`, `PurchPrice`, `PurchDisc`, `PurchTax`, `PurchTotal`)
VALUES
	(27,12,104,5,33000,0,0,165000),
	(28,12,105,5,38000,0,0,190000),
	(29,12,106,1,285000,0,0,285000),
	(30,12,107,1,425000,0,0,425000),
	(31,12,108,1,360000,0,0,360000),
	(32,12,109,2,175000,0,0,350000),
	(33,13,116,1,100000,0,0,100000),
	(34,13,111,2,148000,0,0,296000),
	(35,13,112,4,16000,0,0,64000),
	(36,13,113,4,8000,0,0,32000),
	(37,13,110,2,45000,0,0,90000),
	(38,13,114,1,30000,0,0,30000),
	(39,13,115,1,70000,0,0,70000),
	(40,14,117,1,165000,0,0,165000),
	(41,14,118,2,49000,0,0,98000),
	(42,14,119,2,10000,0,0,20000),
	(43,14,120,1,22000,0,0,22000),
	(44,15,55,1,160000,0,0,160000),
	(45,15,77,1,25000,0,0,25000),
	(46,16,127,1,100000,0,0,100000),
	(47,17,133,1,145000,0,0,145000),
	(48,17,134,1,145000,0,0,145000),
	(49,17,114,1,30000,0,0,30000),
	(50,17,122,6,2500,0,0,15000),
	(51,17,132,4,10000,0,0,40000),
	(52,17,130,2,2000,0,0,4000),
	(53,18,143,1,20000,0,0,20000),
	(62,20,112,3,16000,0,0,48000);

/*!40000 ALTER TABLE `tpembelian_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpembelian_inv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpembelian_inv`;

CREATE TABLE `tpembelian_inv` (
  `InvId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `PurchId` bigint(10) unsigned NOT NULL,
  `InvNo` varchar(50) DEFAULT NULL,
  `InvDate` date NOT NULL,
  `InvTotal` double NOT NULL,
  `InvRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`InvId`),
  KEY `FK_INV_PUR` (`PurchId`),
  CONSTRAINT `FK_INV_PUR` FOREIGN KEY (`PurchId`) REFERENCES `tpembelian` (`PurchId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpembelian_inv` WRITE;
/*!40000 ALTER TABLE `tpembelian_inv` DISABLE KEYS */;

INSERT INTO `tpembelian_inv` (`InvId`, `PurchId`, `InvNo`, `InvDate`, `InvTotal`, `InvRemarks`, `CreatedBy`, `CreatedOn`)
VALUES
	(13,13,'01/01/27/2022','2022-01-27',682000,'','admin.manda','2022-01-28 14:27:09'),
	(14,14,'02/01/27/2022','2022-01-28',305000,'','admin.manda','2022-01-28 14:27:37'),
	(15,16,'04/01/27/2022','2022-01-27',100000,'','admin.manda','2022-01-28 14:28:18'),
	(16,15,'03/01/27/2022','2022-01-27',185000,'','admin.manda','2022-01-28 14:28:59');

/*!40000 ALTER TABLE `tpembelian_inv` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenerimaan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenerimaan`;

CREATE TABLE `tpenerimaan` (
  `ReceiptId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdWarehouse` bigint(10) unsigned NOT NULL,
  `PurchId` bigint(10) unsigned NOT NULL,
  `ReceiptDate` date NOT NULL,
  `ReceiptRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ReceiptId`),
  KEY `FK_RECEIPT_PURCHASE` (`PurchId`),
  KEY `FK_RECEIPT_WAREHOUSE` (`IdWarehouse`),
  CONSTRAINT `FK_RECEIPT_PURCHASE` FOREIGN KEY (`PurchId`) REFERENCES `tpembelian` (`PurchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RECEIPT_WAREHOUSE` FOREIGN KEY (`IdWarehouse`) REFERENCES `mgudang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenerimaan` WRITE;
/*!40000 ALTER TABLE `tpenerimaan` DISABLE KEYS */;

INSERT INTO `tpenerimaan` (`ReceiptId`, `IdWarehouse`, `PurchId`, `ReceiptDate`, `ReceiptRemarks`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(12,1,12,'2022-01-26','Pembelian Barang','admin.manda','2022-01-26 14:55:17','',NULL),
	(13,1,13,'2022-01-27','','admin.manda','2022-01-27 09:41:53','',NULL),
	(14,1,14,'2022-01-27','','admin.manda','2022-01-27 10:17:46','',NULL),
	(15,1,15,'2022-01-27','Pembelian Barang','admin.manda','2022-01-27 17:05:13','',NULL),
	(16,1,16,'2022-01-27','Pembelian Barang','admin.manda','2022-01-27 17:07:42','admin','2022-01-29 00:18:47'),
	(17,1,17,'2022-01-28','Pembelian Barang','admin.manda','2022-01-28 11:59:53','',NULL),
	(18,1,18,'2022-01-28','Pembelian Barang','admin.manda','2022-01-28 13:12:25','',NULL),
	(20,1,20,'2022-01-28','Pembelian Barang','admin.manda','2022-01-28 14:49:04','admin','2022-01-29 00:17:42');

/*!40000 ALTER TABLE `tpenerimaan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenerimaan_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenerimaan_det`;

CREATE TABLE `tpenerimaan_det` (
  `ReceiptDetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ReceiptId` bigint(10) unsigned NOT NULL,
  `ReceiptQty` double NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`ReceiptDetId`),
  KEY `FK_DET_RECEIPT` (`ReceiptId`),
  KEY `FK_RECEIPT_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_RECEIPT` FOREIGN KEY (`ReceiptId`) REFERENCES `tpenerimaan` (`ReceiptId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_RECEIPT_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenerimaan_det` WRITE;
/*!40000 ALTER TABLE `tpenerimaan_det` DISABLE KEYS */;

INSERT INTO `tpenerimaan_det` (`ReceiptDetId`, `ReceiptId`, `ReceiptQty`, `IdStock`)
VALUES
	(27,12,5,104),
	(28,12,5,105),
	(29,12,1,106),
	(30,12,1,107),
	(31,12,1,108),
	(32,12,2,109),
	(33,13,1,116),
	(34,13,2,111),
	(35,13,4,112),
	(36,13,4,113),
	(37,13,2,110),
	(38,13,1,114),
	(39,13,1,115),
	(40,14,1,117),
	(41,14,2,118),
	(42,14,2,119),
	(43,14,1,120),
	(44,15,1,55),
	(45,15,1,77),
	(46,16,1,127),
	(47,17,1,133),
	(48,17,1,134),
	(49,17,1,114),
	(50,17,6,122),
	(51,17,4,132),
	(52,17,2,130),
	(53,18,1,143),
	(62,20,3,112);

/*!40000 ALTER TABLE `tpenerimaan_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpengiriman
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpengiriman`;

CREATE TABLE `tpengiriman` (
  `DOId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdWarehouse` bigint(10) unsigned NOT NULL,
  `SalesId` bigint(10) unsigned NOT NULL,
  `DONo` varchar(100) DEFAULT NULL,
  `DODate` date NOT NULL,
  `DORemarks` text,
  `DOAddr` text NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`DOId`),
  KEY `FK_DO_WAREHOUSE` (`IdWarehouse`),
  KEY `FK_DO_SALES` (`SalesId`),
  CONSTRAINT `FK_DO_SALES` FOREIGN KEY (`SalesId`) REFERENCES `tpenjualan` (`SalesId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DO_WAREHOUSE` FOREIGN KEY (`IdWarehouse`) REFERENCES `mgudang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpengiriman` WRITE;
/*!40000 ALTER TABLE `tpengiriman` DISABLE KEYS */;

INSERT INTO `tpengiriman` (`DOId`, `IdWarehouse`, `SalesId`, `DONo`, `DODate`, `DORemarks`, `DOAddr`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(17,1,19,'01-27/01/2022','2022-01-27','','','admin.manda','2022-01-27 10:41:09',NULL,NULL),
	(18,1,20,'02-27/01/2022','2022-01-27','BON MANDOR','','admin.manda','2022-01-27 13:09:24',NULL,NULL),
	(19,1,21,'03-27/01/2022','2022-01-27','LUNAS','','admin.manda','2022-01-27 15:06:25',NULL,NULL),
	(21,1,23,'04-27/01/2022','2022-01-27','LUNAS','','admin.manda','2022-01-27 17:10:42',NULL,NULL),
	(24,1,26,'05-28/01/2022','2022-01-27','LUNAS','','admin.manda','2022-01-28 14:34:10',NULL,NULL),
	(25,1,27,'06-27/01/2022 ','2022-01-28','DEV','','admin.manda','2022-01-28 17:47:32','admin','2022-01-29 00:52:39'),
	(26,1,28,'07-27/01/2022 ','2022-01-28','LUNAS','','admin.manda','2022-01-28 18:23:31','admin','2022-01-29 00:53:33');

/*!40000 ALTER TABLE `tpengiriman` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpengiriman_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpengiriman_det`;

CREATE TABLE `tpengiriman_det` (
  `DODetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `DOId` bigint(10) unsigned NOT NULL,
  `DOQty` double NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`DODetId`),
  KEY `FK_DET_DO` (`DOId`),
  KEY `FK_DO_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_DO` FOREIGN KEY (`DOId`) REFERENCES `tpengiriman` (`DOId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_DO_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpengiriman_det` WRITE;
/*!40000 ALTER TABLE `tpengiriman_det` DISABLE KEYS */;

INSERT INTO `tpengiriman_det` (`DODetId`, `DOId`, `DOQty`, `IdStock`)
VALUES
	(34,17,1,117),
	(35,17,1,121),
	(36,17,1,122),
	(37,18,1,121),
	(38,18,1,115),
	(39,18,1,111),
	(40,19,1,1),
	(41,19,1,1),
	(45,21,1,55),
	(46,21,2,76),
	(47,21,1,1),
	(48,24,1,1),
	(50,26,4,132),
	(51,26,4,130),
	(52,26,1,1),
	(55,25,3,112);

/*!40000 ALTER TABLE `tpengiriman_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenjualan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenjualan`;

CREATE TABLE `tpenjualan` (
  `SalesId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdCustomer` bigint(10) unsigned NOT NULL,
  `SalesCustomer` varchar(50) DEFAULT NULL,
  `SalesNo` varchar(50) DEFAULT NULL,
  `SalesDate` date NOT NULL,
  `SalesAddr` text,
  `SalesRemarks` text,
  `SalesCustomerNo` varchar(50) DEFAULT NULL,
  `SalesCustomerNoPlat` varchar(50) DEFAULT NULL,
  `SalesCustomerNmVehicle` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`SalesId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenjualan` WRITE;
/*!40000 ALTER TABLE `tpenjualan` DISABLE KEYS */;

INSERT INTO `tpenjualan` (`SalesId`, `IdCustomer`, `SalesCustomer`, `SalesNo`, `SalesDate`, `SalesAddr`, `SalesRemarks`, `SalesCustomerNo`, `SalesCustomerNoPlat`, `SalesCustomerNmVehicle`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(19,1,'SANDHRA PRIMA 120','01-27/01/2022','2022-01-27',NULL,'','-','-','SANDHRA PRIMA 120','admin.manda','2022-01-27 10:41:09',NULL,NULL),
	(20,1,'SANDHRA PRIMA 115','02-27/01/2022','2022-01-27',NULL,'BON MANDOR','-','-','SANDHRA PRIMA ','admin.manda','2022-01-27 13:09:24',NULL,NULL),
	(21,1,'KIJANG MERAH','03-27/01/2022','2022-01-27',NULL,'LUNAS','-','BK 1881 ET','KIJANG','admin.manda','2022-01-27 15:06:25',NULL,NULL),
	(23,1,'AVZ BK 1454 NC','04-27/01/2022','2022-01-27',NULL,'LUNAS','-','BK 1454 NC','AVANZA','admin.manda','2022-01-27 17:10:42',NULL,NULL),
	(26,1,'KARIMUN','05-28/01/2022','2022-01-27',NULL,'LUNAS','-','-','KARIMUN','admin.manda','2022-01-28 14:34:10',NULL,NULL),
	(27,1,'Carry B 8289','06-27/01/2022 ','2022-01-28',NULL,'DEV','','B 8289 lv','Carry','admin.manda','2022-01-28 17:47:32','admin','2022-01-29 00:52:39'),
	(28,1,'INNOVA BK 1962','07-27/01/2022 ','2022-01-28',NULL,'LUNAS','-','BK 1962','INNOVA','admin.manda','2022-01-28 18:23:31','admin','2022-01-29 00:53:33');

/*!40000 ALTER TABLE `tpenjualan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenjualan_det
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenjualan_det`;

CREATE TABLE `tpenjualan_det` (
  `SalesDetId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `SalesId` bigint(10) unsigned NOT NULL,
  `SalesQty` double NOT NULL,
  `SalesPrice` double NOT NULL,
  `SalesDisc` double NOT NULL,
  `SalesTax` double NOT NULL,
  `SalesTotal` double NOT NULL,
  `SalesDesc` text,
  `IdStock` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`SalesDetId`),
  KEY `FK_DET_SALES` (`SalesId`),
  KEY `FK_SALES_STOCK` (`IdStock`),
  CONSTRAINT `FK_DET_SALES` FOREIGN KEY (`SalesId`) REFERENCES `tpenjualan` (`SalesId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SALES_STOCK` FOREIGN KEY (`IdStock`) REFERENCES `mbarang` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenjualan_det` WRITE;
/*!40000 ALTER TABLE `tpenjualan_det` DISABLE KEYS */;

INSERT INTO `tpenjualan_det` (`SalesDetId`, `SalesId`, `SalesQty`, `SalesPrice`, `SalesDisc`, `SalesTax`, `SalesTotal`, `SalesDesc`, `IdStock`)
VALUES
	(34,19,1,185000,0,0,185000,NULL,117),
	(35,19,1,25000,0,0,25000,NULL,121),
	(36,19,1,3000,0,0,3000,NULL,122),
	(37,20,1,25000,0,0,25000,NULL,121),
	(38,20,1,85000,0,0,85000,NULL,115),
	(39,20,1,175000,0,0,175000,NULL,111),
	(40,21,1,200000,0,0,200000,'CUCI RADIATOR + NEMPEL RADIATOR',1),
	(41,21,1,75000,0,0,75000,'ONGKOS PASANG + LEM',1),
	(45,23,1,185000,0,0,185000,NULL,55),
	(46,23,2,25000,0,0,50000,NULL,76),
	(47,23,1,100000,0,0,100000,'ONGKOS + PRESS',1),
	(48,26,1,100000,0,0,100000,'PERBAIKAN TALI KIPAS',1),
	(50,28,4,15000,0,0,60000,NULL,132),
	(51,28,4,10000,0,0,40000,NULL,130),
	(52,28,1,50000,0,0,50000,'ONGKOS',1),
	(55,27,3,25000,0,0,75000,NULL,112);

/*!40000 ALTER TABLE `tpenjualan_det` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tpenjualan_inv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tpenjualan_inv`;

CREATE TABLE `tpenjualan_inv` (
  `InvId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `SalesId` bigint(10) unsigned NOT NULL,
  `DepositId` bigint(10) unsigned DEFAULT NULL,
  `InvNo` varchar(50) NOT NULL DEFAULT '',
  `InvDate` date NOT NULL,
  `InvTotal` double NOT NULL,
  `InvRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`InvId`),
  KEY `FK_INV_SAL` (`SalesId`),
  KEY `FK_INV_DEPO` (`DepositId`),
  CONSTRAINT `FK_INV_DEPO` FOREIGN KEY (`DepositId`) REFERENCES `tdeposit` (`DepositId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_INV_SAL` FOREIGN KEY (`SalesId`) REFERENCES `tpenjualan` (`SalesId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tpenjualan_inv` WRITE;
/*!40000 ALTER TABLE `tpenjualan_inv` DISABLE KEYS */;

INSERT INTO `tpenjualan_inv` (`InvId`, `SalesId`, `DepositId`, `InvNo`, `InvDate`, `InvTotal`, `InvRemarks`, `CreatedBy`, `CreatedOn`)
VALUES
	(11,19,NULL,'01-27/01/2022','2022-01-27',213000,'LUNAS. UANG SAMA BG HERI','admin.manda','2022-01-27 10:41:32'),
	(12,21,NULL,'03-27/01/2022','2022-01-27',275000,'LUNAS','admin.manda','2022-01-27 15:07:32'),
	(13,26,NULL,'05-28/01/2022','2022-01-28',100000,'','admin.manda','2022-01-28 17:33:11'),
	(14,23,NULL,'04-27/01/2022','2022-01-28',335000,'','admin.manda','2022-01-28 17:34:04'),
	(15,28,NULL,'07-27/01/2022','2022-01-28',150000,'LUNAS','admin.manda','2022-01-28 18:23:59');

/*!40000 ALTER TABLE `tpenjualan_inv` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
